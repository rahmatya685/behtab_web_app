package ir.zngis.behtab.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/")
 public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);


    @RequestMapping(value = {"/",""},method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public String login(){
        return "login.html";
    }
    @RequestMapping(value = {"/home"},method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public String home(){
        return "home.html";
    }


    @RequestMapping(value = {"/users"},method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public String users(){
        return "users.html";
    }


    @RequestMapping(value = {"/hospitals"},method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String hospitals(){
        return "hospitals.html";
    }


    @RequestMapping(value = {"/setting"},method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public String settings(){
        return "setting.html";
    }

}
