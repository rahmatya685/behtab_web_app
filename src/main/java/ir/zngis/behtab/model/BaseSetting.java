package ir.zngis.behtab.model;

import javax.persistence.*;


@MappedSuperclass
public class BaseSetting extends BaseEntity {

    private String setNameEn;

    private String setNameFa;

    private String setKey;


    @Basic
    @Column(name = "SET_NAME_EN")
    public String getSetNameEn() {
        return setNameEn;
    }

    public void setSetNameEn(String setNameEn) {
        this.setNameEn = setNameEn;
    }

    @Basic
    @Column(name = "SET_NAME_FA")
    public String getSetNameFa() {
        return setNameFa;
    }

    public void setSetNameFa(String setNameFa) {
        this.setNameFa = setNameFa;
    }

    @Basic
    @Column(name = "SET_KEY")
    public String getSetKey() {
        return setKey;
    }

    public void setSetKey(String setKey) {
        this.setKey = setKey;
    }
}
