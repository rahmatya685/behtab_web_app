package ir.zngis.behtab.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "surgery_department", schema = "public", catalog = "behtab")
public class SurgeryDepartment extends BaseEntity {
    private String rowGuid;
    private String name;
    private Integer numBedsNormCondition;
    private Integer numBedsCrisisCondition;
    private String comment;
    private String buildingId;
    //    private Collection<Evaluation> evaluationsById;
    private Building buildingByBuildingId;

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "num_beds_norm_condition")
    public Integer getNumBedsNormCondition() {
        return numBedsNormCondition;
    }

    public void setNumBedsNormCondition(Integer numBedsNormCondition) {
        this.numBedsNormCondition = numBedsNormCondition;
    }

    @Basic
    @Column(name = "num_beds_crisis_condition")
    public Integer getNumBedsCrisisCondition() {
        return numBedsCrisisCondition;
    }

    public void setNumBedsCrisisCondition(Integer numBedsCrisisCondition) {
        this.numBedsCrisisCondition = numBedsCrisisCondition;
    }

    @Basic
    @Column(name = "comment", length = 10485760)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Transient
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SurgeryDepartment that = (SurgeryDepartment) o;

        if (rowGuid != that.rowGuid) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (numBedsNormCondition != null ? !numBedsNormCondition.equals(that.numBedsNormCondition) : that.numBedsNormCondition != null)
            return false;
        if (numBedsCrisisCondition != null ? !numBedsCrisisCondition.equals(that.numBedsCrisisCondition) : that.numBedsCrisisCondition != null)
            return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        if (buildingId != null ? !buildingId.equals(that.buildingId) : that.buildingId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (numBedsNormCondition != null ? numBedsNormCondition.hashCode() : 0);
        result = 31 * result + (numBedsCrisisCondition != null ? numBedsCrisisCondition.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (buildingId != null ? buildingId.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "surgeryDepartmentBySurgeryDepartmentId")
//    public Collection<Evaluation> getEvaluationsById() {
//        return evaluationsById;
//    }
//
//    public void setEvaluationsById(Collection<Evaluation> evaluationsById) {
//        this.evaluationsById = evaluationsById;
//    }

    @ManyToOne
    @JoinColumn(name = "building_id", referencedColumnName = "id")
    public Building getBuildingByBuildingId() {
        return buildingByBuildingId;
    }

    public void setBuildingByBuildingId(Building buildingByBuildingId) {
        this.buildingByBuildingId = buildingByBuildingId;
        if (buildingByBuildingId != null)
            this.buildingId = buildingByBuildingId.getRowGuid();
    }
}
