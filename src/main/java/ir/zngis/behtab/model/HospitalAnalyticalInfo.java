package ir.zngis.behtab.model;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import javax.persistence.*;



@Entity
public class HospitalAnalyticalInfo {

    private int id;

    private String name;

    private Point centerPoint;

    private String LevelOfImportance_en;

    private String HazardLevels_en;

    private String LevelOfImportance_fa;

    private String HazardLevels_fa;

    private double riskIndexStructural;

    private double riskIndexNonStructural;

    private double riskIndexOrganizational;


    private double reliabilityStructural;

    private double reliabilityNonStructural;

    private double reliabilityOrganizational;

    private double prioritizationIndex;


    private  String provinceName;

    private  String provinceEname;

    private int type;

    @Id
    @Column(name = "hospital_id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "hospital_name" )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Basic
    @Column(name = "center_point")
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Point getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(Point centerPoint) {
        this.centerPoint = centerPoint;
    }



    @Basic
    @Column(name = "risk_index_structural" )
    public double getRiskIndexStructural() {
        return riskIndexStructural;
    }

    public void setRiskIndexStructural(double riskIndexStructural) {
        this.riskIndexStructural = riskIndexStructural;
    }


    @Basic
    @Column(name = "risk_index_non_structural" )
    public double getRiskIndexNonStructural() {
        return riskIndexNonStructural;
    }

    public void setRiskIndexNonStructural(double riskIndexNonStructural) {
        this.riskIndexNonStructural = riskIndexNonStructural;
    }

    @Basic
    @Column(name = "risk_index_organizational" )
    public double getRiskIndexOrganizational() {
        return riskIndexOrganizational;
    }

    public void setRiskIndexOrganizational(double riskIndexOrganizational) {
        this.riskIndexOrganizational = riskIndexOrganizational;
    }

    @Basic
    @Column(name = "reliability_structural" )
    public double getReliabilityStructural() {
        return reliabilityStructural;
    }

    public void setReliabilityStructural(double reliabilityStructural) {
        this.reliabilityStructural = reliabilityStructural;
    }

    @Basic
    @Column(name = "reliability_non_structural" )
    public double getReliabilityNonStructural() {
        return reliabilityNonStructural;
    }

    public void setReliabilityNonStructural(double reliabilityNonStructural) {
        this.reliabilityNonStructural = reliabilityNonStructural;
    }

    @Basic
    @Column(name = "reliability_organizational" )
    public double getReliabilityOrganizational() {
        return reliabilityOrganizational;
    }

    public void setReliabilityOrganizational(double reliabilityOrganizational) {
        this.reliabilityOrganizational = reliabilityOrganizational;
    }

    @Basic
    @Column(name = "prioritization_index" )
    public double getPrioritizationIndex() {
        return prioritizationIndex;
    }

    public void setPrioritizationIndex(double prioritizationIndex) {
        this.prioritizationIndex = prioritizationIndex;
    }




    @Basic
    @Column(name = "province_name" )
    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }




    @Basic
    @Column(name = "province_name_en" )
    public String getProvinceEname() {
        return provinceEname;
    }

    public void setProvinceEname(String provinceEname) {
        this.provinceEname = provinceEname;
    }

    @Basic
    @Column(name = "level_of_importance_en" )
    public String getLevelOfImportance_en() {
        return LevelOfImportance_en;
    }

    public void setLevelOfImportance_en(String levelOfImportance_en) {
        LevelOfImportance_en = levelOfImportance_en;
    }

    @Basic
    @Column(name = "hazard_levels_en" )
    public String getHazardLevels_en() {
        return HazardLevels_en;
    }

    public void setHazardLevels_en(String hazardLevels_en) {
        HazardLevels_en = hazardLevels_en;
    }

    @Basic
    @Column(name = "level_of_importance_fa" )
    public String getLevelOfImportance_fa() {
        return LevelOfImportance_fa;
    }

    public void setLevelOfImportance_fa(String levelOfImportance_fa) {
        LevelOfImportance_fa = levelOfImportance_fa;
    }

    @Basic
    @Column(name = "hazard_levels_fa" )
    public String getHazardLevels_fa() {
        return HazardLevels_fa;
    }

    public void setHazardLevels_fa(String hazardLevels_fa) {
        HazardLevels_fa = hazardLevels_fa;
    }


    @Basic
    @Column(name = "type" )
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
