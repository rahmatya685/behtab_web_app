package ir.zngis.behtab.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity()
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"hospital_id", "editor_id"})
)
public class HospitalComments extends BaseEntity {

    private String rowGuid;

    private String suggestions;

    private String emergencyActions;

    private Hospital hospital;

    private Integer hospitalId;


    @Basic
    @Column(name = "hospital_id", insertable = false, updatable = false)
    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }


    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }


    @Basic
    @Column(name = "Suggestion")
    public String getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    @Basic
    @Column(name = "EmergencyAction")
    public String getEmergencyActions() {
        return emergencyActions;
    }

    public void setEmergencyActions(String emergencyActions) {
        this.emergencyActions = emergencyActions;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospital_id", referencedColumnName = "id")
    @JsonIgnore
    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;

    }


}
