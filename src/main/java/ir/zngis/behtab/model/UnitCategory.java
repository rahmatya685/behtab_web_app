package ir.zngis.behtab.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "unit_category", schema = "public", catalog = "behtab")
public class UnitCategory extends BaseEntity{
    private int id;
    private String titleEn;
    private String titleFa;
    private String type;
    private Collection<UnitSubCategory> unitSubCategories;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titleEn",length=10485760)
    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    @Basic
    @Column(name = "titleFa",length=10485760)
    public String getTitleFa() {
        return titleFa;
    }

    public void setTitleFa(String titleFa) {
        this.titleFa = titleFa;
    }
    @Basic
    @Column(name = "type",nullable = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitCategory that = (UnitCategory) o;

        if (id != that.id) return false;
        if (titleEn != null ? !titleEn.equals(that.titleEn) : that.titleEn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (titleEn != null ? titleEn.hashCode() : 0);
        return result;
    }



    @OneToMany(mappedBy = "unitCategoryByCategoryId")
    public Collection<UnitSubCategory> getUnitSubCategories() {
        return unitSubCategories;
    }

    public void setUnitSubCategories(Collection<UnitSubCategory> unitSubCategoriesById) {
        this.unitSubCategories = unitSubCategoriesById;
    }
}
