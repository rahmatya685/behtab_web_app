package ir.zngis.behtab.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
public class HazardLevel extends BaseSetting {


    private int id ;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }



    private double hazardLevel;


    public void setId(Integer id) {
        this.id = id;
    }


    @Basic
    @Column(name = "HAZARD_LEVEL")
    public double getHazardLevel() {
        return hazardLevel;
    }

    public void setHazardLevel(double hazardLevel) {
        this.hazardLevel = hazardLevel;
    }

    public HazardLevel() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HazardLevel that = (HazardLevel) o;
        return id == that.id &&
                Double.compare(that.hazardLevel, hazardLevel) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, hazardLevel);
    }
}
