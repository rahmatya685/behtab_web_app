package ir.zngis.behtab.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Collection;

@Entity
public class Question extends BaseEntity {
    private int id;
    private Integer categoryId;
    private String questionTitleFa;
    private String questionTitleEn;
    private String code;
    private String helpLink;
    private Double weight;
    private String briefDescriptionEn;
    private String briefDescriptionFa;
    //    private Collection<Evaluation> evaluationsById;
    private QuestionCategory questionCategoryByCategoryId;

    private boolean used4Hospital;

    private boolean used4HealthHouse;

    private Integer rank;

    @Basic
    @Column(name = "HOSPITAL")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isUsed4Hospital() {
        return used4Hospital;
    }

    public void setUsed4Hospital(boolean used4Hospital) {
        this.used4Hospital = used4Hospital;
    }

    @Basic
    @Column(name = "HEALTH_HOUSE")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean isUsed4HealthHouse() {
        return used4HealthHouse;
    }

    public void setUsed4HealthHouse(boolean used4HealthHouse) {
        this.used4HealthHouse = used4HealthHouse;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //    @Basic
//    @Column(name = "category_id")
    @Transient
    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "question_title_fa", length = 10485760)
    public String getQuestionTitleFa() {
        return questionTitleFa;
    }

    public void setQuestionTitleFa(String questionTitleFa) {
        this.questionTitleFa = questionTitleFa;
    }

    @Basic
    @Column(name = "question_title_en", length = 10485760)
    public String getQuestionTitleEn() {
        return questionTitleEn;
    }

    public void setQuestionTitleEn(String questionTitleEn) {
        this.questionTitleEn = questionTitleEn;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "help_link")
    public String getHelpLink() {
        return helpLink;
    }

    public void setHelpLink(String helpLink) {
        this.helpLink = helpLink;
    }

    @Basic
    @Column(name = "weight")
    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "brief_description_en", length = 10485760)
    public String getBriefDescriptionEn() {
        return briefDescriptionEn;
    }

    public void setBriefDescriptionEn(String briefDescriptionEn) {
        this.briefDescriptionEn = briefDescriptionEn;
    }

    @Basic
    @Column(name = "brief_description_fa", length = 10485760)
    public String getBriefDescriptionFa() {
        return briefDescriptionFa;
    }

    public void setBriefDescriptionFa(String briefDescriptionFa) {
        this.briefDescriptionFa = briefDescriptionFa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Question question = (Question) o;

        if (id != question.id) return false;
        if (categoryId != null ? !categoryId.equals(question.categoryId) : question.categoryId != null) return false;
        if (questionTitleFa != null ? !questionTitleFa.equals(question.questionTitleFa) : question.questionTitleFa != null)
            return false;
        if (questionTitleEn != null ? !questionTitleEn.equals(question.questionTitleEn) : question.questionTitleEn != null)
            return false;
        if (code != null ? !code.equals(question.code) : question.code != null) return false;
        if (helpLink != null ? !helpLink.equals(question.helpLink) : question.helpLink != null) return false;
        if (weight != null ? !weight.equals(question.weight) : question.weight != null) return false;
        if (briefDescriptionEn != null ? !briefDescriptionEn.equals(question.briefDescriptionEn) : question.briefDescriptionEn != null)
            return false;
        if (briefDescriptionFa != null ? !briefDescriptionFa.equals(question.briefDescriptionFa) : question.briefDescriptionFa != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
        result = 31 * result + (questionTitleFa != null ? questionTitleFa.hashCode() : 0);
        result = 31 * result + (questionTitleEn != null ? questionTitleEn.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (helpLink != null ? helpLink.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (briefDescriptionEn != null ? briefDescriptionEn.hashCode() : 0);
        result = 31 * result + (briefDescriptionFa != null ? briefDescriptionFa.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "questionByQuestionId")
//    public Collection<Evaluation> getEvaluationsById() {
//        return evaluationsById;
//    }
//
//    public void setEvaluationsById(Collection<Evaluation> evaluationsById) {
//        this.evaluationsById = evaluationsById;
//    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public QuestionCategory getQuestionCategoryByCategoryId() {
        return questionCategoryByCategoryId;
    }

    public void setQuestionCategoryByCategoryId(QuestionCategory questionCategoryByCategoryId) {
        this.questionCategoryByCategoryId = questionCategoryByCategoryId;
        this.categoryId = questionCategoryByCategoryId.getId();
    }

    @Basic
    @Column(name = "rank")
    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
