package ir.zngis.behtab.model;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import ir.zngis.behtab.util.HealthLocationType;
import ir.zngis.behtab.util.HealthLocationTypeConvertor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

@Entity
public class Hospital extends BaseEntity {
    private int id;
    private String name;
    private String address;
    private Geometry boundary;
    private String phone;
    private String fax;
    private String webSite;
    private Integer numTotalApprovedBeds;
    private Integer numTotalActiveBeds;
    private Integer evaluatorUserId;
    private User evaluatorUser;
    private Integer surveyorUserId;
    private User surveyorUser;
    private Double bedOccupancyNormalRate;
    private Integer totalStaffCount;
    private Integer clinicalStaffCount;
    private Integer nonClinicalStaffCount;
    private String seniorManagersName;
    private String crisisManagersNamesAndPhones;
    private Integer organizationalAffiliation;
    private Integer serviceType;
    private Integer roleInHealthcareNetwork;
    private Integer roleInCrisisAndDisasters;
    private String LevelOfImportance;
    private String HazardLevels;
    private String structureType;
    private Long servedPopulation;
    private Geometry influenceArea;
    private BigInteger lastEvaluationRating;
    private Integer observerL2;
    private String observerL2Comment;
    private Collection<Building> buildingsById;
    private Collection<CandidateLocation> candidateLocationsById;
    private User userByObserverL2;
    private Collection<HospitalEntrance> hospitalEntrancesById;
    private HealthLocationType healthLocationType;

    private String executiveSummary;

    private String observations4StructuralAssessment;

    private String observations4NonStructuralAssessment;

    private String observations4OrganizationalAssessment;

    private String recommendations4StructuralAssessment;

    private String recommendations4NonStructuralAssessment;

    private String recommendations4OrganizationalAssessment;

    private String overallRecommendations;

    //this point is entered in web app just to check that evaluators location to be on vicinity of this point
    private Point centerPoint;



    private List<Attachment> attachments;


    @Column(columnDefinition = "smallint",name = "type")
    @Convert(converter = HealthLocationTypeConvertor.class)
    public HealthLocationType getHealthLocationType() {
        return healthLocationType;
    }

    public void setHealthLocationType(HealthLocationType healthLocationType) {
        this.healthLocationType = healthLocationType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "address", length = 85760)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "geom" )
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Geometry getBoundary() {
        return boundary;
    }

    public void setBoundary(Geometry boundary) {
        this.boundary = boundary;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Basic
    @Column(name = "web_site")
    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    @Basic
    @Column(name = "num_total_approved_beds")
    public Integer getNumTotalApprovedBeds() {
        return numTotalApprovedBeds;
    }

    public void setNumTotalApprovedBeds(Integer numTotalApprovedBeds) {
        this.numTotalApprovedBeds = numTotalApprovedBeds;
    }

    @Basic
    @Column(name = "num_total_active_beds")
    public Integer getNumTotalActiveBeds() {
        return numTotalActiveBeds;
    }

    public void setNumTotalActiveBeds(Integer numTotalActiveBeds) {
        this.numTotalActiveBeds = numTotalActiveBeds;
    }

    //    @Basic
//    @Column(name = "evaluator_user")
    @Transient
    public Integer getEvaluatorUserId() {
        return evaluatorUserId;
    }

    public void setEvaluatorUserId(Integer evaluatorUserId) {
        this.evaluatorUserId = evaluatorUserId;
    }


    @Transient
    public Integer getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(Integer surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    @ManyToOne
    @JoinColumn(name = "evaluator_user_id", referencedColumnName = "id")
    public User getEvaluatorUser() {
        return evaluatorUser;
    }

    public void setEvaluatorUser(User evaluatorUser) {
        this.evaluatorUser = evaluatorUser;
        if(evaluatorUser != null)
            this.evaluatorUserId  = evaluatorUser.getId();
    }

    @ManyToOne
    @JoinColumn(name = "surveyor_user_id", referencedColumnName = "id")
    public User getSurveyorUser() {
        return surveyorUser;
    }

    public void setSurveyorUser(User surveyorUser) {
        this.surveyorUser = surveyorUser;
        if (surveyorUser!=null)
            this.surveyorUserId = surveyorUser.getId();
    }


    @Basic
    @Column(name = "bed_occupancy_rate_norm")
    public Double getBedOccupancyNormalRate() {
        return bedOccupancyNormalRate;
    }

    public void setBedOccupancyNormalRate(Double bedOccupancyNormalRate) {
        this.bedOccupancyNormalRate = bedOccupancyNormalRate;
    }

    @Basic
    @Column(name = "total_staff_count")
    public Integer getTotalStaffCount() {
        return totalStaffCount;
    }

    public void setTotalStaffCount(Integer totalStaffCount) {
        this.totalStaffCount = totalStaffCount;
    }

    @Basic
    @Column(name = "clinical_staff_count")
    public Integer getClinicalStaffCount() {
        return clinicalStaffCount;
    }

    public void setClinicalStaffCount(Integer clinicalStaffCount) {
        this.clinicalStaffCount = clinicalStaffCount;
    }

    @Basic
    @Column(name = "non_clinical_staff_count")
    public Integer getNonClinicalStaffCount() {
        return nonClinicalStaffCount;
    }

    public void setNonClinicalStaffCount(Integer nonClinicalStaffCount) {
        this.nonClinicalStaffCount = nonClinicalStaffCount;
    }

    @Basic
    @Column(name = "senior_managers_name", length = 85760)
    public String getSeniorManagersName() {
        return seniorManagersName;
    }

    public void setSeniorManagersName(String seniorManagersName) {
        this.seniorManagersName = seniorManagersName;
    }

    @Basic
    @Column(name = "crisis_managers_names_and_phones")
    public String getCrisisManagersNamesAndPhones() {
        return crisisManagersNamesAndPhones;
    }

    public void setCrisisManagersNamesAndPhones(String crisisManagersNamesAndPhones) {
        this.crisisManagersNamesAndPhones = crisisManagersNamesAndPhones;
    }

    @Basic
    @Column(name = "organizational_affiliation")
    public Integer getOrganizationalAffiliation() {
        return organizationalAffiliation;
    }

    public void setOrganizationalAffiliation(Integer organizationalAffiliation) {
        this.organizationalAffiliation = organizationalAffiliation;
    }

    @Basic
    @Column(name = "service_type")
    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    @Basic
    @Column(name = "role_in_healthcare_network")
    public Integer getRoleInHealthcareNetwork() {
        return roleInHealthcareNetwork;
    }

    public void setRoleInHealthcareNetwork(Integer roleInHealthcareNetwork) {
        this.roleInHealthcareNetwork = roleInHealthcareNetwork;
    }

    @Basic
    @Column(name = "role_in_crisis_and_disasters")
    public Integer getRoleInCrisisAndDisasters() {
        return roleInCrisisAndDisasters;
    }

    public void setRoleInCrisisAndDisasters(Integer roleInCrisisAndDisasters) {
        this.roleInCrisisAndDisasters = roleInCrisisAndDisasters;
    }

    @Basic
    @Column(name = "structure_type")
    public String getStructureType() {
        return structureType;
    }

    public void setStructureType(String structureType) {
        this.structureType = structureType;
    }

    @Basic
    @Column(name = "served_population")
    public Long getServedPopulation() {
        return servedPopulation;
    }

    public void setServedPopulation(Long servedPopulation) {
        this.servedPopulation = servedPopulation;
    }

    @Basic
    @Column(name = "influence_area" )
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Geometry getInfluenceArea() {
        return influenceArea;
    }

    public void setInfluenceArea(Geometry influenceArea) {
        this.influenceArea = influenceArea;
    }

    @Basic
    @Column(name = "last_evaluation_rating")
    public BigInteger getLastEvaluationRating() {
        return lastEvaluationRating;
    }

    public void setLastEvaluationRating(BigInteger lastEvaluationRating) {
        this.lastEvaluationRating = lastEvaluationRating;
    }


    @Basic
    @Column(name = "center_point")
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Point getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(Point accessPoint) {
        this.centerPoint = accessPoint;
    }



//    @Basic
//    @Column(name = "observer_l2")
//    public Integer getObserverL2() {
//        return observerL2;
//    }
//
//    public void setObserverL2(Integer observerL2) {
//        this.observerL2 = observerL2;
//    }
//
//    @Basic
//    @Column(name = "observer_l2_comment")
//    public String getObserverL2Comment() {
//        return observerL2Comment;
//    }

    public void setObserverL2Comment(String observerL2Comment) {
        this.observerL2Comment = observerL2Comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hospital hospital = (Hospital) o;

        if (id != hospital.id) return false;
        if (name != null ? !name.equals(hospital.name) : hospital.name != null) return false;
        if (address != null ? !address.equals(hospital.address) : hospital.address != null) return false;
        if (boundary != null ? !boundary.equals(hospital.boundary) : hospital.boundary != null) return false;
        if (phone != null ? !phone.equals(hospital.phone) : hospital.phone != null) return false;
        if (fax != null ? !fax.equals(hospital.fax) : hospital.fax != null) return false;
        if (webSite != null ? !webSite.equals(hospital.webSite) : hospital.webSite != null) return false;
        if (numTotalApprovedBeds != null ? !numTotalApprovedBeds.equals(hospital.numTotalApprovedBeds) : hospital.numTotalApprovedBeds != null)
            return false;
        if (numTotalActiveBeds != null ? !numTotalActiveBeds.equals(hospital.numTotalActiveBeds) : hospital.numTotalActiveBeds != null)
            return false;
        if (evaluatorUserId != null ? !evaluatorUserId.equals(hospital.evaluatorUserId) : hospital.evaluatorUserId != null)
            return false;
        if (bedOccupancyNormalRate != null ? !bedOccupancyNormalRate.equals(hospital.bedOccupancyNormalRate) : hospital.bedOccupancyNormalRate != null)
            return false;
        if (totalStaffCount != null ? !totalStaffCount.equals(hospital.totalStaffCount) : hospital.totalStaffCount != null)
            return false;
        if (clinicalStaffCount != null ? !clinicalStaffCount.equals(hospital.clinicalStaffCount) : hospital.clinicalStaffCount != null)
            return false;
        if (nonClinicalStaffCount != null ? !nonClinicalStaffCount.equals(hospital.nonClinicalStaffCount) : hospital.nonClinicalStaffCount != null)
            return false;
        if (seniorManagersName != null ? !seniorManagersName.equals(hospital.seniorManagersName) : hospital.seniorManagersName != null)
            return false;
        if (crisisManagersNamesAndPhones != null ? !crisisManagersNamesAndPhones.equals(hospital.crisisManagersNamesAndPhones) : hospital.crisisManagersNamesAndPhones != null)
            return false;
        if (organizationalAffiliation != null ? !organizationalAffiliation.equals(hospital.organizationalAffiliation) : hospital.organizationalAffiliation != null)
            return false;
        if (serviceType != null ? !serviceType.equals(hospital.serviceType) : hospital.serviceType != null)
            return false;
        if (roleInHealthcareNetwork != null ? !roleInHealthcareNetwork.equals(hospital.roleInHealthcareNetwork) : hospital.roleInHealthcareNetwork != null)
            return false;
        if (roleInCrisisAndDisasters != null ? !roleInCrisisAndDisasters.equals(hospital.roleInCrisisAndDisasters) : hospital.roleInCrisisAndDisasters != null)
            return false;
        if (structureType != null ? !structureType.equals(hospital.structureType) : hospital.structureType != null)
            return false;
        if (servedPopulation != null ? !servedPopulation.equals(hospital.servedPopulation) : hospital.servedPopulation != null)
            return false;
        if (influenceArea != null ? !influenceArea.equals(hospital.influenceArea) : hospital.influenceArea != null)
            return false;
        if (lastEvaluationRating != null ? !lastEvaluationRating.equals(hospital.lastEvaluationRating) : hospital.lastEvaluationRating != null)
            return false;
        if (observerL2 != null ? !observerL2.equals(hospital.observerL2) : hospital.observerL2 != null) return false;
        if (observerL2Comment != null ? !observerL2Comment.equals(hospital.observerL2Comment) : hospital.observerL2Comment != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (boundary != null ? boundary.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (fax != null ? fax.hashCode() : 0);
        result = 31 * result + (webSite != null ? webSite.hashCode() : 0);
        result = 31 * result + (numTotalApprovedBeds != null ? numTotalApprovedBeds.hashCode() : 0);
        result = 31 * result + (numTotalActiveBeds != null ? numTotalActiveBeds.hashCode() : 0);
        result = 31 * result + (evaluatorUserId != null ? evaluatorUserId.hashCode() : 0);
        result = 31 * result + (bedOccupancyNormalRate != null ? bedOccupancyNormalRate.hashCode() : 0);
        result = 31 * result + (totalStaffCount != null ? totalStaffCount.hashCode() : 0);
        result = 31 * result + (clinicalStaffCount != null ? clinicalStaffCount.hashCode() : 0);
        result = 31 * result + (nonClinicalStaffCount != null ? nonClinicalStaffCount.hashCode() : 0);
        result = 31 * result + (seniorManagersName != null ? seniorManagersName.hashCode() : 0);
        result = 31 * result + (crisisManagersNamesAndPhones != null ? crisisManagersNamesAndPhones.hashCode() : 0);
        result = 31 * result + (organizationalAffiliation != null ? organizationalAffiliation.hashCode() : 0);
        result = 31 * result + (serviceType != null ? serviceType.hashCode() : 0);
        result = 31 * result + (roleInHealthcareNetwork != null ? roleInHealthcareNetwork.hashCode() : 0);
        result = 31 * result + (roleInCrisisAndDisasters != null ? roleInCrisisAndDisasters.hashCode() : 0);
        result = 31 * result + (structureType != null ? structureType.hashCode() : 0);
        result = 31 * result + (servedPopulation != null ? servedPopulation.hashCode() : 0);
        result = 31 * result + (influenceArea != null ? influenceArea.hashCode() : 0);
        result = 31 * result + (lastEvaluationRating != null ? lastEvaluationRating.hashCode() : 0);
        result = 31 * result + (observerL2 != null ? observerL2.hashCode() : 0);
        result = 31 * result + (observerL2Comment != null ? observerL2Comment.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "hospitalByHospitalId")
//    public Collection<Building> getBuildingsById() {
//        return buildingsById;
//    }
//
//    public void setBuildingsById(Collection<Building> buildingsById) {
//        this.buildingsById = buildingsById;
//    }
//
//    @OneToMany(mappedBy = "hospitalByHospitalId")
//    public Collection<CandidateLocation> getCandidateLocationsById() {
//        return candidateLocationsById;
//    }
//
//    public void setCandidateLocationsById(Collection<CandidateLocation> candidateLocationsById) {
//        this.candidateLocationsById = candidateLocationsById;
//    }

//    @ManyToOne
//    @JoinColumn(name = "observer_l2", referencedColumnName = "id")
//    public User getUserByObserverL2() {
//        return userByObserverL2;
//    }
//
//    public void setUserByObserverL2(User userByObserverL2) {
//        this.userByObserverL2 = userByObserverL2;
//    }
//
//    @OneToMany(mappedBy = "hospitalByHospitalId")
//    public Collection<HospitalEntrance> getHospitalEntrancesById() {
//        return hospitalEntrancesById;
//    }
//
//    public void setHospitalEntrancesById(Collection<HospitalEntrance> hospitalEntrancesById) {
//        this.hospitalEntrancesById = hospitalEntrancesById;
//    }

    @Basic
    @Column(name = "executive_summary",columnDefinition="TEXT")
    public String getExecutiveSummary() {
        return executiveSummary;
    }

    public void setExecutiveSummary(String executiveSummary) {
        this.executiveSummary = executiveSummary;
    }

    @Basic
    @Column(name = "observations_4_structural_assessment",columnDefinition="TEXT")
    public String getObservations4StructuralAssessment() {
        return observations4StructuralAssessment;
    }

    public void setObservations4StructuralAssessment(String observations4StructuralAssessment) {
        this.observations4StructuralAssessment = observations4StructuralAssessment;
    }

    @Basic
    @Column(name = "observations_4_non_structural_assessment",columnDefinition="TEXT")
    public String getObservations4NonStructuralAssessment() {
        return observations4NonStructuralAssessment;
    }

    public void setObservations4NonStructuralAssessment(String observations4NonStructuralAssessment) {
        this.observations4NonStructuralAssessment = observations4NonStructuralAssessment;
    }

    @Basic
    @Column(name = "observations_4_organizational_assessment",columnDefinition="TEXT")
    public String getObservations4OrganizationalAssessment() {
        return observations4OrganizationalAssessment;
    }

    public void setObservations4OrganizationalAssessment(String observations4OrganizationalAssessment) {
        this.observations4OrganizationalAssessment = observations4OrganizationalAssessment;
    }

    @Basic
    @Column(name = "recommendations_4_structural_assessment",columnDefinition="TEXT")
    public String getRecommendations4StructuralAssessment() {
        return recommendations4StructuralAssessment;
    }

    public void setRecommendations4StructuralAssessment(String recommendations4StructuralAssessment) {
        this.recommendations4StructuralAssessment = recommendations4StructuralAssessment;
    }

    @Basic
    @Column(name = "recommendations_4_non_structural_assessment" ,columnDefinition="TEXT")
    public String getRecommendations4NonStructuralAssessment() {
        return recommendations4NonStructuralAssessment;
    }

    public void setRecommendations4NonStructuralAssessment(String recommendations4NonStructuralAssessment) {
        this.recommendations4NonStructuralAssessment = recommendations4NonStructuralAssessment;
    }

    @Basic
    @Column(name = "recommendations_4_organizational_assessment" ,columnDefinition="TEXT")
    public String getRecommendations4OrganizationalAssessment() {
        return recommendations4OrganizationalAssessment;
    }

    public void setRecommendations4OrganizationalAssessment(String recommendations4OrganizationalAssessment) {
        this.recommendations4OrganizationalAssessment = recommendations4OrganizationalAssessment;
    }

    @Basic
    @Column(name = "overall_recommendations",columnDefinition="TEXT")
    public String getOverallRecommendations() {
        return overallRecommendations;
    }

    public void setOverallRecommendations(String overallRecommendations) {
        this.overallRecommendations = overallRecommendations;
    }

    @Basic
    @Column(name = "LEVEL_OF_IMPORTANCE")
    public String getLevelOfImportance() {
        return LevelOfImportance;
    }

    public void setLevelOfImportance(String levelOfImportance) {
        LevelOfImportance = levelOfImportance;
    }

    @Basic
    @Column(name = "HAZARD_LEVELS")
    public String getHazardLevels() {
        return HazardLevels;
    }

    public void setHazardLevels(String hazardLevels) {
        HazardLevels = hazardLevels;
    }

    @Transient
    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }
}
