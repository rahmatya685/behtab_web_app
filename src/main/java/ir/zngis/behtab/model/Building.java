package ir.zngis.behtab.model;


import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import ir.zngis.behtab.building.dto.BuildingMeta;
import ir.zngis.behtab.building.dto.BuildingMobile;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "BUILDING")
@Table(name = "BUILDING")
public class Building extends BaseEntity {
    private String rowGuid;
    private String name;
    private Geometry geom;
    private String comment;
    private Integer hospitalId;
    private Hospital hospitalByHospitalId;

    public static Building create(BuildingMobile buildingMobile) {
        Building building = new Building();
        building.rowGuid = buildingMobile.getRowGuid();
        building.hospitalByHospitalId = buildingMobile.getHospitalByHospitalId();
        building.name = buildingMobile.getName();
        building.comment = buildingMobile.getComment();
        building.geom = buildingMobile.getGeom();
        building.setEditorUser(buildingMobile.getEditorUser());
        building.hospitalId = buildingMobile.getHospitalId();
        building.setModifyDate(new Date());
        return building;
    }

    public static Building create(BuildingMeta buildingMeta) {
        Building building = new Building();
        building.rowGuid = buildingMeta.getRowGuid();
        building.hospitalByHospitalId = buildingMeta.getHospitalByHospitalId();
        building.name = buildingMeta.getName();
        building.comment = buildingMeta.getComment();
        building.setEditorUser(buildingMeta.getEditorUser());
        building.hospitalId = buildingMeta.getHospitalId();
        building.setModifyDate(new Date());
        building.comment = buildingMeta.getComment();
        building.constructionYear = buildingMeta.getConstructionYear();
        building.extension = buildingMeta.getExtension();
        building.floorArea = buildingMeta.getFloorArea();
        building.totalConstructionArea = buildingMeta.getTotalConstructionArea();
        building.typology = buildingMeta.getTypology();
        building.numFloor = buildingMeta.getNumFloor();
        return building;
    }

    public void update(BuildingMobile buildingMobile) {
        this.rowGuid = buildingMobile.getRowGuid();
        this.hospitalByHospitalId = buildingMobile.getHospitalByHospitalId();
        this.name = buildingMobile.getName();
        this.comment = buildingMobile.getComment();
        this.geom = buildingMobile.getGeom();
        this.setEditorUser(buildingMobile.getEditorUser());
        this.hospitalId = buildingMobile.getHospitalId();
        this.setModifyDate(new Date());
    }

    public void update(BuildingMeta buildingMeta) {
        this.name = buildingMeta.getName();
        this.comment = buildingMeta.getComment();
        this.constructionYear = buildingMeta.getConstructionYear();
        this.extension = buildingMeta.getExtension();
        this.floorArea = buildingMeta.getFloorArea();
        this.totalConstructionArea = buildingMeta.getTotalConstructionArea();
        this.typology = buildingMeta.getTypology();
        this.numFloor = buildingMeta.getNumFloor();
        this.setEditorUser(buildingMeta.getEditorUser());
        this.setModifyDate(new Date());
    }


    //SQ.M.
    private double totalConstructionArea;

    //SQ.M.
    private double floorArea;

    private Integer typology;

    private String constructionYear;

    private int numFloor;

    private Boolean extension;

    @Basic
    @Column(name = "total_construction_area")
    public double getTotalConstructionArea() {
        return totalConstructionArea;
    }

    public void setTotalConstructionArea(Double t) {
        if (t == null) {
            this.totalConstructionArea = -1;
        } else {
            this.totalConstructionArea = t;
        }
    }

    @Basic
    @Column(name = "floor_area")
    public double getFloorArea() {
        return floorArea;
    }

    public void setFloorArea(Double f) {
        if (f == null) {
            this.floorArea = -1;
        } else {
            this.floorArea = f;
        }
    }

    @Basic
    @Column(name = "typology")
    public Integer getTypology() {
        return typology;
    }

    public void setTypology(Integer t) {
        if (t == null)
            this.typology = -1;
        else
            this.typology = t;
    }

    @Basic
    @Column(name = "construction_year")
    public String getConstructionYear() {
        return constructionYear;
    }

    public void setConstructionYear(String constructionYear) {
        this.constructionYear = constructionYear;
    }

    @Basic
    @Column(name = "num_floor")
    public int getNumFloor() {
        return numFloor;
    }

    public void setNumFloor(Integer n) {

        if (n == null)
            this.numFloor = -1;
        else
            numFloor = n;
    }

    @Basic
    @Column(name = "extension")
    public Boolean isExtension() {
        return extension;
    }


    public void setExtension(Boolean e) {
        if (e == null)
            this.extension = false;
        else
            this.extension = e;

    }


    //    private Collection<ClinicalUnit> clinicalUnitsById;
//    private Collection<Evaluation> evaluationsById;
//    private Collection<NonClinicalUnit> nonClinicalUnitsById;
//    private Collection<SurgeryDepartment> surgeryDepartmentsById;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }

    @Basic
    @Column(name = "name", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "geom")
//    @Type(type="org.hibernate.spatial.GeometryType")
//    @Convert(converter = GeojsonConvertor.class)
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Polygon geom) {
        this.geom = geom;
    }

    @Basic
    @Column(name = "comment", length = 10485760)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Transient
    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Building building = (Building) o;

        if (rowGuid != building.rowGuid) return false;
//        if (name != null ? !name.equals(building.name) : building.name != null) return false;
//        if (geom != null ? !geom.equals(building.geom) : building.geom != null) return false;
//        if (comment != null ? !comment.equals(building.comment) : building.comment != null) return false;
//        if (hospitalId != null ? !hospitalId.equals(building.hospitalId) : building.hospitalId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (geom != null ? geom.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (hospitalId != null ? hospitalId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "hospital_id", referencedColumnName = "id")
    public Hospital getHospitalByHospitalId() {
        return hospitalByHospitalId;
    }

    public void setHospitalByHospitalId(Hospital hospitalByHospitalId) {
        this.hospitalByHospitalId = hospitalByHospitalId;
        if (hospitalByHospitalId != null)
            hospitalId = hospitalByHospitalId.getId();
    }
//
//    @OneToMany(mappedBy = "buildingByBuildingId")
//    public Collection<ClinicalUnit> getClinicalUnitsById() {
//        return clinicalUnitsById;
//    }
//
//    public void setClinicalUnitsById(Collection<ClinicalUnit> clinicalUnitsById) {
//        this.clinicalUnitsById = clinicalUnitsById;
//    }
//
//    @OneToMany(mappedBy = "buildingByBuildingId")
//    public Collection<Evaluation> getEvaluationsById() {
//        return evaluationsById;
//    }
//
//    public void setEvaluationsById(Collection<Evaluation> evaluationsById) {
//        this.evaluationsById = evaluationsById;
//    }
//
//    @OneToMany(mappedBy = "buildingByBuildingId")
//    public Collection<NonClinicalUnit> getNonClinicalUnitsById() {
//        return nonClinicalUnitsById;
//    }
//
//    public void setNonClinicalUnitsById(Collection<NonClinicalUnit> nonClinicalUnitsById) {
//        this.nonClinicalUnitsById = nonClinicalUnitsById;
//    }
//
//    @OneToMany(mappedBy = "buildingByBuildingId")
//    public Collection<SurgeryDepartment> getSurgeryDepartmentsById() {
//        return surgeryDepartmentsById;
//    }
//
//    public void setSurgeryDepartmentsById(Collection<SurgeryDepartment> surgeryDepartmentsById) {
//        this.surgeryDepartmentsById = surgeryDepartmentsById;
//    }


}
