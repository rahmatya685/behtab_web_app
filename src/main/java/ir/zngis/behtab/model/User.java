package ir.zngis.behtab.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ir.zngis.behtab.util.EntityStatus;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;

import static java.util.Objects.requireNonNull;

@Entity
@Table(name = "user", schema = "public", catalog = "behtab")
public class User extends BaseEntity implements UserDetails {
     private String name;
    private String pass;
    private String userName;
    private String token;

    //    private Collection<Evaluation> evaluationsById;
//    private Collection<Hospital> hospitalsById;
    private Collection<UserRole> authorities;

    private Collection<LoginDevice> loginDevices;


    public User() {
    }

    @JsonCreator
    public User(@JsonProperty("id") final int id,
                @JsonProperty("username") final String username,
                @JsonProperty("password") final String password) {
        super();
        this.id = requireNonNull(id);
        this.userName = requireNonNull(username);
        this.pass = password;
    }

    private Integer id = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "pass", nullable = false)
    @Override
//    @Pattern(regexp = "^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z])|(?=.{8,})(?=.*\\d)(?=.*[!@#$%^&])|(?=.{8,})(?=.*[a-zA-Z])(?=.*[!@#$%^&]).*$")
//    @JsonIgnore
    public String getPassword() {
        return pass;
    }

    public void setPassword(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "user_name", unique = true)
    @Override
    public String getUsername() {
        return userName;
    }


    public void setUsername(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "token", unique = true)
    @JsonIgnore
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (pass != null ? !pass.equals(user.pass) : user.pass != null) return false;
        if (userName != null ? !userName.equals(user.userName) : user.userName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (pass != null ? pass.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "userByUserId")
//    @JsonIgnore
//    public Collection<Evaluation> getEvaluationsById() {
//        return evaluationsById;
//    }
//
//    public void setEvaluationsById(Collection<Evaluation> evaluationsById) {
//        this.evaluationsById = evaluationsById;
//    }

//    @OneToMany(mappedBy = "userByObserverL2")
//    @JsonIgnore
//    public Collection<Hospital> getHospitalsById() {
//        return hospitalsById;
//    }
//
//    public void setHospitalsById(Collection<Hospital> hospitalsById) {
//        this.hospitalsById = hospitalsById;
//    }
//

    public void setAuthorities(Collection<UserRole> userRolesById) {
        this.authorities = userRolesById;
    }

    @Override
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    public Collection<UserRole> getAuthorities() {
        return authorities;
    }

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonIgnore
    public Collection<LoginDevice> getLoginDevices() {
        return loginDevices;
    }

    public void setLoginDevices(Collection<LoginDevice> loginDevices) {
        this.loginDevices = loginDevices;
    }

    @Override
    @JsonIgnore
    @Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return true;
    }


}
