package ir.zngis.behtab.model;


import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ChartInfo4Category {

    @Id
    @Basic
    @Column(name = "category_name")
    private String questionCategory;



    @Column(name = "module_name")
    private String moduleName;

    @Column(name = "row_id")
    private int rowId;


    @Basic
    @Column(name = "reliability")
    private double reliability;



    @Basic
    @Column(name = "Weighted_Avearage_Scrore")
    private double weightedAvgScore;


    @Basic
    @Column(name = "lst")
    private double lst;


    public String getQuestionCategory() {
        return questionCategory;
    }

    public void setQuestionCategory(String questionCategory) {
        this.questionCategory = questionCategory;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public double getReliability() {
        return reliability;
    }

    public void setReliability(double reliability) {
        this.reliability = reliability;
    }

    public double getWeightedAvgScore() {
        return weightedAvgScore;
    }

    public void setWeightedAvgScore(double weightedAvgScore) {
        this.weightedAvgScore = weightedAvgScore;
    }

    public double getLst() {
        return lst;
    }

    public void setLst(double lst) {
        this.lst = lst;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }
}
