package ir.zngis.behtab.model;

public class UserRoleType {

    public UserRoleType(int type, String typeNameFa,String typeNameEn   ) {
        Type = type;
        this.typeNameFa = typeNameFa;
        this.typeNameEn = typeNameEn;

    }

    private int Type;

    private String typeNameEn;

    private String typeNameFa;

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getTypeNameEn() {
        return typeNameEn;
    }

    public void setTypeNameEn(String typeNameEn) {
        this.typeNameEn = typeNameEn;
    }

    public String getTypeNameFa() {
        return typeNameFa;
    }

    public void setTypeNameFa(String typeNameFa) {
        this.typeNameFa = typeNameFa;
    }
}
