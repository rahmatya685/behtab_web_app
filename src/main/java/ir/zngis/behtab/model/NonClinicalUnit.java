package ir.zngis.behtab.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "non_clinical_unit", schema = "public", catalog = "behtab")
public class NonClinicalUnit extends BaseEntity {
    private String rowGuid;
    private int category;
    private int name;
    private int realStaffCount;
    private Integer predictedStaffCount;
    private String observation;
    private String buildingId;
    private UnitCategory unitCategoryByCategory;
    private UnitSubCategory unitSubCategoryByName;
    private Building buildingByBuildingId;

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }

    //    @Basic
//    @Column(name = "category")
    @Transient
    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    //    @Basic
//    @Column(name =
    @Transient
    public int getName() {
        return name;
    }

    @Transient
    public void setName(int name) {
        this.name = name;
    }

    @Basic
    @Column(name = "real_staff_count")
    public int getRealStaffCount() {
        return realStaffCount;
    }

    public void setRealStaffCount(int realStaffCount) {
        this.realStaffCount = realStaffCount;
    }

    @Basic
    @Column(name = "predicted_staff_count")
    public Integer getPredictedStaffCount() {
        return predictedStaffCount;
    }

    public void setPredictedStaffCount(Integer predictedStaffCount) {
        this.predictedStaffCount = predictedStaffCount;
    }

    @Basic
    @Column(name = "observation", length = 10485760)
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

//    @Basic
//    @Column(name = "building_id")
    @Transient
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NonClinicalUnit that = (NonClinicalUnit) o;

        if (rowGuid != that.rowGuid) return false;
        if (name != that.name) return false;
        if (realStaffCount != that.realStaffCount) return false;
        if (predictedStaffCount != null ? !predictedStaffCount.equals(that.predictedStaffCount) : that.predictedStaffCount != null)
            return false;
        if (observation != null ? !observation.equals(that.observation) : that.observation != null) return false;
        if (buildingId != null ? !buildingId.equals(that.buildingId) : that.buildingId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + name;
        result = 31 * result + realStaffCount;
        result = 31 * result + (predictedStaffCount != null ? predictedStaffCount.hashCode() : 0);
        result = 31 * result + (observation != null ? observation.hashCode() : 0);
        result = 31 * result + (buildingId != null ? buildingId.hashCode() : 0);
        return result;
    }


    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "id", nullable = false)
    public UnitCategory getUnitCategoryByCategory() {
        return unitCategoryByCategory;
    }

    public void setUnitCategoryByCategory(UnitCategory unitCategoryByCategory) {
        this.unitCategoryByCategory = unitCategoryByCategory;
        if (unitCategoryByCategory!= null)
            this.category = unitCategoryByCategory.getId();
    }

    @ManyToOne
    @JoinColumn(name = "name", referencedColumnName = "id", nullable = false)
    public UnitSubCategory getUnitSubCategoryByName() {
        return unitSubCategoryByName;
    }

    public void setUnitSubCategoryByName(UnitSubCategory unitSubCategoryByName) {
        this.unitSubCategoryByName = unitSubCategoryByName;
        if (unitSubCategoryByName!= null)
            this.name = unitSubCategoryByName.getId();
    }

    @ManyToOne
    @JoinColumn(name = "building_id", referencedColumnName = "id")
    public Building getBuildingByBuildingId() {
        return buildingByBuildingId;
    }

    public void setBuildingByBuildingId(Building buildingByBuildingId) {
        this.buildingByBuildingId = buildingByBuildingId;
        if (buildingByBuildingId!= null)
            this.buildingId = buildingByBuildingId.getRowGuid();
    }
}
