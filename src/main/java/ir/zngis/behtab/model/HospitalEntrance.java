package ir.zngis.behtab.model;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Point;

import javax.persistence.*;

@Entity
@Table(name = "hospital_entrance", schema = "public", catalog = "behtab")
public class HospitalEntrance extends BaseEntity{

    private String rowGuid;

    private String name;
    private Integer type;
    private Point accessPoint;
    private Integer hospitalId;
    private Hospital hospitalByHospitalId;


    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }



    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "geom")
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Point getAccessPoint() {
        return accessPoint;
    }

    public void setAccessPoint(Point accessPoint) {
        this.accessPoint = accessPoint;
    }

//    @Basic
//    @Column(name = "hospital_id")

    @Transient
    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HospitalEntrance that = (HospitalEntrance) o;

        if (!rowGuid.contentEquals(that.rowGuid) ) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (accessPoint != null ? !accessPoint.equals(that.accessPoint) : that.accessPoint != null) return false;
        if (hospitalId != null ? !hospitalId.equals(that.hospitalId) : that.hospitalId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (accessPoint != null ? accessPoint.hashCode() : 0);
        result = 31 * result + (hospitalId != null ? hospitalId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "hospital_id", referencedColumnName = "id")
    public Hospital getHospitalByHospitalId() {
        return hospitalByHospitalId;
    }

    public void setHospitalByHospitalId(Hospital hospitalByHospitalId) {
        this.hospitalByHospitalId = hospitalByHospitalId;
        if (hospitalByHospitalId!= null)
            this.hospitalId = hospitalByHospitalId.getId();
    }
}
