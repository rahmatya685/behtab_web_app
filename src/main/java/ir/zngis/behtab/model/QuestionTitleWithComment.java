package ir.zngis.behtab.model;

import javax.persistence.*;


public class QuestionTitleWithComment {

    private String qustionTitle;
    private String comment;
    private int id;
    private int questionId;
    private int questionCategoryId;


    @Id
    @Transient
    @Column(name = "id" )
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    @Basic
    @Transient
    @Column(name = "question_title_en")
    public String getQustionTitle() {
        return qustionTitle;
    }

    public void setQustionTitle(String qustionTitle) {
        this.qustionTitle = qustionTitle;
    }

    @Basic
    @Transient
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Basic
    @Transient
    @Column(name = "question_id")
    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    @Basic
    @Transient
    @Column(name = "category_id")
    public int getQuestionCategoryId() {
        return questionCategoryId;
    }

    public void setQuestionCategoryId(int questionCategoryId) {
        this.questionCategoryId = questionCategoryId;
    }
}
