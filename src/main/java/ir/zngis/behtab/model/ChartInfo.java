package ir.zngis.behtab.model;


import javax.persistence.*;


@Entity
public class ChartInfo {


    @Id
    @Basic
    @Column(name = "module_name")
    private String moduleName;


    @Basic
    @Column(name = "reliability")
    private double reliability;



    @Basic
    @Column(name = "Weighted_Avearage_Scrore")
    private double weightedAvgScore;


    @Basic
    @Column(name = "lst")
    private double lst;


    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public double getReliability() {
        return reliability;
    }

    public void setReliability(double reliability) {
        this.reliability = reliability;
    }

    public double getWeightedAvgScore() {
        return weightedAvgScore;
    }

    public void setWeightedAvgScore(double weightedAvgScore) {
        this.weightedAvgScore = weightedAvgScore;
    }

    public double getLst() {
        return lst;
    }

    public void setLst(double lst) {
        this.lst = lst;
    }
}
