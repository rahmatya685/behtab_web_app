package ir.zngis.behtab.model;

import javax.persistence.*;

@Entity
public class ExposureIndex extends BaseSetting {

    private int id ;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "EXPOSURE_INDEX")
    private double exposureIndex;


    public void setId(Integer id) {
        this.id = id;
    }

    public double getExposureIndex() {
        return exposureIndex;
    }

    public void setExposureIndex(double exposureIndex) {
        this.exposureIndex = exposureIndex;
    }
}
