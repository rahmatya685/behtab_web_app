package ir.zngis.behtab.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
public class Evaluation extends BaseEntity {
    private String rowGuid;
    private boolean hasObserved;
    private Integer riskLevel;
    private String comment;
    private String buildingId;
    private Integer questionId;
    private String surgeryDepartmentId;
    private String clinicalUnitId;
    private String nonClinicalUnitId;
    private Integer userId;
    private Building buildingByBuildingId;
    private Question questionByQuestionId;
    private SurgeryDepartment surgeryDepartmentBySurgeryDepartmentId;
    private ClinicalUnit clinicalUnitByClinicalUnitId;
    private NonClinicalUnit nonClinicalUnitByNonClinicalUnitId;

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }

    @Basic
    @Column(name = "has_observed")
    public boolean getHasObserved() {
        return hasObserved;
    }

    public void setHasObserved(boolean hasObserved) {
        this.hasObserved = hasObserved;
    }

    @Basic
    @Column(name = "risk_level")
    public Integer getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(Integer riskLevel) {
        this.riskLevel = riskLevel;
    }

    @Basic
    @Column(name = "comment", length = 10485760)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    //    @Basic
//    @Column(name = "building_id")
    @Transient
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

//    @Basic
//    @Column(name = "question_id")

    @Transient
    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }
//
//    @Basic
//    @Column(name = "surgery_department_id")

    @Transient
    public String getSurgeryDepartmentId() {
        return surgeryDepartmentId;
    }

    public void setSurgeryDepartmentId(String surgeryDepartmentId) {
        this.surgeryDepartmentId = surgeryDepartmentId;
    }

//    @Basic
//    @Column(name = "clinical_unit_id")

    @Transient
    public String getClinicalUnitId() {
        return clinicalUnitId;
    }

    public void setClinicalUnitId(String clinicalUnitId) {
        this.clinicalUnitId = clinicalUnitId;
    }
//
//    @Basic
//    @Column(name = "non_clinical_unit_id")


    @Transient
    public String getNonClinicalUnitId() {
        return nonClinicalUnitId;
    }

    public void setNonClinicalUnitId(String nonClinicalUnitId) {
        this.nonClinicalUnitId = nonClinicalUnitId;
    }
//
//    @Basic
//    @Column(name = "user_id")


    @Transient
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Evaluation that = (Evaluation) o;

        if (rowGuid != that.rowGuid) return false;
        if (riskLevel != null ? !riskLevel.equals(that.riskLevel) : that.riskLevel != null) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        if (buildingId != null ? !buildingId.equals(that.buildingId) : that.buildingId != null) return false;
        if (questionId != null ? !questionId.equals(that.questionId) : that.questionId != null) return false;
        if (surgeryDepartmentId != null ? !surgeryDepartmentId.equals(that.surgeryDepartmentId) : that.surgeryDepartmentId != null)
            return false;
        if (clinicalUnitId != null ? !clinicalUnitId.equals(that.clinicalUnitId) : that.clinicalUnitId != null)
            return false;
        if (nonClinicalUnitId != null ? !nonClinicalUnitId.equals(that.nonClinicalUnitId) : that.nonClinicalUnitId != null)
            return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + (riskLevel != null ? riskLevel.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (buildingId != null ? buildingId.hashCode() : 0);
        result = 31 * result + (questionId != null ? questionId.hashCode() : 0);
        result = 31 * result + (surgeryDepartmentId != null ? surgeryDepartmentId.hashCode() : 0);
        result = 31 * result + (clinicalUnitId != null ? clinicalUnitId.hashCode() : 0);
        result = 31 * result + (nonClinicalUnitId != null ? nonClinicalUnitId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }


    @ManyToOne
    @JoinColumn(name = "building_id", referencedColumnName = "id")
    public Building getBuildingByBuildingId() {
        return buildingByBuildingId;
    }

    public void setBuildingByBuildingId(Building buildingByBuildingId) {
        this.buildingByBuildingId = buildingByBuildingId;
        if (buildingByBuildingId != null)
            this.buildingId = buildingByBuildingId.getRowGuid();
    }

    @ManyToOne
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    public Question getQuestionByQuestionId() {
        return questionByQuestionId;
    }

    public void setQuestionByQuestionId(Question questionByQuestionId) {
        this.questionByQuestionId = questionByQuestionId;
        if (questionByQuestionId != null)
            this.questionId = questionByQuestionId.getId();
    }

    @ManyToOne
    @JoinColumn(name = "surgery_department_id", referencedColumnName = "id")
    public SurgeryDepartment getSurgeryDepartmentBySurgeryDepartmentId() {
        return surgeryDepartmentBySurgeryDepartmentId;
    }

    public void setSurgeryDepartmentBySurgeryDepartmentId(SurgeryDepartment surgeryDepartmentBySurgeryDepartmentId) {
        this.surgeryDepartmentBySurgeryDepartmentId = surgeryDepartmentBySurgeryDepartmentId;
        if (surgeryDepartmentBySurgeryDepartmentId != null) {
            this.surgeryDepartmentId = surgeryDepartmentBySurgeryDepartmentId.getRowGuid();
        }
    }

    @ManyToOne
    @JoinColumn(name = "clinical_unit_id", referencedColumnName = "id")
    public ClinicalUnit getClinicalUnitByClinicalUnitId() {
        return clinicalUnitByClinicalUnitId;
    }

    public void setClinicalUnitByClinicalUnitId(ClinicalUnit clinicalUnitByClinicalUnitId) {
        this.clinicalUnitByClinicalUnitId = clinicalUnitByClinicalUnitId;
        if (clinicalUnitByClinicalUnitId != null)
            this.clinicalUnitId = clinicalUnitByClinicalUnitId.getRowGuid();
    }

    @ManyToOne
    @JoinColumn(name = "non_clinical_unit_id", referencedColumnName = "id")
    public NonClinicalUnit getNonClinicalUnitByNonClinicalUnitId() {
        return nonClinicalUnitByNonClinicalUnitId;
    }

    public void setNonClinicalUnitByNonClinicalUnitId(NonClinicalUnit nonClinicalUnitByNonClinicalUnitId) {
        this.nonClinicalUnitByNonClinicalUnitId = nonClinicalUnitByNonClinicalUnitId;
        if (nonClinicalUnitByNonClinicalUnitId != null)
            this.nonClinicalUnitId = nonClinicalUnitByNonClinicalUnitId.getRowGuid();
    }


}
