package ir.zngis.behtab.model;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Polygon;

import javax.persistence.*;

@Entity
@Table(name = "candidate_location")
public class CandidateLocation extends BaseEntity{
    private String rowGuid;
    private String name;
    private Double area;
    private Polygon geom;
    private String landUse;
    private Boolean  hasWater;
    private Boolean  hasElectricity;
    private Boolean  hasTelephone;
    private Boolean  hasLandfillManagement;
    private Boolean  hasHeating;
    private Boolean  hasCooling;
    private Boolean  hasVentilation;
    private Integer hospitalId;
    private String observation;
    private Hospital hospitalByHospitalId;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "geom" )
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    public Polygon getGeom() {
        return geom;
    }

    public void setGeom(Polygon geom) {
        this.geom = geom;
    }


    @Basic
    @Column(name = "area")
    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Basic
    @Column(name = "land_use")
    public String getLandUse() {
        return landUse;
    }

    public void setLandUse(String landUse) {
        this.landUse = landUse;
    }

    @Basic
    @Column(name = "has_water")
    public Boolean  getHasWater() {
        return hasWater;
    }

    public void setHasWater(Boolean  hasWater) {
        this.hasWater = hasWater;
    }

    @Basic
    @Column(name = "has_electricity")
    public Boolean  getHasElectricity() {
        return hasElectricity;
    }

    public void setHasElectricity(Boolean  hasElectricity) {
        this.hasElectricity = hasElectricity;
    }

    @Basic
    @Column(name = "has_telephone")
    public Boolean  getHasTelephone() {
        return hasTelephone;
    }

    public void setHasTelephone(Boolean  hasTelephone) {
        this.hasTelephone = hasTelephone;
    }

    @Basic
    @Column(name = "has_landfill_management")
    public Boolean  getHasLandfillManagement() {
        return hasLandfillManagement;
    }

    public void setHasLandfillManagement(Boolean  hasLandfillManagement) {
        this.hasLandfillManagement = hasLandfillManagement;
    }

    @Basic
    @Column(name = "has_heating")
    public Boolean   getHasHeating() {
        return hasHeating;
    }

    public void setHasHeating(Boolean  hasHeating) {
        this.hasHeating = hasHeating;
    }

    @Basic
    @Column(name = "has_cooling")
    public Boolean  getHasCooling() {
        return hasCooling;
    }

    public void setHasCooling(Boolean  hasCooling) {
        this.hasCooling = hasCooling;
    }

    @Basic
    @Column(name = "has_ventilation")
    public Boolean  getHasVentilation() {
        return hasVentilation;
    }

    public void setHasVentilation(Boolean  hasVentilation) {
        this.hasVentilation = hasVentilation;

    }

//    @Basic
//    @Column(name = "hospital_id")

    @Transient
    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    @Basic
    @Column(name = "observation",length=10485760)
    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CandidateLocation that = (CandidateLocation) o;

        if (rowGuid != that.rowGuid) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (geom != null ? !geom.equals(that.geom) : that.geom != null) return false;
        if (landUse != null ? !landUse.equals(that.landUse) : that.landUse != null) return false;
        if (hasWater != null ? !hasWater.equals(that.hasWater) : that.hasWater != null) return false;
        if (hasElectricity != null ? !hasElectricity.equals(that.hasElectricity) : that.hasElectricity != null)
            return false;
        if (hasTelephone != null ? !hasTelephone.equals(that.hasTelephone) : that.hasTelephone != null) return false;
        if (hasLandfillManagement != null ? !hasLandfillManagement.equals(that.hasLandfillManagement) : that.hasLandfillManagement != null)
            return false;
        if (hasHeating != null ? !hasHeating.equals(that.hasHeating) : that.hasHeating != null) return false;
        if (hasCooling != null ? !hasCooling.equals(that.hasCooling) : that.hasCooling != null) return false;
        if (hasVentilation != null ? !hasVentilation.equals(that.hasVentilation) : that.hasVentilation != null)
            return false;
        if (hospitalId != null ? !hospitalId.equals(that.hospitalId) : that.hospitalId != null) return false;
        if (observation != null ? !observation.equals(that.observation) : that.observation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (geom != null ? geom.hashCode() : 0);
        result = 31 * result + (landUse != null ? landUse.hashCode() : 0);
        result = 31 * result + (hasWater != null ? hasWater.hashCode() : 0);
        result = 31 * result + (hasElectricity != null ? hasElectricity.hashCode() : 0);
        result = 31 * result + (hasTelephone != null ? hasTelephone.hashCode() : 0);
        result = 31 * result + (hasLandfillManagement != null ? hasLandfillManagement.hashCode() : 0);
        result = 31 * result + (hasHeating != null ? hasHeating.hashCode() : 0);
        result = 31 * result + (hasCooling != null ? hasCooling.hashCode() : 0);
        result = 31 * result + (hasVentilation != null ? hasVentilation.hashCode() : 0);
        result = 31 * result + (hospitalId != null ? hospitalId.hashCode() : 0);
        result = 31 * result + (observation != null ? observation.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "hospital_id", referencedColumnName = "id")
    public Hospital getHospitalByHospitalId() {
        return hospitalByHospitalId;
    }

    public void setHospitalByHospitalId(Hospital hospitalByHospitalId) {
        this.hospitalByHospitalId = hospitalByHospitalId;
        if (hospitalByHospitalId!= null)
            hospitalId = hospitalByHospitalId.getId();
    }
}
