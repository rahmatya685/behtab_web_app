package ir.zngis.behtab.model;

import javax.persistence.*;

@Entity
public class RelativeWeight extends BaseSetting {


    private int id ;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "RELATIVE_WEIGHT")
    private double relativeWeight;

    public double getRelativeWeight() {
        return relativeWeight;
    }

    public void setRelativeWeight(double relativeWeight) {
        this.relativeWeight = relativeWeight;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
