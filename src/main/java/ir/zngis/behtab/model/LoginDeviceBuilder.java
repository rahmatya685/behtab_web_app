package ir.zngis.behtab.model;

public class LoginDeviceBuilder {
    private String ip;
    private User user;

    public LoginDeviceBuilder setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public LoginDeviceBuilder setUser(User user) {
        this.user = user;
        return this;
    }

    public LoginDevice createLoginDevice() {
        return new LoginDevice(ip, user);
    }
}