package ir.zngis.behtab.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "clinical_unit", schema = "public", catalog = "behtab")
public class ClinicalUnit extends BaseEntity {
    private String rowGuid;
    private Integer category;
    private int name;
    private int normalCapacity;
    private int maxCapacity;
    private Integer predictedStaffCount;
    private Integer realStaffCount;
    private Integer isolatedBedCount;
    private String comment;
    private String buildingId;
    private UnitCategory unitCategoryByCategory;
    private UnitSubCategory unitSubCategoryByName;
    private Building buildingByBuildingId;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }

    public void setRowGuid(String id) {
        this.rowGuid = id;
    }

    @Transient
    public Integer getCategory() {
        return category;
    }


    public void setCategory(Integer category) {
        this.category = category;
    }
    @Transient
    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }
    @Transient
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    @Basic
    @Column(name = "normal_capacity")
    public int getNormalCapacity() {
        return normalCapacity;
    }

    public void setNormalCapacity(int normalCapacity) {
        this.normalCapacity = normalCapacity;
    }

    @Basic
    @Column(name = "max_capacity")
    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    @Basic
    @Column(name = "predicted_staff_count")
    public Integer getPredictedStaffCount() {
        return predictedStaffCount;
    }

    public void setPredictedStaffCount(Integer predictedStaffCount) {
        this.predictedStaffCount = predictedStaffCount;
    }

    @Basic
    @Column(name = "real_staff_count")
    public Integer getRealStaffCount() {
        return realStaffCount;
    }

    public void setRealStaffCount(Integer realStaffCount) {
        this.realStaffCount = realStaffCount;
    }

    @Basic
    @Column(name = "isolated_bed_count")
    public Integer getIsolatedBedCount() {
        return isolatedBedCount;
    }

    public void setIsolatedBedCount(Integer isolatedBedCount) {
        this.isolatedBedCount = isolatedBedCount;
    }

    @Basic
    @Column(name = "comment", length = 10485760)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClinicalUnit that = (ClinicalUnit) o;

        if (!rowGuid.equals(that.rowGuid)) return false;
        if (normalCapacity != that.normalCapacity) return false;
        if (maxCapacity != that.maxCapacity) return false;
        if (predictedStaffCount != null ? !predictedStaffCount.equals(that.predictedStaffCount) : that.predictedStaffCount != null)
            return false;
        if (realStaffCount != null ? !realStaffCount.equals(that.realStaffCount) : that.realStaffCount != null)
            return false;
        if (isolatedBedCount != null ? !isolatedBedCount.equals(that.isolatedBedCount) : that.isolatedBedCount != null)
            return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rowGuid.hashCode();
        result = 31 * result + normalCapacity;
        result = 31 * result + maxCapacity;
        result = 31 * result + (predictedStaffCount != null ? predictedStaffCount.hashCode() : 0);
        result = 31 * result + (realStaffCount != null ? realStaffCount.hashCode() : 0);
        result = 31 * result + (isolatedBedCount != null ? isolatedBedCount.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "id", nullable = false)
    public UnitCategory getUnitCategoryByCategory() {
        return unitCategoryByCategory;
    }

    public void setUnitCategoryByCategory(UnitCategory unitCategoryByCategory) {
        this.unitCategoryByCategory = unitCategoryByCategory;
        if (unitCategoryByCategory!= null)
            this.category = unitCategoryByCategory.getId();
    }

    @ManyToOne
    @JoinColumn(name = "name", referencedColumnName = "id", nullable = false)
    @JsonBackReference
    public UnitSubCategory getUnitSubCategoryByName() {
        return unitSubCategoryByName;
    }

    public void setUnitSubCategoryByName(UnitSubCategory unitSubCategoryByName) {
        this.unitSubCategoryByName = unitSubCategoryByName;
        if (unitSubCategoryByName != null)
            name = unitSubCategoryByName.getId();
    }

    @ManyToOne
    @JoinColumn(name = "building_id", referencedColumnName = "id")
    public Building getBuildingByBuildingId() {
        return buildingByBuildingId;
    }

    public void setBuildingByBuildingId(Building buildingByBuildingId) {
        this.buildingByBuildingId = buildingByBuildingId;
        if (buildingByBuildingId != null)
            buildingId = buildingByBuildingId.getRowGuid();
    }

}
