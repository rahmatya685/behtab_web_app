package ir.zngis.behtab.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "unit_sub_category", schema = "public", catalog = "behtab")
public class UnitSubCategory extends BaseEntity{
    private int id;
    private int categoryId;
    private String titleEn;
    private String titleFa;
    private UnitCategory unitCategoryByCategoryId;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    @Basic
//    @Column(name = "category_id")
//    public int getCategoryId() {
//        return categoryId;
//    }
//
//    public void setCategoryId(int categoryId) {
//        this.categoryId = categoryId;
//    }

    @Basic
    @Column(name = "titleEn", length = 10485760)
    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String title_en) {
        this.titleEn = title_en;
    }


    @Basic
    @Column(name = "titleFa", length = 10485760)
    public String getTitleFa() {
        return titleFa;
    }

    public void setTitleFa(String titleFa) {
        this.titleFa = titleFa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnitSubCategory that = (UnitSubCategory) o;

        if (id != that.id) return false;
        if (categoryId != that.categoryId) return false;
        if (titleEn != null ? !titleEn.equals(that.titleEn) : that.titleEn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + categoryId;
        result = 31 * result + (titleEn != null ? titleEn.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    public UnitCategory getUnitCategoryByCategoryId() {
        return unitCategoryByCategoryId;

    }

    public void setUnitCategoryByCategoryId(UnitCategory unitCategoryByCategoryId) {
        this.unitCategoryByCategoryId = unitCategoryByCategoryId;
        this.categoryId = unitCategoryByCategoryId.getId();
    }
}
