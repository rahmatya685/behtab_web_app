package ir.zngis.behtab.model;


import javax.persistence.*;

@Entity
@Table(name = "mobile_app_setting")
public class MobileAppSetting extends BaseEntity {

    private String rowGuid;

    private String name;

    private String value;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    public String getRowGuid() {
        return rowGuid;
    }


    public void setRowGuid(String rowGuid) {
        this.rowGuid = rowGuid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Basic
    @Column(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
