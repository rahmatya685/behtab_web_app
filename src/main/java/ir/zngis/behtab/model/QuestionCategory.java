package ir.zngis.behtab.model;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "question_category", schema = "public", catalog = "behtab")
public class QuestionCategory extends BaseEntity {
    private int id;
    private String moduleName;
    private String categoryTitleFa;
    private String categoryTitleEn;
    private double evaluationAvg;
    //private Collection<Question> questionsById;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "module_name")
    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Basic
    @Column(name = "category_title_fa", length = 10485760)
    public String getCategoryTitleFa() {
        return categoryTitleFa;
    }

    public void setCategoryTitleFa(String categoryTitleFa) {
        this.categoryTitleFa = categoryTitleFa;
    }

    @Basic
    @Column(name = "category_title_en", length = 10485760)
    public String getCategoryTitleEn() {
        return categoryTitleEn;
    }

    public void setCategoryTitleEn(String categoryTitleEn) {
        this.categoryTitleEn = categoryTitleEn;
    }


    @Basic
    @Column(name = "weighted_avg", nullable = true)
    public double getEvaluationAvg() {
        return evaluationAvg;
    }

    public void setEvaluationAvg(@Nullable Double evaluationAvg) {
        if (evaluationAvg != null)
            this.evaluationAvg = evaluationAvg;
        else
            this.evaluationAvg = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionCategory that = (QuestionCategory) o;

        if (id != that.id) return false;
        if (moduleName != null ? !moduleName.equals(that.moduleName) : that.moduleName != null) return false;
        if (categoryTitleFa != null ? !categoryTitleFa.equals(that.categoryTitleFa) : that.categoryTitleFa != null)
            return false;
        if (categoryTitleEn != null ? !categoryTitleEn.equals(that.categoryTitleEn) : that.categoryTitleEn != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (moduleName != null ? moduleName.hashCode() : 0);
        result = 31 * result + (categoryTitleFa != null ? categoryTitleFa.hashCode() : 0);
        result = 31 * result + (categoryTitleEn != null ? categoryTitleEn.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "questionCategoryByCategoryId")
//    public Collection<Question> getQuestionsById() {
//        return questionsById;
//    }

    // public void setQuestionsById(Collection<Question> questionsById) {
//        this.questionsById = questionsById;
//    }
}
