package ir.zngis.behtab.model;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class ProvinceAnalyticalInfo {


    private int id;

    private String name;

    private String ename;

//    private MultiPolygon geom;

    private double riskIndexStructural;

    private double riskIndexNonStructural;

    private double riskIndexOrganizational;

    private double prioritizationIndex;

    private int countHospital;


    @Id
    @Column(name = "objectid", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "ostn_name" )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


//    @Basic
//    @Column(name = "geom")
//    @JsonSerialize(using = GeometrySerializer.class)
//    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
//    public MultiPolygon getGeom() {
//        return geom;
//    }
//
//    public void setGeom(MultiPolygon centerPoint) {
//        this.geom = centerPoint;
//    }

    @Basic
    @Column(name = "risk_index_structural" )
    public double getRiskIndexStructural() {
        return riskIndexStructural;
    }

    public void setRiskIndexStructural(double riskIndexStructural) {
        this.riskIndexStructural = riskIndexStructural;
    }


    @Basic
    @Column(name = "risk_index_non_structural" )
    public double getRiskIndexNonStructural() {
        return riskIndexNonStructural;
    }

    public void setRiskIndexNonStructural(double riskIndexNonStructural) {
        this.riskIndexNonStructural = riskIndexNonStructural;
    }

    @Basic
    @Column(name = "risk_index_organizational" )
    public double getRiskIndexOrganizational() {
        return riskIndexOrganizational;
    }

    public void setRiskIndexOrganizational(double riskIndexOrganizational) {
        this.riskIndexOrganizational = riskIndexOrganizational;
    }


    @Basic
    @Column(name = "prioritization_index" )
    public double getPrioritizationIndex() {
        return prioritizationIndex;
    }

    public void setPrioritizationIndex(double prioritizationIndex) {
        this.prioritizationIndex = prioritizationIndex;
    }


    @Basic
    @Column(name = "count" )
    public int getCountHospital() {
        return countHospital;
    }

    public void setCountHospital(int countHospital) {
        this.countHospital = countHospital;
    }


    @Basic
    @Column(name = "ostn_name_en" )
    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }
}
