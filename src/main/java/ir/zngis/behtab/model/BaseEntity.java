package ir.zngis.behtab.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.zngis.behtab.util.EntityStatus;
import ir.zngis.behtab.util.EntityStatusConvertor;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity {

    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyDate = new Date();

    private EntityStatus status;

    private User editorUser;

//    private Integer editorUserId;
//
//    @Basic
//    @Column(name = "editor_id", insertable = false, updatable = false)
//    public Integer getEditorUserId() {
//        return editorUserId;
//    }
//
//    public void setEditorUserId(Integer editorUserId) {
//        this.editorUserId = editorUserId;
//    }

    @ManyToOne
    @JoinColumn(name = "editor_id", referencedColumnName = "id")
    @JsonIgnore
    public User getEditorUser() {
        return editorUser;
    }

    public void setEditorUser(User userByUserId) {
        this.editorUser = userByUserId;
    }


    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }


    @Column(columnDefinition = "smallint")
    @Convert(converter = EntityStatusConvertor.class)
    public EntityStatus getStatus() {
        return status;
    }

    public void setStatus(EntityStatus status) {
        this.status = status;
    }
}
