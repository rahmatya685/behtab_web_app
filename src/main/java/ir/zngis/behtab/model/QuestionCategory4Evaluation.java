package ir.zngis.behtab.model;

import org.apache.catalina.LifecycleState;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "QuestionCategory4Evaluation")
public class QuestionCategory4Evaluation {
    private int row_id;
    private String building_name;
    private String building_id;
    private double score;
    private int question_category_id;
    private String qeustion_category_title_en;
    private String module_name;


    @Column(name = "building_name")
    public String getBuilding_name() {
        return building_name;
    }

    public void setBuilding_name(String building_name) {
        this.building_name = building_name;
    }

    @Column(name = "building_id")
    public String getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(String building_id) {
        this.building_id = building_id;
    }


    @Column(name = "score")
    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Column(name = "question_category_id")
    public int getQuestion_category_id() {
        return question_category_id;
    }


    public void setQuestion_category_id(int question_category_id) {
        this.question_category_id = question_category_id;
    }

    @Column(name = "qeustion_category_title_en")
    public String getQeustion_category_title_en() {
        return qeustion_category_title_en;
    }

    public void setQeustion_category_title_en(String qeustion_category_title_en) {
        this.qeustion_category_title_en = qeustion_category_title_en;
    }

    @Column(name = "module_name")
    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }


    @Id
    @Column(name = "row_id")
    public int getRow_id() {
        return row_id;
    }

    public void setRow_id(int row_id) {
        this.row_id = row_id;
    }
}
