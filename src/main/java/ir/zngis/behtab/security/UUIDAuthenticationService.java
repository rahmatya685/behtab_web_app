package ir.zngis.behtab.security;

import ir.zngis.behtab.User.LoginDeviceRepo;
import ir.zngis.behtab.User.UserCrudService;
import ir.zngis.behtab.model.LoginDevice;
import ir.zngis.behtab.model.LoginDeviceBuilder;
import ir.zngis.behtab.model.User;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;


@Service
public class UUIDAuthenticationService implements UserAuthenticationService {


    @Autowired
    UserCrudService userCrudService;

    @Autowired
    LoginDeviceRepo loginDeviceRepo;

    @Override
    public Optional<String> register(User u, String ip) {


        Optional<User> user = userCrudService.findByUsername(u.getUsername());
        if (user.isPresent()) {
            return Optional.of(Constants.USER_ALREADY_EXISTS);
        } else {
            u.setToken(UUID.randomUUID().toString());
            User newUser = userCrudService.save(u);
            return login(newUser.getUsername(), newUser.getPassword(), ip);
        }
    }

    @Override
    public Optional<String> deviseLogin(String username, String password, String ip, String setupId) {
        Optional<User> user = userCrudService.findByUsername(username);

        if (user.isPresent()) {
            if (!user.get().getPassword().equals(password)) {
                return Optional.of(Constants.USER_NOT_FOUND);
            }
        }

        //check if user uninstalled application and a new setup id has been sent so find and delete old device login
        if (user.isPresent()) {
            for (LoginDevice loginDevice : user.get().getLoginDevices()) {
                if (loginDevice.getStatus() != EntityStatus.DELETE && loginDevice.getIp().contentEquals(ip)) {
                    if (loginDevice.getInstallationId() != null && !loginDevice.getInstallationId().equals(setupId)) {
                        loginDevice.setStatus(EntityStatus.DELETE);
                        userCrudService.save(user.get());
                    }
                }
            }
        }

        return user.map(u -> {

            int countLoginDevices = (int) u.getLoginDevices().stream().filter(loginDevice -> loginDevice.getStatus() != EntityStatus.DELETE).count();

            if (countLoginDevices > 0) {
                return Optional.of(Constants.MORE_THAN_ONE_USER_LOGIN_IN_NOT_ALLOWED);
            } else {

                LoginDevice lastLogin = null;

                for (LoginDevice loginDevice : u.getLoginDevices()) {
                    if (loginDevice.getIp().contentEquals(ip) && loginDevice.getStatus() != EntityStatus.DELETE)
                        lastLogin = loginDevice;
                }

                if (lastLogin == null) {
                    LoginDevice loginDevice = new LoginDeviceBuilder().createLoginDevice();
                    loginDevice.setDeviceType("");
                    loginDevice.setIp(ip);
                    loginDevice.setUser(user.get());
                    loginDevice.setInstallationId(setupId);

                    loginDeviceRepo.save(loginDevice);

                } else {

                    lastLogin.setLastLoginDate(new Date());
                    loginDeviceRepo.save(lastLogin);
                }

                if (u.getToken() == null) {
                    u.setToken(UUID.randomUUID().toString());
                    userCrudService.save(u);
                }

                return Optional.of(u.getToken());
            }


        }).orElseGet(() -> Optional.of(Constants.USER_NOT_FOUND));

    }

    @Override
    public Optional<String> login(String username, String password, String ip) {
        Optional<User> user = userCrudService.findByUsername(username);
        if (user.isPresent()) {
            if (!user.get().getPassword().equals(password)) {
                return Optional.of(Constants.USER_NOT_FOUND);
            }
        }

        return user.map(u -> {

            if (u.getToken() == null) {
                u.setToken(UUID.randomUUID().toString());
                userCrudService.save(u);
            }

            return Optional.of(u.getToken());


        }).orElseGet(() -> Optional.of(Constants.USER_NOT_FOUND));
    }

    @Override
    public Optional<User> findByToken(String token) {
        return userCrudService.findByToken(token);
    }

    @Override
    public void logout(User user, String ip) {

        for (LoginDevice loginDevice : user.getLoginDevices()) {
            if (loginDevice.getIp().contentEquals(ip)) {

                loginDevice.setStatus(EntityStatus.DELETE);
                loginDevice.setModifyDate(new Date());

                loginDeviceRepo.save(loginDevice);
            }
        }
        userCrudService.save(user);
    }
}
