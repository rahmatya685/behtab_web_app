package ir.zngis.behtab.security;

import ir.zngis.behtab.model.User;

import java.util.Optional;

public interface UserAuthenticationService {


    /**
     * Register in with the given {@code username} and {@code password}.
     *
     * @param user
     * @param ip
     * @return an {@link Optional} of a user when login succeeds
     */
    Optional<String> register(User user, String ip );

    /**
     * Logs in with the given {@code username} and {@code password}.
     *
     * @param username
     * @param password
     * @param ip
     * @return an {@link Optional} of a user when login succeeds
     */
    Optional<String> deviseLogin(String username, String password, String ip,String setupId );



    /**
     * Logs in with the given {@code username} and {@code password}.
     *
     * @param username
     * @param password
     * @param ip
     * @return an {@link Optional} of a user when login succeeds
     */
    Optional<String> login(String username, String password, String ip );

    /**
     * Finds a user by its dao-key.
     *
     * @param token user dao key
     * @return
     */
    Optional<User> findByToken(String token);

    /**
     * Logs out the given input {@code user}.
     *
     * @param user the user to logout
     * @param hostAddress
     */
    void logout(User user, String hostAddress);
}
