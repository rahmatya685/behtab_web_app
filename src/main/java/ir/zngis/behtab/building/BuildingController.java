package ir.zngis.behtab.building;

import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.building.dto.BuildingMeta;
import ir.zngis.behtab.building.dto.BuildingMobile;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.hospitalUnit.ClinicalUnitRepository;
import ir.zngis.behtab.hospitalUnit.NonClinicalUnitRepository;
import ir.zngis.behtab.hospitalUnit.SurgeryDepartmentRepository;
import ir.zngis.behtab.model.Building;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.model.UserRole;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;
import java.util.stream.Stream;

@RestController
@RequestMapping("/protected/")
public class BuildingController {

    @Autowired
    BuildingRepository buildingRepository;

    @Autowired
    HospitalRepo hospitalRepo;

    @Autowired
    SurgeryDepartmentRepository surgeryDepartmentRepository;

    @Autowired
    NonClinicalUnitRepository nonClinicalUnitRepository;

    @Autowired
    ClinicalUnitRepository clinicalUnitRepository;

    @Autowired
    EvaluationRepository evaluationRepository;

    @Autowired
    UserRepo userRepo;


    @GetMapping("hospitals/{hospitalid}/buildings")
    public Page<?> getHospitals(Principal principal, @PathVariable("hospitalid") int hospitalid, Pageable pageable) {

        return userRepo.findByUsername(principal.getName()).map(user -> {
                    Collection<UserRole> roles = user.getAuthorities();
                    if (!roles.isEmpty()) {
                        return buildingRepository.getBuildingByUserId(hospitalid, pageable);
                    } else {
                        return Page.empty();
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("buildings/getAllBuildings4User")
    public List<Building> getAllBuildingsOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return buildingRepository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities()) || ControllerHelper.UserIsSurveyor(user.getAuthorities())) {
                        return buildingRepository.getBuildingByUserId(user.getId());
                    } else
                        return new ArrayList<Building>();
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    /*
     * Insert or update building's shape and name and comment and editor user
     */
    @PostMapping("hospitals/{hospitalid}/buildings/addAll")
    public Object insert(@RequestBody List<BuildingMobile> incomingBuildings, @PathVariable("hospitalid") int hospitalid, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> hospitalRepo.findById(hospitalid).map(hospital -> {
                    if (!incomingBuildings.isEmpty()) {

                        incomingBuildings.forEach(incomingBuilding -> {
                            incomingBuilding.setHospitalByHospitalId(hospital);
                            incomingBuilding.setEditorUser(user);
                            incomingBuilding.setModifyDate(new Date());
                        });


                        String[] guids = incomingBuildings.stream().map(BuildingMobile::getRowGuid).toArray(String[]::new);

                        List<Building> oldBuildings = buildingRepository.findAll(guids);

                        List<Building> newBuildings = new ArrayList<>();

                        for (BuildingMobile building : incomingBuildings) {

                            long count = oldBuildings.stream().filter(oldBuilding -> building.getRowGuid().equalsIgnoreCase(oldBuilding.getRowGuid())).count();

                            if (count == 0) {
                                newBuildings.add(Building.create(building));
                            }
                        }

                        if (!newBuildings.isEmpty())
                            buildingRepository.saveAll(newBuildings);


                        oldBuildings.forEach(oldBuilding -> {
                            incomingBuildings.forEach(incomingBuilding -> {
                                if (incomingBuilding.getRowGuid().equalsIgnoreCase(oldBuilding.getRowGuid())) {
                                    oldBuilding.update(incomingBuilding);
                                }
                            });
                        });

                        buildingRepository.saveAll(oldBuildings);

                        return incomingBuildings;
                    } else {
                        return new ArrayList<>();
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.HOSPITAL_NOT_FOUND))).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping(value = "hospitals/{hospitalid}/buildings/add")
    public BuildingMeta insert(@RequestBody BuildingMeta incomingBuilding, @PathVariable("hospitalid") int hospitalid, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    return hospitalRepo.findById(hospitalid).map(hospital -> {

                        Optional<Building> optionalBuilding = buildingRepository.findById(incomingBuilding.getRowGuid());

                        if (optionalBuilding.isPresent()) {
                            optionalBuilding.ifPresent(foundBuilding -> {
                                foundBuilding.update(incomingBuilding);
                                foundBuilding.setHospitalByHospitalId(hospital);
                                foundBuilding.setEditorUser(user);
                                foundBuilding.setModifyDate(new Date());
                                buildingRepository.save(foundBuilding);
                            });
                        } else {

                            Building newBuilding = Building.create(incomingBuilding);
                            buildingRepository.save(newBuilding);

                        }

                        return incomingBuilding;
                    }).orElse(incomingBuilding);
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @PostMapping(value = "hospitals/{hospitalid}/buildings", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<Building> get(@RequestBody int hospitalId, @PathVariable("hospitalid") int hospitalid) {
        return buildingRepository.getBuildingByHospitalByHospitalId(hospitalId);
    }


    @PostMapping("hospitals/{hospitalid}/buildings/delete")
    public Optional<Boolean> delete(@RequestBody List<String> buildingIds, @PathVariable("hospitalid") int hospitalid, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> hospitalRepo.findById(hospitalid).map(hospital -> {

                    List<Building> buildings = buildingRepository.findAllById(buildingIds);

                    buildings.forEach(building -> {
                        building.setModifyDate(new Date());
                        building.setEditorUser(user);
                        building.setStatus(EntityStatus.DELETE);
                    });

                    boolean retVal = ControllerHelper.UserIsAdmin(user.getAuthorities()) || hospital.getSurveyorUserId() == user.getId();

                    if (retVal) buildingRepository.saveAll(buildings);

                    return Optional.of(retVal);

                })
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.HOSPITAL_NOT_FOUND)).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


}
