package ir.zngis.behtab.building.dto;


import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import ir.zngis.behtab.model.BaseEntity;
import ir.zngis.behtab.model.Building;
import ir.zngis.behtab.model.Hospital;

import javax.persistence.*;


public class BuildingMeta extends BaseEntity {

    private String rowGuid;
    private String name;
    private String comment;
    private Integer hospitalId;
    private Hospital hospitalByHospitalId;
    private double totalConstructionArea;
    private double floorArea;
    private int typology;
    private String constructionYear;
    private int numFloor;
    private Boolean extension;



    public BuildingMeta() {

    }


    public BuildingMeta(Building building) {
        this.comment = building.getComment();
        this.constructionYear = building.getConstructionYear();
        this.extension = building.isExtension();
        this.floorArea = building.getFloorArea();
        this.hospitalByHospitalId = building.getHospitalByHospitalId();
        this.name = building.getName();
        this.typology = building.getTypology();
        this.rowGuid = building.getRowGuid();
        this.numFloor = building.getNumFloor();
        this.totalConstructionArea = building.getTotalConstructionArea();
        this.hospitalId = building.getHospitalId();
     }


    public String getRowGuid() {
        return rowGuid;
    }

    public String getName() {
        return name;
    }


    public String getComment() {
        return comment;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public Hospital getHospitalByHospitalId() {
        return hospitalByHospitalId;
    }

    public double getTotalConstructionArea() {
        return totalConstructionArea;
    }

    public double getFloorArea() {
        return floorArea;
    }


    public int getTypology() {
        return typology;
    }

    public String getConstructionYear() {
        return constructionYear;
    }

    public int getNumFloor() {
        return numFloor;
    }

    public Boolean getExtension() {
        return extension;
    }

}
