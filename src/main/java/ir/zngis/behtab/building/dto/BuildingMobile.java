package ir.zngis.behtab.building.dto;


import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import ir.zngis.behtab.model.BaseEntity;
import ir.zngis.behtab.model.Building;
import ir.zngis.behtab.model.Hospital;

import javax.persistence.*;
import java.util.Objects;


public class BuildingMobile extends BaseEntity {
    private String rowGuid;
    private String name;
    private Geometry geom;
    private String comment;
    private Integer hospitalId;
    private Hospital hospitalByHospitalId;

    public BuildingMobile() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BuildingMobile that = (BuildingMobile) o;
        return Objects.equals(rowGuid, that.rowGuid) &&
                Objects.equals(name, that.name) &&
                Objects.equals(geom, that.geom) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(hospitalId, that.hospitalId) &&
                Objects.equals(hospitalByHospitalId, that.hospitalByHospitalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rowGuid, name, geom, comment, hospitalId, hospitalByHospitalId);
    }

    public BuildingMobile(Building building) {
        this.rowGuid = building.getRowGuid();
        this.name = building.getName();
        this.geom = building.getGeom();
        this.comment = building.getComment();
        this.hospitalId = building.getHospitalId();
        this.hospitalByHospitalId = building.getHospitalByHospitalId();
    }

    public String getRowGuid() {
        return rowGuid;
    }

    public String getName() {
        return name;
    }

    public Geometry getGeom() {
        return geom;
    }


    public String getComment() {
        return comment;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public Hospital getHospitalByHospitalId() {
        return hospitalByHospitalId;
    }

    public void setHospitalByHospitalId(Hospital hospitalByHospitalId) {
        this.hospitalByHospitalId = hospitalByHospitalId;
    }

    public void setRowGuid(String rowGuid) {
        this.rowGuid = rowGuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }
}
