package ir.zngis.behtab.building;

import ir.zngis.behtab.model.Building;
import ir.zngis.behtab.model.Hospital;
import org.apache.catalina.LifecycleState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface BuildingRepository extends JpaRepository<Building, String> {


    @Query(value = "SELECT * FROM building   WHERE  hospital_id = :hospitalId  ", nativeQuery = true)
    List<Building> getBuildingByHospitalByHospitalId(@Param("hospitalId") int hospitalId);

    @Query(value = "SELECT * FROM building   WHERE  hospital_id = :hospitalId  and status!=1", nativeQuery = true)
    List<Building> getUndeletedBuildingsByHospitalId(@Param("hospitalId") int hospitalId);


    @Query(value = "SELECT * FROM building   WHERE  hospital_id IN (select hospital.id from hospital   where  hospital.evaluator_user_id = :userId or hospital.surveyor_user_id = :userId)", nativeQuery = true)
    List<Building> getBuildingByUserId(@Param("userId") int userId);

    @Query(value = "SELECT * FROM building   WHERE  building.hospital_id  = :hospitalId and   building.status != 1", nativeQuery = true)
    Page<List<Building>> getBuildingByUserId(@Param("hospitalId") int hospitalId, Pageable pageable);

    @Query(value = "SELECT * FROM building   WHERE  building.id in (:guids)", nativeQuery = true)
    List<Building> findAll(@Param("guids") String[] guids);

    @Query(value = "select * from building where id in (select building_id from non_clinical_unit where id like :unit_id)", nativeQuery = true)
    Optional<Building> getBuildingByNonClinicalUnitId(@Param("unit_id")String unit_id);

    @Query(value = "select * from building where id in (select building_id from clinical_unit where id like :unit_id)", nativeQuery = true)
    Optional<Building> getBuildingByClinicalUnitId(@Param("unit_id")String unit_id);

    @Query(value = "select * from building where id in (select building_id from surgery_department where id like :unit_id)", nativeQuery = true)
    Optional<Building> getBuildingBySurgericalUnitId(@Param("unit_id")String unit_id);
}
