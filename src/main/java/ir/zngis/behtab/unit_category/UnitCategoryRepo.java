package ir.zngis.behtab.unit_category;

import ir.zngis.behtab.model.UnitCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UnitCategoryRepo extends JpaRepository<UnitCategory,Integer   > {

}
