package ir.zngis.behtab.unit_category;


import ir.zngis.behtab.model.UnitCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController()
@RequestMapping("/protected/unit_categories")
public class UnitCategoryController {

    @Autowired
    UnitCategoryRepo unitCategoryRepo;


    @PostMapping("")
    public Collection<UnitCategory> getAll(){
        return unitCategoryRepo.findAll();
    }

    //Principal principal







}
