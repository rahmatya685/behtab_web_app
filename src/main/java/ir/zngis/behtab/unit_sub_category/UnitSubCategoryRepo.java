package ir.zngis.behtab.unit_sub_category;

import ir.zngis.behtab.model.UnitSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitSubCategoryRepo extends JpaRepository<UnitSubCategory, Integer> {
}
