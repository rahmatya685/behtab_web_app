package ir.zngis.behtab.hospital;


import ir.zngis.behtab.Entrance.EntranceRepository;
import ir.zngis.behtab.Task.ExcelReport;
import ir.zngis.behtab.Task.GenerateAnalyticalReport;
import ir.zngis.behtab.Task.GeoserverProps;
import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.attachment.AttachmentRepo;
import ir.zngis.behtab.attachment.FileStorageService;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.evaluation.ChartInfo4CategoryRepository;
import ir.zngis.behtab.evaluation.ChartInfoRepository;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.evaluation.QuestionCategory4EvaluationRepository;
import ir.zngis.behtab.hospitalUnit.ClinicalUnitRepository;
import ir.zngis.behtab.hospitalUnit.NonClinicalUnitRepository;
import ir.zngis.behtab.hospitalUnit.SurgeryDepartmentRepository;
import ir.zngis.behtab.model.*;
import ir.zngis.behtab.questions.QuestionCategoryRepo;
import ir.zngis.behtab.questions.QuestionRepo;
import ir.zngis.behtab.setting.ExposureIndexRepo;
import ir.zngis.behtab.setting.HazardLevelRepo;
import ir.zngis.behtab.setting.RelativeWeightRepo;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import ir.zngis.behtab.util.ReportFileStatus;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Path;
import java.security.Principal;
import java.util.*;

@RestController
public class HospitalController {

    @Autowired
    HospitalCommentsRepo hospitalCommentsRepo;

    @Autowired
    HospitalRepo hospitalRepo;

    @Autowired
    BuildingRepository buildingRepository;

    @Autowired
    SurgeryDepartmentRepository surgeryDepartmentRepository;

    @Autowired
    NonClinicalUnitRepository nonClinicalUnitRepository;

    @Autowired
    ClinicalUnitRepository clinicalUnitRepository;

    @Autowired
    EvaluationRepository evaluationRepository;


    @Autowired
    AttachmentRepo attachmentRepo;


    @Autowired
    UserRepo userRepo;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    QuestionRepo questionRepo;


    @Autowired
    QuestionCategoryRepo questionCategory;

    @Autowired
    QuestionCategory4EvaluationRepository questionCategory4EvaluationRepository;

    @Autowired
    EntranceRepository mEntranceRepository;

    @Autowired
    ChartInfoRepository mChartInfoRepository;

    @Autowired
    HazardLevelRepo mHazardLevelRepo;

    @Autowired
    ExposureIndexRepo mExposureIndexRepo;


    @Autowired
    RelativeWeightRepo mRelativeWeightRepo;

    @Autowired
    ChartInfo4CategoryRepository mChartInfo4CategoryRepository;


    @Autowired
    GeoserverProps geoserverProps;

    @Autowired
    HospitalAnalyticalInfoRepo hospitalAnalyticalInfoRepo;

    @PostMapping("/protected/hospitals/generateAnalyticalReport/{id}")
    public String getAnalyticalReport(@PathVariable int id, Principal principal, HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("application/json");
        response.setCharacterEncoding("ISO-8859-1");

        List<Evaluation> evaluationList = evaluationRepository.getAllByHospitalId(id);

        if (evaluationList.isEmpty()) {
            return Constants.NO_EVALUATION_FOUND;
        } else {

            return userRepo.findByUsername(principal.getName()).map(user -> {

                        Hospital hospital = hospitalRepo.findById(id).get();

                        String fileName = String.format("%s_%s.pdf", hospital.getName(), UUID.randomUUID().toString());

                        HospitalEntrance hospitalEntrance = mEntranceRepository.getFirstInsertedEntrance(hospital.getId());
                        GenerateAnalyticalReport excelReport = new GenerateAnalyticalReport(
                                hospital, fileStorageService, questionCategory,
                                hospitalRepo, fileName,
                                attachmentRepo, hospitalCommentsRepo, evaluationRepository,
                                questionCategory4EvaluationRepository, buildingRepository,
                                hospitalEntrance, mChartInfoRepository,
                                mHazardLevelRepo, mExposureIndexRepo, mRelativeWeightRepo, mChartInfo4CategoryRepository, geoserverProps);

                        Thread thread = new Thread(excelReport);

                        thread.start();


                        return fileName;
                    }
            ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
        }


    }


    @PostMapping("/protected/hospitals/generateReport/{id}")
    public String getReport(@PathVariable int id, Principal principal, HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("application/json");
        response.setCharacterEncoding("ISO-8859-1");

        List<Evaluation> evaluationList = evaluationRepository.getAllByHospitalId(id);

        if (evaluationList.isEmpty()) {
            return Constants.NO_EVALUATION_FOUND;
        } else {

            return userRepo.findByUsername(principal.getName()).map(user -> {

                        Hospital hospital = hospitalRepo.findById(id).get();

                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(hospital.getName());
                        stringBuilder.append(UUID.randomUUID().toString());
                        stringBuilder.append(".xlsx");

                        String fileName = stringBuilder.toString();

                        ExcelReport excelReport = new ExcelReport(id, fileName, questionRepo, fileStorageService, evaluationRepository, attachmentRepo,buildingRepository);

                        Thread thread = new Thread(excelReport);

                        thread.start();

                        return fileName;
                    }
            ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
        }


    }

    @PostMapping("hospitals/checkIfReportFileIsReady/{excelFileName}")
    public String checkIfReportFileIsReady(@PathVariable String excelFileName, HttpServletResponse response) {

        response.setContentType("application/json");
        response.setCharacterEncoding("ISO-8859-1");

        String logFileName = "STATUS_" + excelFileName.replace("xlsx", "txt");
        logFileName = logFileName.replace("pdf", "txt");

        Path filePath = fileStorageService.resolveFileInReportDirec(logFileName);
        if (filePath.toFile().exists()) {

            try {
                FileReader fileReader = new FileReader(filePath.toFile());

                BufferedReader bufferedReader = new BufferedReader(fileReader);

                String retVal = bufferedReader.readLine();

                filePath.toFile().delete();

                return retVal;

            } catch (IOException e) {
                e.printStackTrace();
                return ReportFileStatus.ERROR_GENERATING_FILE.toString();
            }
        } else {
            return ReportFileStatus.PROCESSING.toString();
        }
    }

    @GetMapping("hospitals/downloadreport/{fileName}")
    public ResponseEntity<Resource> downloadReportFile(@PathVariable String fileName, HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("application/json");
        response.setCharacterEncoding("ISO-8859-1");

        Path filePath = fileStorageService.resolveFileInReportDirec(fileName);

        Resource resource = fileStorageService.loadReportFileAsResource(filePath.getFileName().toString());

        // Try to determine file's content type
        String contentType = null;
        try {

            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

        } catch (IOException ex) {
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);


    }


    @GetMapping("/protected/hospitals4Mobile")
    public List<Hospital> getHospitals(Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {

                    Collection<UserRole> roles = user.getAuthorities();

                    if (!roles.isEmpty()) {
                        if (ControllerHelper.UserIsAdmin(roles)) {
                            return hospitalRepo.getAllHospitals();
                        } else {
                            return hospitalRepo.findAllBySurveyorUserAndEvaluatorUser(user.getId());
                        }
                    } else {
                        return new ArrayList<Hospital>();
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @GetMapping("/protected/hospitals")
    public Page<?> getHospitals(Principal principal, Pageable pageable, String search) {

        return userRepo.findByUsername(principal.getName()).map(user -> {

                    Collection<UserRole> roles = user.getAuthorities();

                    if (!roles.isEmpty()) {
                        Page<List<Hospital>> listPageableHospital = null;
                        if (ControllerHelper.UserIsAdmin(roles)) {

                            listPageableHospital = hospitalRepo.getAllHospitals(search, pageable);
                        } else {
                            listPageableHospital = hospitalRepo.findAllBySurveyorUserAndEvaluatorUser(search, user.getId(), pageable);
                        }

                        return listPageableHospital;
                    } else {
                        return Page.empty();
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/protected/hospitals/add")
    public Object create(@RequestBody Hospital hospital, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        if (hospitalRepo.getByName(hospital.getName()) == null) {
                            saveHospital(hospital, user);
                            return true;
                        } else {
                            return new ResourceNotFoundException(Constants.HospitalNameShouldBeUnique);
                        }
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/protected/hospitals/delete/{id}")
    public Object delete(@PathVariable int id, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {

                        Hospital hospital = hospitalRepo.findById(id).get();
                        hospital.setStatus(EntityStatus.DELETE);
                        hospitalRepo.save(hospital);
                        return true;
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/protected/hospitals/update")
    public Object update(@RequestBody Hospital hospital, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities()) || user.getId() == hospital.getSurveyorUserId()) {
                        saveHospital(hospital, user);
                        return true;
                    } else
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    private Object saveHospital(Hospital hospital, User user) {
        hospital.setEditorUser(user);
        hospital.setModifyDate(new Date());
        userRepo.findById(hospital.getEvaluatorUserId()).ifPresent(hospital::setEvaluatorUser);
        userRepo.findById(hospital.getSurveyorUserId()).ifPresent(hospital::setSurveyorUser);
        return hospitalRepo.save(hospital);
    }

    @PostMapping("/protected/hospitals/updateAll")
    public List<Hospital> update(@RequestBody List<Hospital> hospitals, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    List<Hospital> updateList = new ArrayList<>();


                    hospitals.forEach(hospital -> {

                        hospital.setEditorUser(user);
                        hospital.setModifyDate(new Date());

                        if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {

                            updateList.add(hospital);

                        } else if (ControllerHelper.UserIsSurveyor(user.getAuthorities())) {

                            hospitalRepo.findById(hospital.getId()).ifPresent(existingHospital -> {

                                        if (existingHospital.getSurveyorUserId() == user.getId()) {

                                            userRepo.findById(existingHospital.getEvaluatorUserId()).ifPresent(hospital::setEvaluatorUser);
                                            userRepo.findById(existingHospital.getSurveyorUserId()).ifPresent(hospital::setSurveyorUser);

                                            hospital.setExecutiveSummary(existingHospital.getExecutiveSummary());
                                            hospital.setHazardLevels(existingHospital.getHazardLevels());
                                            hospital.setLevelOfImportance(existingHospital.getLevelOfImportance());
                                            hospital.setObservations4NonStructuralAssessment(existingHospital.getObservations4NonStructuralAssessment());
                                            hospital.setObservations4OrganizationalAssessment(existingHospital.getObservations4OrganizationalAssessment());
                                            hospital.setObservations4StructuralAssessment(existingHospital.getRecommendations4StructuralAssessment());
                                            hospital.setRecommendations4NonStructuralAssessment(existingHospital.getRecommendations4NonStructuralAssessment());
                                            hospital.setRecommendations4OrganizationalAssessment(existingHospital.getRecommendations4OrganizationalAssessment());
                                            hospital.setRecommendations4StructuralAssessment(existingHospital.getRecommendations4StructuralAssessment());
                                            hospital.setOverallRecommendations(existingHospital.getOverallRecommendations());
                                            hospital.setAddress(existingHospital.getAddress());
                                            hospital.setWebSite(existingHospital.getWebSite());
                                            hospital.setPhone(existingHospital.getPhone());
                                            hospital.setServiceType(existingHospital.getServiceType());
                                            hospital.setOrganizationalAffiliation(existingHospital.getOrganizationalAffiliation());
                                            hospital.setNonClinicalStaffCount(existingHospital.getNonClinicalStaffCount());
                                            hospital.setClinicalStaffCount(existingHospital.getClinicalStaffCount());
                                            hospital.setBedOccupancyNormalRate(existingHospital.getBedOccupancyNormalRate());
                                            hospital.setNumTotalApprovedBeds(existingHospital.getNumTotalApprovedBeds());


                                            updateList.add(hospital);
                                        }
                                    }
                            );
                        }
                    });
                    if (updateList.isEmpty())
                        return updateList;
                    else
                        return hospitalRepo.saveAll(updateList);
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @GetMapping("/protected/hospitals/analytical")
    public Object create(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return hospitalAnalyticalInfoRepo.getHospitalsAnlyticalInfos();
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @GetMapping("/protected/hospitals/risk_info_for_level_of_importance/{type}")
    public ResponseEntity<String> calcRiskInfoByLevelOfImportance(Principal principal, @PathVariable int type) {
        String retVal = hospitalAnalyticalInfoRepo.calcRiskInfoByLevelOfImportance(type);
        return new ResponseEntity<>(retVal.toString(), HttpStatus.OK);
    }


    @GetMapping("/protected/hospitals/risk_info_for_level_of_hazard/{type}")
    public ResponseEntity<String> calcRiskInfoByLevelOfHazard(Principal principal, @PathVariable int type) {
        String retVal = hospitalAnalyticalInfoRepo.calcRiskInfoByLevelOfHazard(type);
        return new ResponseEntity<>(retVal.toString(), HttpStatus.OK);
    }

}
