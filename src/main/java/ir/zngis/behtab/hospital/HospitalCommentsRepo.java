package ir.zngis.behtab.hospital;

import ir.zngis.behtab.model.Hospital;
import ir.zngis.behtab.model.HospitalComments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface HospitalCommentsRepo extends JpaRepository<HospitalComments, Integer> {

    @Query(value = "select * from hospital_comments  join hospital on hospital_comments.hospital_id = hospital.id where hospital_comments.status != 1 and hospital.status != 1 and  hospital_comments.editor_id=:userId", nativeQuery = true)
    List<HospitalComments> getHospitalCommentsByUserId(@Param("userId") int userId);


    @Query(value = "select * from hospital_comments  join hospital on hospital_comments.hospital_id = hospital.id where hospital_comments.status != 1 and hospital.status != 1 ",nativeQuery = true)
    List<HospitalComments> getAll();


    @Query(value = "select * from hospital_comments  join hospital on hospital_comments.hospital_id = hospital.id where hospital_comments.status != 1 and hospital.status != 1 and hospital.id=:hospital_id",nativeQuery = true)
    List<HospitalComments> getAll4Hospital(@Param("hospital_id")int hospitalId);


}
