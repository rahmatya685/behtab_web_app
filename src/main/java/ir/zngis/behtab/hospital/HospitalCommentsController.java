package ir.zngis.behtab.hospital;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.model.Hospital;
import ir.zngis.behtab.model.HospitalComments;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/protected/")
public class HospitalCommentsController {

    @Autowired
    HospitalCommentsRepo hospitalCommentsRepo;


    @Autowired
    UserRepo userRepo;

    @Autowired
    HospitalRepo hospitalRepo;

    @PostMapping("hospitalcomments/getAllComments4User")
    public List<?> getAllCommentOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return hospitalCommentsRepo.getAll();
                    } else {
                        return hospitalCommentsRepo.getHospitalCommentsByUserId(user.getId());
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping(value = "hospitalcomments/add")
    public HospitalComments insert(@RequestBody HospitalComments hospitalComments, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {

                    hospitalComments.setEditorUser(user);
                    hospitalComments.setModifyDate(new Date());

                    return hospitalCommentsRepo.save(hospitalComments);

                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("hospitalcomments/addAll")
    public Optional<List<?>> insert(@RequestBody List<HospitalComments> comments, Principal principal) {

        return Optional.of(userRepo.findByUsername(principal.getName()).map(user -> {

                    comments.forEach(hospitalComments -> {
                        hospitalComments.setEditorUser(user);
                        hospitalRepo.findById(hospitalComments.getHospitalId()).ifPresent(hospital -> hospitalComments.setHospital(hospital));

                    });

                    if (comments.isEmpty()) {
                        return new ArrayList<>();
                    } else {
                        return hospitalCommentsRepo.saveAll(comments);
                    }
                }

        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND)));
    }


}
