package ir.zngis.behtab.hospital;

import ir.zngis.behtab.model.Hospital;
import org.geotools.data.ows.GetCapabilitiesRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.net.URL;

public class HospitalSpecification implements Specification<Hospital> {


    private Hospital filter;

    public HospitalSpecification(Hospital filter) {
        this.filter = filter;

    }

    @Override
    public Specification<Hospital> and(Specification<Hospital> other) {
        return null;
    }

    @Override
    public Specification<Hospital> or(Specification<Hospital> other) {
        return null;
    }

    @Override
    public Predicate toPredicate(Root<Hospital> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        Predicate p = criteriaBuilder.disjunction();

        if (filter.getName() != null) {
            p.getExpressions().add(criteriaBuilder.equal(root.get("name"), filter.getName()));
        }

        return p;
    }
}
