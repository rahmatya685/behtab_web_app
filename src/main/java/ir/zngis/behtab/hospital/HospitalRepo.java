package ir.zngis.behtab.hospital;

import ir.zngis.behtab.model.Hospital;
import ir.zngis.behtab.model.HospitalAnalyticalInfo;
import ir.zngis.behtab.model.User;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import javax.validation.constraints.Max;
import java.util.List;
import java.util.Optional;


@Repository
public interface HospitalRepo extends JpaRepository<Hospital, Integer>, JpaSpecificationExecutor<Hospital> {


    List<Hospital> findAllByEvaluatorUser(User evaluatorUserId);

    List<Hospital> findAllBySurveyorUser(User surveyorUserId);

//    @Query("select h from  Hospital h left join fetch h.hospitalComments comments  where  h.status not  in (1) and comments.userId in (1) order by  h.id asc   ")
//    Page<List<Hospital>> getAllHospitals(Pageable pageable);


    @Query(value = "select  * from hospital where hospital.status != 1 and hospital.name like %:search% order  by id  ", nativeQuery = true)
    Page<List<Hospital>> getAllHospitals(@Param("search") String search, Pageable pageable);

    @Query(value = "select  * from hospital where hospital.status != 1 AND hospital.name = :hospitalName order  by id", nativeQuery = true)
    Hospital getByName(@Param("hospitalName") String hospitalName);

    @Query(value = "select  * from hospital where hospital.status != 1 AND hospital.name like %:search% and (surveyor_user_id = :userId or evaluator_user_id = :userId) order  by id", nativeQuery = true)
    Page<List<Hospital>> findAllBySurveyorUserAndEvaluatorUser(@Param("search") String search, @Param("userId") int userId, Pageable pageable);


    @Query(value = "select  * from hospital where hospital.status != 1 order  by id  ", nativeQuery = true)
    List<Hospital> getAllHospitals();

    @Query(value = "select  * from hospital where hospital.status != 1 AND surveyor_user_id = :userId or evaluator_user_id = :userId order  by id", nativeQuery = true)
    List<Hospital> findAllBySurveyorUserAndEvaluatorUser(@Param("userId") int userId);


    @Query(value = "select concat(ST_XMin(ST_Extent(ST_Buffer(geom,0.0005))),',',ST_YMin(ST_Extent(ST_Buffer(geom,0.0005))),',',ST_XMax(ST_Extent(ST_Buffer(geom,0.0005))),',',ST_YMax(ST_Extent(ST_Buffer(geom,0.0005))))    from (\n" +
            " select candidate_location.id,candidate_location.geom  as geom from candidate_location where candidate_location.hospital_id = :hospitalId \n" +
            " union all\n" +
            " select building.id,building.geom as geom from building  where  building.hospital_id =  :hospitalId \n" +
            " union all\n" +
            " select hospital_entrance.id,hospital_entrance.geom as geom from hospital_entrance where hospital_entrance.hospital_id = :hospitalId \n" +
            "\n" +
            ") as T;", nativeQuery = true)
    Optional<String> getHospitalSmallBBox(@Param("hospitalId") int hospitalId);


    @Query(value = "select concat(ST_XMin(ST_Extent(ST_Buffer(geom,0.0120))),',',ST_YMin(ST_Extent(ST_Buffer(geom,0.0120))),',',ST_XMax(ST_Extent(ST_Buffer(geom,0.0120))),',',ST_YMax(ST_Extent(ST_Buffer(geom,0.0120))))    from (\n" +
            " select candidate_location.id,candidate_location.geom  as geom from candidate_location where candidate_location.hospital_id = :hospitalId \n" +
            " union all\n" +
            " select building.id,building.geom as geom from building  where  building.hospital_id =  :hospitalId \n" +
            " union all\n" +
            " select hospital_entrance.id,hospital_entrance.geom as geom from hospital_entrance where hospital_entrance.hospital_id = :hospitalId \n" +
            "\n" +
            ") as T;", nativeQuery = true)
    Optional<String> getHospitalBigBBox2(@Param("hospitalId") int hospitalId);




}
