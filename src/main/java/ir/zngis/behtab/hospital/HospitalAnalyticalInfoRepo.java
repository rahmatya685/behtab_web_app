package ir.zngis.behtab.hospital;

import ir.zngis.behtab.model.Hospital;
import ir.zngis.behtab.model.HospitalAnalyticalInfo;
import org.json.simple.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface HospitalAnalyticalInfoRepo extends JpaRepository<HospitalAnalyticalInfo, Integer> {

    @Query(value = "select * from (select * from calc_risk_indices_hospitals())  as risk where risk.prioritization_index > 0 order by risk.prioritization_index desc ", nativeQuery = true)
    List<HospitalAnalyticalInfo> getHospitalsAnlyticalInfos();


    @Query(value = " select concat(array_to_json(array_agg(row_to_json(t)))||'') from(\n" +
            " select  province_name_en,province_name,level_of_importance_fa,level_of_importance_en,level_of_importance_key, \n" +
            "                 avg(risk_index_structural) as  risk_index_structural,  \n" +
            "                   avg(risk_index_non_structural) as  risk_index_non_structural, \n" +
            "                   avg(risk_index_organizational) as  risk_index_organizational, \n" +
            "                   avg(prioritization_index) as  prioritization_index\n" +
            "            \n" +
            "                   from   calc_risk_indices_hospitals() as TB where TB.type =:type\n" +
            "              group by level_of_importance_fa,level_of_importance_en,province_name_en,province_name,level_of_importance_key  order by level_of_importance_key) t", nativeQuery = true)
    String calcRiskInfoByLevelOfImportance(@Param("type") int type);


    @Query(value = " select concat(array_to_json(array_agg(row_to_json(t)))||'') from(" +
            "select TB.hazard_levels_en,TB.hazard_levels_fa,sum(TB.hazard_level) as hazard_level,\n" +
            "    sum(risk_index_structural) as risk_index_structural,\n" +
            "    sum (risk_index_non_structural) as risk_index_non_structural,\n" +
            "    sum(risk_index_organizational) as risk_index_organizational,\n" +
            "    sum(prioritization_index) as prioritization_index\n" +
            " from (     \n" +
            "                       select hazard_levels_en,hazard_levels_fa,0 as hazard_level,\n" +
            "                              avg(risk_index_structural) as  risk_index_structural,  \n" +
            "                              avg(risk_index_non_structural) as  risk_index_non_structural, \n" +
            "                              avg(risk_index_organizational) as  risk_index_organizational, \n" +
            "                              avg(prioritization_index) as  prioritization_index\n" +
            "                              from   calc_risk_indices_hospitals() as TB where TB.type =:type \n" +
            "                         group by hazard_levels_en,hazard_levels_fa\n" +
            "        union all \n" +
            "\n" +
            "         select hazard_level.set_name_en as hazard_levels_en,\n" +
            "                hazard_level.set_name_fa as hazard_levels_en,\n" +
            "                hazard_level,0,0,0,0 \n" +
            "         from hazard_level) as TB\n" +
            "         group by hazard_levels_en,hazard_levels_fa order by hazard_level) t;", nativeQuery = true)
    String calcRiskInfoByLevelOfHazard(@Param("type") int type);
}
