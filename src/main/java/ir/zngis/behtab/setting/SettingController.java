package ir.zngis.behtab.setting;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.model.*;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/protected/setting")
public class SettingController {


    @Autowired
    MobileAppSettingRepo mobileAppSettingRepo;


    @Autowired
    HazardLevelRepo hazardLevelRepo;


    @Autowired
    ExposureIndexRepo exposureIndexRepo;

    @Autowired
    RelativeWeightRepo relativeWeightRepo;

    @Autowired
    UserRepo userRepo;


    @PostMapping("/updateRelativeWeight")
    private Object updateRelativeWeights(@RequestBody  RelativeWeight relativeWeights, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return relativeWeightRepo.save(relativeWeights);
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("/updateExposureIndice")
    private Object updateExposureIndexs(@RequestBody  ExposureIndex exposureIndices, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return exposureIndexRepo.save(exposureIndices);
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("/updateHazardLevel")
    private Object updateHazardLevels(@RequestBody  HazardLevel hazardLevels, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return hazardLevelRepo.save(hazardLevels);
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @GetMapping("/getRelativeWeights")
    private Page<?>  getRelativeWeights(Principal principal,Pageable pageable) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                relativeWeightRepo.findAll(pageable)
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @GetMapping("/getExposureIndices")
    private Page<?> getExposureIndexs(Principal principal,Pageable pageable) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                exposureIndexRepo.findAll(pageable)
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @GetMapping("/getHazardLevels")
    private Page<?> getHazardLevels(Principal principal, Pageable pageable) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                hazardLevelRepo.findAll(pageable)
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/getAllMobileAppSetting")
    private Collection<MobileAppSetting> getAllMobileAppSetting() {
        return mobileAppSettingRepo.findAll();
    }


    @PostMapping("/getMobileAppSetting")
    private Collection<MobileAppSetting> getMobileAppSetting(@RequestBody List<String> ids, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                mobileAppSettingRepo.findAllById(ids)
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/updateMobileAppSetting")
    private Collection<MobileAppSetting> updateMobileAppSetting(@RequestBody List<MobileAppSetting> settings,
                                                                Principal principal) {
        List<MobileAppSetting> retVal = new ArrayList<>();
        for (MobileAppSetting setting : settings) {
            mobileAppSettingRepo.findById(setting.getRowGuid()).ifPresent(setting1 -> {
                setting1.setValue(setting.getValue());

                retVal.add(mobileAppSettingRepo.save(setting1));
            });

        }
        return retVal;

    }


}
