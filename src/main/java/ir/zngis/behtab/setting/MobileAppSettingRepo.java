package ir.zngis.behtab.setting;


import ir.zngis.behtab.model.MobileAppSetting;
import ir.zngis.behtab.model.UnitCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MobileAppSettingRepo extends JpaRepository<MobileAppSetting,String   > {



}
