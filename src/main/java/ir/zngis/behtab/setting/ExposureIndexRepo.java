package ir.zngis.behtab.setting;

import ir.zngis.behtab.model.ExposureIndex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ExposureIndexRepo extends JpaRepository<ExposureIndex, Integer> {


    @Query(value = "select  * from exposure_index where exposure_index.set_key like :setKey",nativeQuery = true)
    public ExposureIndex findBySetKey(@Param("setKey") String setKey);
}
