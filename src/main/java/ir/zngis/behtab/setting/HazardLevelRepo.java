package ir.zngis.behtab.setting;

import ir.zngis.behtab.model.HazardLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HazardLevelRepo extends JpaRepository<HazardLevel, Integer> {



    @Query(value = "select  * from hazard_level where hazard_level.set_key like :setKey",nativeQuery = true)
    public HazardLevel findBySetKey(@Param("setKey") String setKey);
}
