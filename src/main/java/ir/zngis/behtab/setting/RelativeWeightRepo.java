package ir.zngis.behtab.setting;


import ir.zngis.behtab.model.RelativeWeight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RelativeWeightRepo extends JpaRepository<RelativeWeight, Integer> {



    @Query(value = "select  * from relative_weight where relative_weight.set_key like :setKey",nativeQuery = true)
    public RelativeWeight findByModule(@Param("setKey") String setKey);
}
