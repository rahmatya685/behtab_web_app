package ir.zngis.behtab.province;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.model.Hospital;
import ir.zngis.behtab.model.ProvinceAnalyticalInfo;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.model.UserRole;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.HealthLocationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class ProvinceController {

    @Autowired
    ProvinceRepo provinceRepo;

    @Autowired
    UserRepo userRepo;

    @GetMapping("/protected/province/getanalyticalinfo/{healthLocationType}")
    public List<ProvinceAnalyticalInfo> getProvinceAnalyticalInfo(Principal principal,@PathVariable int healthLocationType) {
        return userRepo.findByUsername(principal.getName()).map(user -> provinceRepo.getAll(healthLocationType)
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }
}
