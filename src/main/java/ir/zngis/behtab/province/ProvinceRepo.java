package ir.zngis.behtab.province;

import ir.zngis.behtab.model.ProvinceAnalyticalInfo;
import ir.zngis.behtab.util.HealthLocationType;
import org.hibernate.validator.constraints.pl.REGON;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ProvinceRepo extends JpaRepository<ProvinceAnalyticalInfo, Integer> {


    @Query(value = "select TbMain.objectid , TbMain.ostn_name, TbMain.ostn_name_en, sum (TbMain.count) as count,\n" +
            "       sum(risk_index_structural) as risk_index_structural,\n" +
            "       sum(risk_index_non_structural) as risk_index_non_structural,\n" +
            "       sum(risk_index_organizational) as risk_index_organizational,\n" +
            "       sum(prioritization_index) as prioritization_index\n" +
            "\n" +
            "      from ((select province.gid  as objectid,province.ostn_name,province.ostn_name_en , \n" +
            "                           round( COALESCE(avg(hospital_risk_index.risk_index_structural),0),2) as risk_index_structural, \n" +
            "                             round( COALESCE(avg(hospital_risk_index.risk_index_non_structural),0),2) as risk_index_non_structural, \n" +
            "                             round( COALESCE(avg(hospital_risk_index.risk_index_organizational),0),2) as risk_index_organizational, \n" +
            "                             round( COALESCE(avg(hospital_risk_index.prioritization_index),0),2) as prioritization_index , \n" +
            "                             count(*) \n" +
            "                            from province left join (select * from calc_risk_indices_hospitals()) as hospital_risk_index on ST_WITHIN (    hospital_risk_index.center_point,province.geom) \n" +
            "                            where hospital_risk_index.type = :healthLocationType \n" +
            "                            group by  province.ostn_name,province.ostn_name_en,province.gid  )\n" +
            " union all \n" +
            "  (select province.gid  as objectid,province.ostn_name,province.ostn_name_en,0,0,0,0,0 from province ))as TbMain \n" +
            "     group by TbMain.objectid , TbMain.ostn_name, TbMain.ostn_name_en order by prioritization_index desc", nativeQuery = true)
    public List<ProvinceAnalyticalInfo> getAll(@Param("healthLocationType") int healthLocationType);

}
