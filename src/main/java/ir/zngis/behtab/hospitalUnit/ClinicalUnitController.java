package ir.zngis.behtab.hospitalUnit;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.model.ClinicalUnit;
import ir.zngis.behtab.model.UnitSubCategory;
import ir.zngis.behtab.unit_category.UnitCategoryRepo;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/protected/")
public class ClinicalUnitController {

    @Autowired
    ClinicalUnitRepository repository;
    @Autowired
    BuildingRepository buildingRepository;

    @Autowired
    UserRepo userRepo;

    @Autowired
    UnitCategoryRepo unitCategoryRepo;

    @Autowired
    EvaluationRepository evaluationRepository;

    @PostMapping("clinicalunit/getAllclinicalunits4User")
    public List<ClinicalUnit> getAllBuildingsOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return repository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities()) || ControllerHelper.UserIsSurveyor(user.getAuthorities())) {
                        return repository.getAllClinicalUnits(user.getId());
                    } else
                        return new ArrayList<ClinicalUnit>();
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/clinicalunit/addAll")
    public List<ClinicalUnit> insertAll(@RequestBody List<ClinicalUnit> clinicalUnits,
                                        @PathVariable("hospitalid") int hospitalid,
                                        @PathVariable("buildingId") String buildingId, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> buildingRepository.findById(buildingId).map(building -> {
                    clinicalUnits.forEach(nonClinicalUnit -> {
                        nonClinicalUnit.setBuildingByBuildingId(building);
                        nonClinicalUnit.setEditorUser(user);
                        nonClinicalUnit.setModifyDate(new Date());

                    });

                    return repository.saveAll(clinicalUnits);
                }).orElseThrow(() -> new RuntimeException("Invlid hospital id"))
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));

    }
//
//    @PostMapping(value = "hospitals/{hospitalid}/buildings/{buildingId}/clinicalunit/add")
//    public ClinicalUnit insert(@RequestBody ClinicalUnit clinicalUnit,
//                               @PathVariable("hospitalid") int hospitalid,
//                               @PathVariable("buildingId") String buildingId) {
//
//        buildingRepository.findById(buildingId).map(building -> {
//            clinicalUnit.setBuildingByBuildingId(building);
//            return repository.save(clinicalUnit);
//        }).orElseThrow(() -> new RuntimeException("Invalid Building id"));
//        return null;
//    }


    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/clinicalunit/delete")
    public boolean delete(@RequestBody List<String> ids,
                          @PathVariable("hospitalid") int hospitalid,
                          @PathVariable("buildingId") String buildingId, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {
                    List<ClinicalUnit> units = repository.findAllById(ids);
                    units.forEach(unit -> {
                        unit.setEditorUser(user);
                        unit.setModifyDate(new Date());
                        unit.setStatus(EntityStatus.DELETE);
                    });
                    if (!units.isEmpty())
                        repository.saveAll(units);
                    return true;
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
//        try {
//
//            List<ClinicalUnit> units = repository.findAllById(ids);
//
//            units.forEach(unit -> {
//                evaluationRepository.deleteAllByClinicalUnitByClinicalUnitId(unit.getRowGuid());
//            });
//
//            repository.deleteAll( units);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
    }

    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/clinicalunit/update")
    public List<ClinicalUnit> update(@RequestBody List<ClinicalUnit> clinicalUnits,
                                     @PathVariable("hospitalid") int hospitalid,
                                     @PathVariable("buildingId") int buildingId) {
        return repository.saveAll(clinicalUnits);
    }


}
