package ir.zngis.behtab.hospitalUnit;


import ir.zngis.behtab.model.ClinicalUnit;
import org.apache.catalina.LifecycleState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicalUnitRepository extends JpaRepository<ClinicalUnit, String> {

    @Query(value = "select  * from clinical_unit where clinical_unit.building_id in (select building.id from building where hospital_id in (select hospital.id from hospital where hospital.evaluator_user_id = :userId or hospital.surveyor_user_id = :userId)  and building.status != 1) ", nativeQuery = true)
    public List<ClinicalUnit> getAllClinicalUnits(@Param("userId") int userId);



    @Query(value = "select * from clinical_unit join building on building.id = surgery_department.building_id join hospital on hospital.id = building.hospital_id where building.status != 1",nativeQuery = true)
    public List<ClinicalUnit> findAll();

}
