package ir.zngis.behtab.hospitalUnit;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.model.NonClinicalUnit;
import ir.zngis.behtab.model.SurgeryDepartment;
import ir.zngis.behtab.model.UnitCategory;
import ir.zngis.behtab.unit_category.UnitCategoryRepo;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/protected/")
public class NonClinicalUnitController {

    @Autowired
    NonClinicalUnitRepository repository;
    @Autowired
    BuildingRepository buildingRepository;

    @Autowired
    UnitCategoryRepo unitCategoryRepo;

    @Autowired
    EvaluationRepository evaluationRepository;

    @Autowired
    UserRepo userRepo;

    @PostMapping("nonclinicalunit/getAllNonClinicalUnits4User")
    public List<NonClinicalUnit> getAllBuildingsOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return repository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities()) || ControllerHelper.UserIsSurveyor(user.getAuthorities())) {
                        return repository.getAllNonClinicalUnit(user.getId());
                    } else
                        return new ArrayList<NonClinicalUnit>();
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/nonclinicalunit/addAll")
    public List<NonClinicalUnit> insert(@RequestBody List<NonClinicalUnit> nonClinicalUnits,
                                        @PathVariable("hospitalid") int hospitalid,
                                        @PathVariable("buildingId") String buildingId, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> buildingRepository.findById(buildingId).map(building -> {
                    nonClinicalUnits.forEach(nonClinicalUnit -> {
                        nonClinicalUnit.setBuildingByBuildingId(building);
                        nonClinicalUnit.setEditorUser(user);
                        nonClinicalUnit.setModifyDate(new Date());

                    });

                    return repository.saveAll(nonClinicalUnits);
                }).orElseThrow(() -> new RuntimeException("Invlid hospital id"))
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));


    }

//    @PostMapping(value = "hospitals/{hospitalid}/buildings/{buildingId}/nonclinicalunit/add")
//    public NonClinicalUnit insert(@RequestBody NonClinicalUnit nonClinicalUnit,
//                                  @PathVariable("hospitalid") int hospitalid,
//                                  @PathVariable("buildingId") String  buildingId) {
//
//        buildingRepository.findById(buildingId).map(building -> {
//            nonClinicalUnit.setBuildingByBuildingId(building);
//            return repository.save(nonClinicalUnit);
//        }).orElseThrow(() -> new RuntimeException("Invlid hospital id"));
//        return null;
//    }


    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/nonclinicalunit/delete")
    public boolean delete(@RequestBody List<String> ids,
                          @PathVariable("hospitalid") int hospitalid,
                          @PathVariable("buildingId") String buildingId, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {
                    List<NonClinicalUnit> units = repository.findAllById(ids);
                    units.forEach(unit -> {
                        unit.setEditorUser(user);
                        unit.setStatus(EntityStatus.DELETE);
                        unit.setModifyDate(new Date());

                    });
                    if (!units.isEmpty())
                        repository.saveAll(units);
                    return true;
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
//        try {
//
//            List<NonClinicalUnit> units = repository.findAllById(ids);
//
//            units.forEach(unit -> {
//                evaluationRepository.deleteAllByNonClinicalUnitByNonClinicalUnitId(unit.getRowGuid());
//            });
//
//            repository.deleteAll( units);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
    }


}
