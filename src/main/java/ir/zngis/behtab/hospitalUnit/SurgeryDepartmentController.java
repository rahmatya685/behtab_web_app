package ir.zngis.behtab.hospitalUnit;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.model.SurgeryDepartment;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/protected/")
public class SurgeryDepartmentController {

    @Autowired
    SurgeryDepartmentRepository repository;

    @Autowired
    BuildingRepository buildingRepository;


    @Autowired
    UserRepo userRepo;

    @Autowired
    EvaluationRepository evaluationRepository;

    @PostMapping("surgerydepartment/getAllclinicalunits4User")
    public List<SurgeryDepartment> getAllBuildingsOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return repository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities()) || ControllerHelper.UserIsSurveyor(user.getAuthorities())) {
                        return repository.getAllNonClinicalUnit(user.getId());
                    } else
                        return new ArrayList<SurgeryDepartment>();
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/surgerydepartment/addAll")
    public List<SurgeryDepartment> insert(@RequestBody List<SurgeryDepartment> surgeryDepartments,
                                          @PathVariable("hospitalid") int hospitalid,
                                          @PathVariable("buildingId") String buildingId, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> buildingRepository.findById(buildingId).map(building -> {

                    surgeryDepartments.forEach(surgeryDepartment ->
                            {
                                surgeryDepartment.setBuildingByBuildingId(building);
                                surgeryDepartment.setEditorUser(user);
                                surgeryDepartment.setModifyDate(new Date());

                            }
                    );

                    return repository.saveAll(surgeryDepartments);
                }).orElseThrow(() -> new RuntimeException("Invlid hospital id"))
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));


    }


    @PostMapping("hospitals/{hospitalid}/buildings/{buildingId}/surgerydepartment/delete")
    public boolean delete(@RequestBody List<String> ids,
                          @PathVariable("hospitalid") int hospitalid,
                          @PathVariable("buildingId") String buildingId, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {
                    List<SurgeryDepartment> units = repository.findAllById(ids);
                    units.forEach(unit -> {
                        unit.setEditorUser(user);
                        unit.setStatus(EntityStatus.DELETE);
                        unit.setModifyDate(new Date());

                    });
                    if (!units.isEmpty())
                        repository.saveAll(units);
                    return true;
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
//        try {
//
//            List<SurgeryDepartment> units = repository.findAllById(ids);
//
//            units.forEach(unit -> {
//                evaluationRepository.deleteAllBySurgeryDepartmentBySurgeryDepartmentId(unit.getRowGuid());
//            });
//
//            repository.deleteAll(units);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }

    }
}
