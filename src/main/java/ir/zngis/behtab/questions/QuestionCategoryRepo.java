package ir.zngis.behtab.questions;

import ir.zngis.behtab.model.QuestionCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface QuestionCategoryRepo extends JpaRepository<QuestionCategory, Integer> {

    @Query(value = " select question_category.id,question_category.status,question_category.modify_date,question_category.editor_id,   module_name, category_title_en, category_title_fa,  " +
            " (sum(evaluation.risk_level*question.weight)/sum(question.weight))*100/4  as weighted_avg from  question_category  " +
            "left join question on question_category.id = question.category_id  " +
            "left join evaluation on  question.id = evaluation.question_id  " +
            "left join building on evaluation.building_id = building.id  " +
            "left join surgery_department on evaluation.surgery_department_id =surgery_department.id  " +
            "left join clinical_unit on  evaluation.clinical_unit_id = clinical_unit.id " +
            "left join non_clinical_unit on evaluation.non_clinical_unit_id =non_clinical_unit.id  " +
            " where evaluation.building_id in (select id from building where building.hospital_id = :hospitalId and  status != 1 OR status is null)  " +
            " or evaluation.surgery_department_id in (select id from surgery_department where building_id in (select id from building where building.hospital_id = :hospitalId and  ( status != 1 OR status is null) ))  " +
            " or evaluation.clinical_unit_id in (select id from clinical_unit where building_id in (select id from building where building.hospital_id = :hospitalId and  ( status != 1 OR status is null)  ))  " +
            " or evaluation.non_clinical_unit_id in (select id from non_clinical_unit where building_id in (select id from building where building.hospital_id = :hospitalId and   ( status != 1 OR status is null)  ))  " +
            " group by question_category.id,category_title_en,category_title_fa,module_name,question_category.status,question_category.modify_date,question_category.editor_id order by  question_category.id", nativeQuery = true)
    List<QuestionCategory> getCates4Report(@Param("hospitalId") int hospitalId);



    @Query(value = "select * from question_category where  question_category.status != 1 OR status is null ",nativeQuery = true)
    List<QuestionCategory> getAllCates( );

}
