package ir.zngis.behtab.questions;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.model.Question;
import ir.zngis.behtab.model.QuestionCategory;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/protected/questions")
public class QuestionController {


    @Autowired
    QuestionRepo questionRepo;

    @Autowired
    QuestionCategoryRepo questionCategoryRepo;


    @Autowired
    UserRepo userRepo;


    @PostMapping("")
    public Collection<Question> getQuestions() {
        return questionRepo.getSorted();
    }


    @GetMapping("/page")
    public Collection<Question> getQuestions(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                questionRepo.getAllSorted()
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("/add")
    public Object addQuestion(@RequestBody Question question, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {




                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        if (question.getRank() != null) {

                            List<Question> questions = questionRepo.getQuestionByRank(question.getRank());

                            if (!questions.isEmpty()) {

                                List<Question> upperRankQuestions = questionRepo.getQuestionUpperThanRank(question.getRank());

                                int rank = question.getRank();

                                for (Question upperRankQuestion : upperRankQuestions) {
                                    upperRankQuestion.setRank(++rank);
                                }

                                questionRepo.saveAll(upperRankQuestions);

                            }

                        }
                        question.setEditorUser(user);
                        return questionRepo.save(question);
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("/delete")
    public Object deleteQuestion(@RequestBody Question question, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        question.setStatus(EntityStatus.DELETE);
                        question.setEditorUser(user);
                        //this is because of mobile app not null constraint
                        if (question.getHelpLink() == null)
                            question.setHelpLink("https://unhabitat.org/iran/");
                        questionRepo.save(question);

                        return true;
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @PostMapping("/update")
    public Object updateQuestion(@RequestBody Question question, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return questionCategoryRepo.findById(question.getCategoryId()).map(questionCategory1 -> {

                            question.setQuestionCategoryByCategoryId(questionCategory1);
                            question.setEditorUser(user);

                            return questionRepo.save(question);

                        }).orElseThrow(() -> new ResourceNotFoundException("invalid_question_category"));
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }


}
