package ir.zngis.behtab.questions;

import ir.zngis.behtab.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepo extends JpaRepository<Question, Integer> {

    @Query(value = "select * from question where status!= 1 order by  rank ", nativeQuery = true)
    List<Question> getSorted();

    @Query(value = "select * from question  order by  rank ", nativeQuery = true)
    List<Question> getAllSorted();

    @Query(value = "select * from question where question.rank = :rank", nativeQuery = true)
    List<Question> getQuestionByRank(@Param("rank") int rank);


    @Query(value = "select * from question where question.rank >= :rank order by  rank", nativeQuery = true)
    List<Question> getQuestionUpperThanRank(@Param("rank") int rank);


}
