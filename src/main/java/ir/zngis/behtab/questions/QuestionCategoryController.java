package ir.zngis.behtab.questions;


import ir.zngis.behtab.User.UserCrudService;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.model.QuestionCategory;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/protected/qcategories")
public class QuestionCategoryController {


    @Autowired
    QuestionCategoryRepo questionRepo;

    @Autowired
    UserCrudService userRepo;

    @Autowired
    HospitalRepo hospitalRepo;


    @PostMapping("")
    public Collection<QuestionCategory> getQuestionCategories() {
        return questionRepo.findAll();
    }


    @PostMapping("/add")
    public QuestionCategory addQuestion(@RequestBody QuestionCategory questionCategory) {
        return questionRepo.save(questionCategory);
    }


    @PostMapping("/delete/{id}")
    public boolean deleteQuestion(@PathVariable int id) {
        try {
            questionRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @PostMapping("/update")
    public QuestionCategory updateQuestion(@RequestBody QuestionCategory question) {
        return questionRepo.save(question);
    }


    /*
     * returns evaluations weighted avg by hospital for each question category
     */
    @PostMapping("/{hospitalId}/getReportData")
    public Object getQuestionCategoryByEvaluationAvg(Principal principal, @PathVariable("hospitalId") int hospitalid) {
        return userRepo.findByUsername(principal.getName()).map(user -> hospitalRepo.findById(hospitalid).map(hospital -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities()) || hospital.getSurveyorUserId() == user.getId() || hospital.getEvaluatorUserId() == user.getId()) {

                        List<QuestionCategory> catesWitData = questionRepo.getCates4Report(hospitalid);

                        List<QuestionCategory> allCates = questionRepo.getAllCates();

                        for (QuestionCategory catesWitDatum : catesWitData) {
                            for (QuestionCategory allCate : allCates) {
                                if (catesWitDatum.getId() == allCate.getId()) {
                                    allCate.setEvaluationAvg(allCate.getEvaluationAvg());
                                }
                            }
                        }

                        return allCates;
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.HOSPITAL_NOT_FOUND))).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));


    }
}
