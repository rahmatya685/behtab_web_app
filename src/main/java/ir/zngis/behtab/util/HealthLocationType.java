package ir.zngis.behtab.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum HealthLocationType {
    HOSPITAL(1),
    HEALTH_HOUSE(2);


    public final int code;


    HealthLocationType(int i) {
        code = i;
    }



    @JsonCreator
    public static int toInt(HealthLocationType healthLocationType) {
        switch (healthLocationType) {
            case HOSPITAL:
                return 1;
            case HEALTH_HOUSE:
                return 2;
            default:
                return 0;
        }
    }

    @JsonCreator
    public static HealthLocationType toHealthLocationType(Integer type) {
        switch (type) {
            case 1:
                return HealthLocationType.HOSPITAL;
            case 2:
                return HealthLocationType.HEALTH_HOUSE;
            default:
                return HealthLocationType.HOSPITAL;
        }

    }

    @JsonValue
    public int getValue() {
        return toInt(this);
    }
}
