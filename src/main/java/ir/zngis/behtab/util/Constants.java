package ir.zngis.behtab.util;

public interface Constants {
    String HOSTPITAL_SURVEY_USER_MISMATCH="Hospital survey user mismatch";
    String HOSPITAL_NOT_FOUND = "Hospital not found";
    String USER_NOT_FOUND = "User not found";
    String ACTION_NOT_ALLOWED="Action not allowed";
    String HospitalNameShouldBeUnique="HospitalNameShouldBeUnique";
    String MORE_THAN_ONE_USER_LOGIN_IN_NOT_ALLOWED ="MORE_THAN_ONE_USER_LOGIN_IN_NOT_ALLOWED";
    String INVALID_USERNAME_AND_PASSWORD = "Invalid username or password";
    String USER_ALREADY_EXISTS = "User Already Exist";
    String ENTITY_NOT_FOUND="ENTITY NOT FOUND";
    String SETTING_NOT_FOUND ="SETTING_NOT_FOUND";
    String NO_EVALUATION_FOUND="NO_EVALUATION_FOUND";
    String DELETING_USER_IS_ALLOWED_4_ADMIN="DELETING_USER_IS_ALLOWED_4_ADMIN";



    interface UserRoles {
        int Surveyor = 1;
        int Evaluator = 2;
        int Surveillance = 3;
        int Admin = 4;
    }
}
