package ir.zngis.behtab.util;

import ir.zngis.behtab.model.UserRole;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Integer.*;

public class ControllerHelper {


    public static InetAddress getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        try {
            return InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            return null;
        }
    }

    public static boolean UserIsSurvillance(Collection<UserRole> userRoles) {
        return UserHasRole(userRoles, Constants.UserRoles.Surveillance);
    }

    public static boolean UserIsAdmin(Collection<UserRole> userRoles) {
        return UserHasRole(userRoles, Constants.UserRoles.Admin);
    }

    public static boolean UserIsEvaluator(Collection<UserRole> userRoles) {
        return UserHasRole(userRoles, Constants.UserRoles.Evaluator);
    }

    public static boolean UserIsSurveyor(Collection<UserRole> userRoles) {
        return UserHasRole(userRoles, Constants.UserRoles.Surveyor);
    }

    private static boolean UserHasRole(Collection<UserRole> userRoles, Integer role) {

        for (UserRole userRole : userRoles) {
            if (valueOf(userRole.getAuthority()).equals(role)) {
                return true;
            }
        }

        return false;
    }
}
