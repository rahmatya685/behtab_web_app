package ir.zngis.behtab.util;

public enum ReportFileStatus {
    READY,
    PROCESSING,
    ERROR_GENERATING_FILE
}
