package ir.zngis.behtab.util;

import javax.persistence.AttributeConverter;
 import javax.persistence.Converter;

@Converter
public class EntityStatusConvertor implements AttributeConverter<EntityStatus,Integer> {
    @Override
    public Integer convertToDatabaseColumn(EntityStatus status) {
        if (status == null){
            return EntityStatus.INSERT.getValue();
        }else {
            return EntityStatus.toInt(status);
        }
     }

    @Override
    public EntityStatus convertToEntityAttribute(Integer integer) {
        if (integer == null){
            return EntityStatus.INSERT;
        }else{
            return EntityStatus.toEntityStatus(integer);
        }
     }
}
