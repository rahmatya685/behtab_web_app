package ir.zngis.behtab.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.annotation.Nullable;

public enum EntityStatus {
    DELETE(1),
    INSERT(2),
    SYNCED(3);

    public final int code;

    EntityStatus(int value) {
        this.code = value;
    }


    @JsonCreator
    public static EntityStatus toEntityStatus(@Nullable int code) {
        switch (code) {
            case 1:
                return DELETE;
            case 2:
                return INSERT;
            case 3:
                return SYNCED;
            default:
                return INSERT;
        }
    }

    @JsonCreator
    public static EntityStatus toEntityStatus(@Nullable String  code) {
        switch (code) {
            case "1":
                return DELETE;
            case "2":
                return INSERT;
            case "3":
                return SYNCED;
            default:
                return INSERT;
        }
    }


    public static int toInt(@Nullable EntityStatus status) {
        switch (status) {
            case DELETE:
                return 1;
            case INSERT:
                return 2;
            case SYNCED:
                return 3;
            default:
                return 2;


        }
    }

    @JsonValue
    public int getValue() {
        return toInt(this);
    }


}
