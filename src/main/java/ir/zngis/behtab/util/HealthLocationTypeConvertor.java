package ir.zngis.behtab.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class HealthLocationTypeConvertor implements AttributeConverter<HealthLocationType,Integer> {
    @Override
    public Integer convertToDatabaseColumn(HealthLocationType healthLocationType) {
        return HealthLocationType.toInt(healthLocationType);
    }

    @Override
    public HealthLocationType convertToEntityAttribute(Integer integer) {
        return HealthLocationType.toHealthLocationType(integer);
    }
}
