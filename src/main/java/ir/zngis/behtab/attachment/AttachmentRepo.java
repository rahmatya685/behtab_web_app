package ir.zngis.behtab.attachment;

import ir.zngis.behtab.model.Attachment;
import ir.zngis.behtab.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AttachmentRepo extends JpaRepository<Attachment, String> {


    @Query(value = "select * from attachment where  attachment.id in (:guids)", nativeQuery = true)
    public List<Attachment> findAllByRowGuid(@Param("guids") List<String> guids);

    @Query(value = "select * from attachment where attachment.editor_id = :userId and attachment.category like :cat and attachment.link_id = :linkedId", nativeQuery = true)
    List<Attachment> findAll(int userId, String cat, int linkedId);

    @Query(value = "select * from attachment where  attachment.category like :cat and attachment.link_id = :linkedId", nativeQuery = true)
    List<Attachment> findAll(@Param("cat") String cat, @Param("linkedId") String linkedId);


    @Query(value = "select * from attachment where  attachment.category like :cat and attachment.link_id = :linkedId and attachment.status != 1", nativeQuery = true)
    Page<List<Attachment>> findAll(@Param("cat") String cat, @Param("linkedId") String linkedId, Pageable pageable);


    List<Attachment> findAllByEditorUser(User user);
}
