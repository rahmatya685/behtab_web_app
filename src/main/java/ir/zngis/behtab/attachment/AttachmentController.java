package ir.zngis.behtab.attachment;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.model.Attachment;
import ir.zngis.behtab.model.Pair;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.EntityStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@RestController
public class AttachmentController {

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    UserRepo userRepo;

    private static final Logger logger = LoggerFactory.getLogger(AttachmentController.class);


    @PostMapping("/protected/attachments")
    public List<Attachment> getAttachments(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->

                attachmentRepo.findAllByEditorUser(user)

        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
    }


    @GetMapping("/protected/attachments/get")
    public Page<?> getAttachmentsPageble(Principal principal, @RequestParam("type") String type, @RequestParam("id") String id, Pageable pageable) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                attachmentRepo.findAll(type, id,pageable)
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
    }


    @PostMapping("/protected/attachments/get")
    public List<Attachment> getAttachments(Principal principal, @RequestParam("type") String type, @RequestParam("id") String id) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                attachmentRepo.findAll(type, id)
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/protected/attachments/uploadFile")
    public Attachment uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("category") String category, @RequestParam("linkedId") String linkedId,
                                 @RequestParam("RowGUID") String rowGuids, @RequestParam("comment") String comment, @RequestParam("extension") String extension, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {

                    String fileName = fileStorageService.storeFile(file);

                    String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                            .path("/downloadFile/")
                            .path(fileName)
                            .toUriString();

                    Attachment attachment = new Attachment();
                    attachment.setFileName(fileName);
                    attachment.setFileDownloadUri(fileDownloadUri);
                    attachment.setExtension(extension);

                    attachment.setLinkId(linkedId);
                    attachment.setComment(comment);
                    attachment.setRowGuid(rowGuids);
                    attachment.setCategory(category);
                    attachment.setEditorUser(user);

                    return attachmentRepo.save(attachment);
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));


    }

    @PostMapping("/protected/attachments/deleteFiles")
    public List<Pair<String, Boolean>> deleteAttachments(@RequestBody List<String> guids, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    List<Pair<String, Boolean>> retVals = new ArrayList<>();

                    List<Attachment> attachments = attachmentRepo.findAllByRowGuid(guids);
                    attachments.forEach(attachment -> {
                                boolean isDeleted = false;
                                if (attachment.getEditorUser().getId() == user.getId()) {
                                    isDeleted = fileStorageService.deleteUploadFile(attachment.getFileName());
                                    if (isDeleted) {
                                        attachment.setStatus(EntityStatus.DELETE);
                                        attachmentRepo.save(attachment);
                                    }
                                }
                                retVals.add(new Pair<>(attachment.getRowGuid(), isDeleted));

                            }
                    );

                    return retVals;
                }
        ).orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));

    }


//    @PostMapping("/uploadFiles")
//    public List<Attachment> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//
//         return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file ))
//                .collect(Collectors.toList());
//    }

    private Attachment findAttachment(List<Attachment> attachments, MultipartFile file) {
        AtomicReference<Attachment> attachment = new AtomicReference<>(new Attachment());
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        attachments.forEach(a -> {
            if (a.getFileName().contentEquals(fileName))
                attachment.set(a);
        });
        return attachment.get();
    }


    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadUploadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


}
