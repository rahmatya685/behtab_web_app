package ir.zngis.behtab.attachment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.util.List;
import java.util.UUID;

@Service
public class FileStorageService {
    private final Path fileStorageLocation;

    private final Path reportStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        this.reportStorageLocation = Paths.get(fileStorageProperties.getReportDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }

        try {
            Files.createDirectories(this.reportStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the report files will be stored.", ex);
        }
    }

    public boolean deleteReportFile(String fileName) {
        try {
            Path path = this.reportStorageLocation.resolve(fileName);

            Files.deleteIfExists(path);

        } catch (Exception e) {

        }
        return true;
    }

    public boolean deleteUploadFile(String fileName) {
        try {
            Path path = this.fileStorageLocation.resolve(fileName);

            Files.deleteIfExists(path);

        } catch (Exception e) {

        }
        return true;
    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadUploadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public Resource loadReportFileAsResource(String fileName) {
        try {
            Path filePath = this.reportStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public Path generateFile(String fileName) {
        Path path = this.reportStorageLocation.resolve(fileName);
        Path newFile = null;
        try {
            Files.deleteIfExists(path);
            newFile = Files.createFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFile;
    }

    public Path resolveFileInReportDirec(String fileName) {
        return this.reportStorageLocation.resolve(fileName);
    }
    public Path resolveFileInAttachmentDirec(String fileName) {
        return this.fileStorageLocation.resolve(fileName);
    }

    public String getRandomFileName() {
        return UUID.randomUUID().toString();
    }
}
