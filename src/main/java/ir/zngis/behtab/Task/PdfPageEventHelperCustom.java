package ir.zngis.behtab.Task;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.*;
import org.springframework.core.io.ClassPathResource;

import java.io.File;

public class PdfPageEventHelperCustom extends PdfPageEventHelper {


    private boolean mDrawBorder = false;

    private Font mTextFont;

    private BaseColor mBorderColor;

    public PdfPageEventHelperCustom() {
        mBorderColor = BaseColor.BLACK;
        try {


            File resource = new ClassPathResource("font/Vazir.ttf").getFile();

            BaseFont bfComic = BaseFont.createFont(resource.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            mTextFont = new Font(bfComic, 11, Font.NORMAL);

            this.mTextFont.setColor(BaseColor.RED);

        } catch (Exception e) {

        }
    }

    public void drawBorder(BaseColor baseColor) {
        this.mBorderColor = baseColor;
        this.mDrawBorder = true;
    }

    public float offset = 5;
    public float startPosition;

    @Override
    public void onParagraph(PdfWriter writer, Document document, float paragraphPosition) {
        this.startPosition = paragraphPosition;
    }

    @Override
    public void onParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) {
        if (mDrawBorder) {
            PdfContentByte cb = writer.getDirectContentUnder();
            cb.rectangle(document.left(), paragraphPosition - offset,
                    document.right() - document.left(), startPosition - paragraphPosition);
            cb.setColorStroke(mBorderColor);
            cb.stroke();
            mDrawBorder = false;
        }
    }


    @Override
    public void onStartPage(PdfWriter writer, Document document) {


    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {

        try {

            Phrase topLeft = new Phrase();


            Image behdashIcon = Image.getInstance(new ClassPathResource("imgs/behdasht_icon.jpg").getURL());
            behdashIcon.scaleToFit(70, 50);
            behdashIcon.setAlignment(Element.ALIGN_CENTER);


            Image japanFlag = Image.getInstance(new ClassPathResource("imgs/japan_flag.jpg").getURL());
            japanFlag.scaleToFit(70, 50);
            japanFlag.setAlignment(Element.ALIGN_CENTER);


            Chunk behdashtIconChunk = new Chunk(behdashIcon, 50, -20);
            Chunk japanFlagChunk = new Chunk(japanFlag, 50, -20);

            topLeft.add(behdashtIconChunk);
            topLeft.add(japanFlagChunk);


            Image unHabitatIcon = Image.getInstance(new ClassPathResource("imgs/UNHabitat_blue_logo.jpeg").getURL());
            unHabitatIcon.scaleToFit(70, 50);
            unHabitatIcon.setAlignment(Element.ALIGN_CENTER);

            Image behtabIcon = Image.getInstance(new ClassPathResource("imgs/behtab_icon.jpg").getURL());
            behtabIcon.scaleToFit(30, 30);
            behtabIcon.setAlignment(Element.ALIGN_CENTER);


            Chunk unHabitatIconChunk = new Chunk(unHabitatIcon, 50, -10);
            Chunk behtabIconChunk = new Chunk(behtabIcon, 50, -10);

            Phrase topRight = new Phrase();
            topRight.add(unHabitatIconChunk);
            topRight.add(behtabIconChunk);


            Paragraph headerTitle = new Paragraph("RAPID VISUAL ASSESSMENT REPORT", mTextFont);

            if (writer.getCurrentPageNumber() != 1) {
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, headerTitle, 300, 800, 0);
            }
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, topLeft, 0, 820, 0);
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, topRight, 490, 810, 0);


        } catch (Exception e) {
            e.printStackTrace();
        }

//        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("http://www.xxxx-your_example.com/"), 110, 30, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("page " + document.getPageNumber()), document.getPageSize().getWidth() / 2, 30, 0);
    }
}
