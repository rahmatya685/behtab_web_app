package ir.zngis.behtab.Task;

import ir.zngis.behtab.attachment.AttachmentRepo;
import ir.zngis.behtab.attachment.FileStorageService;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.model.*;
import ir.zngis.behtab.questions.QuestionRepo;
import ir.zngis.behtab.util.ReportFileStatus;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class ExcelReport implements Runnable {

    static final String UNKNOWN = "Unknown";

    private int mHospitalId;

    private FileStorageService fileStorageService;
    private QuestionRepo questionRepo;
    private EvaluationRepository evaluationRepository;
    private AttachmentRepo attachmentRepo;
    private BuildingRepository mBuildingRepository;

    private String mFileName;

    private ReportFileStatus mReportStatus;

    public ExcelReport(int mHospitalId, String fileName, QuestionRepo questionRepo, FileStorageService fileStorageService, EvaluationRepository evaluationRepository, AttachmentRepo attachmentRepo, BuildingRepository buildingRepository) {

        this.mHospitalId = mHospitalId;
        this.evaluationRepository = evaluationRepository;
        this.fileStorageService = fileStorageService;
        this.questionRepo = questionRepo;
        this.mFileName = fileName;
        this.attachmentRepo = attachmentRepo;
        this.mBuildingRepository = buildingRepository;

        mReportStatus = ReportFileStatus.PROCESSING;
    }

    @Override
    public void run() {

        // writeToLog(mReportStatus.toString());

        try {
            List<Question> questions = questionRepo.getSorted();

            List<Evaluation> evaluations = evaluationRepository.getAllByHospitalId(mHospitalId);


            Workbook workbook = new XSSFWorkbook();


            Map<String, Sheet> sheets = new HashMap<>();

            List<String> columns = new ArrayList<>();

            columns.add("Category");
            columns.add("Question");
            columns.add("Weight");


            for (Evaluation evaluation : evaluations) {

                String sheetName = evaluation.getEditorUser().getName();

                Sheet currentSheet = null;

                if (sheets.containsKey(sheetName)) {
                    currentSheet = sheets.get(sheetName);
                } else {
                    currentSheet = workbook.createSheet(sheetName.replace("\\","-").replace("//","-"));
                    sheets.put(sheetName, currentSheet);
                }

                int rowIndex = findQuestionRow(questions, evaluation);

                if (rowIndex != -1) {

                    Row row = currentSheet.getRow(rowIndex);
                    if (row == null)
                        row = currentSheet.createRow(rowIndex);


                    String columnName = null;

                    if (evaluation.getBuildingByBuildingId() != null) {

                        columnName = evaluation.getBuildingByBuildingId().getName();

                        putEvaluation(evaluation, row, columnName, columns);


                    } else if (evaluation.getClinicalUnitByClinicalUnitId() != null) {


                        if (evaluation.getClinicalUnitByClinicalUnitId().getUnitSubCategoryByName().getTitleFa() != null)
                            columnName = evaluation.getClinicalUnitByClinicalUnitId().getUnitSubCategoryByName().getTitleFa() + "_" + getBuildingName(evaluation.getClinicalUnitByClinicalUnitId());

                        if (evaluation.getClinicalUnitByClinicalUnitId().getUnitSubCategoryByName().getTitleEn() != null)
                            columnName = evaluation.getClinicalUnitByClinicalUnitId().getUnitSubCategoryByName().getTitleEn() + "_" + getBuildingName(evaluation.getClinicalUnitByClinicalUnitId());

                        putEvaluation(evaluation, row, columnName, columns);


                    } else if (evaluation.getNonClinicalUnitByNonClinicalUnitId() != null) {

                        if (evaluation.getNonClinicalUnitByNonClinicalUnitId().getUnitSubCategoryByName().getTitleFa() != null)
                            columnName = evaluation.getNonClinicalUnitByNonClinicalUnitId().getUnitSubCategoryByName().getTitleFa() + "_" + getBuildingName(evaluation.getNonClinicalUnitByNonClinicalUnitId());

                        if (evaluation.getNonClinicalUnitByNonClinicalUnitId().getUnitSubCategoryByName().getTitleEn() != null)
                            columnName = evaluation.getNonClinicalUnitByNonClinicalUnitId().getUnitSubCategoryByName().getTitleEn() + "_" + getBuildingName(evaluation.getNonClinicalUnitByNonClinicalUnitId());

                        putEvaluation(evaluation, row, columnName, columns);


                    } else if (evaluation.getSurgeryDepartmentBySurgeryDepartmentId() != null) {

                        if (evaluation.getSurgeryDepartmentBySurgeryDepartmentId().getName() != null)
                            columnName = evaluation.getSurgeryDepartmentBySurgeryDepartmentId().getName() + "_" + getBuildingName(evaluation.getSurgeryDepartmentBySurgeryDepartmentId());
                        ;

                        putEvaluation(evaluation, row, columnName, columns);

                    }


                }

            }
            for (Map.Entry<String, Sheet> stringSheetEntry : sheets.entrySet()) {
                Row row = stringSheetEntry.getValue().getRow(0);
                if (row == null)
                    row = stringSheetEntry.getValue().createRow(0);

                for (int i = 0; i < columns.size(); i++) {
                    String column = columns.get(i);
                    Cell cell = row.createCell(i + 1);
                    if (cell != null)
                        cell.setCellValue(column);
                }
            }


            for (Map.Entry<String, Sheet> stringSheetEntry : sheets.entrySet()) {
                for (int i = 0; i < questions.size(); i++) {

                    Question q = questions.get(i);

                    int rowIndex = i + 1;

                    Row row = stringSheetEntry.getValue().getRow(rowIndex);
                    if (row == null)
                        row = stringSheetEntry.getValue().createRow(rowIndex);


                    String module = q.getQuestionCategoryByCategoryId().getModuleName();

                    Cell cell0 = row.getCell(0);
                    if (cell0 == null)
                        cell0 = row.createCell(0);

                    if (module == null)
                        module = UNKNOWN;

                    cell0.setCellValue(module);

                    String category = q.getQuestionCategoryByCategoryId().getCategoryTitleEn();

                    if (category == null || category.isEmpty()) {
                        category = UNKNOWN;
                    }
                    Cell cell = row.getCell(1);
                    if (cell == null)
                        cell = row.createCell(1);

                    cell.setCellValue(category);


                    String qTitle = q.getQuestionTitleEn();
                    if (qTitle == null || qTitle.isEmpty()) {
                        qTitle = UNKNOWN;
                    }
                    Cell cell2 = row.getCell(2);
                    if (cell2 == null)
                        cell2 = row.createCell(2);

                    cell2.setCellValue(qTitle);


                    Double qWeight = q.getWeight();
                    if (qWeight == null) {
                        qWeight = 0.0;
                    }
                    Cell cell3 = row.getCell(3);
                    if (cell3 == null)
                        cell3 = row.createCell(3);

                    cell3.setCellValue(qWeight);
                }
            }

            Path filePath = fileStorageService.generateFile(mFileName);

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(filePath.toString());
            workbook.write(fileOut);

            fileOut.close();

            // Closing the workbook
            workbook.close();

            mReportStatus = ReportFileStatus.READY;

            writeToLog(mReportStatus.toString());

        } catch (Exception e) {

            writeToLog(e.toString());

            e.printStackTrace();


        }

    }

    private void writeToLog(String string) {
        Path filePath = fileStorageService.generateFile("STATUS_" + mFileName.replace("xlsx", "txt"));

        File file = filePath.toFile();

        try {
            if (file.exists())
                file.delete();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(string);
            fileWriter.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }


    private void putEvaluation(Evaluation evaluation, Row row, String columnName, List<String> columns) {
        if (!columns.contains(columnName))
            columns.add(columnName);
        if (columnName != null) {

            int colIndex = columns.indexOf(columnName);

            int cellIndex = colIndex + 1;

            Cell cell = row.getCell(cellIndex);
            if (cell == null) {
                cell = row.createCell(cellIndex);
            }
            if (evaluation.getHasObserved()) {
                cell.setCellValue(evaluation.getRiskLevel());
            } else {
                cell.setCellValue(evaluation.getComment());
            }


            String attachColName = String.format("%s %s", "پیوستهای ", columnName);

            if (!columns.contains(attachColName))
                columns.add(attachColName);

            colIndex = columns.indexOf(attachColName);

            cellIndex = colIndex + 1;

            List<Attachment> attachments = attachmentRepo.findAll(Evaluation.class.getSimpleName(), evaluation.getRowGuid());

            StringBuilder stringBuilder = new StringBuilder();

            for (Attachment attachment : attachments) {

                stringBuilder.append(attachment.getFileDownloadUri());
                stringBuilder.append("\n");
            }
            cell = row.getCell(cellIndex);
            if (cell == null) {
                cell = row.createCell(cellIndex);
            }
            cell.setCellValue(stringBuilder.toString());

        }
    }

    private String getBuildingName(BaseEntity baseEntity) {
        String buildingName = "";

        if (baseEntity instanceof NonClinicalUnit) {
            Optional<Building> bName = mBuildingRepository.getBuildingByNonClinicalUnitId(((NonClinicalUnit) baseEntity).getRowGuid());
            if (bName.isPresent())
                buildingName = bName.get().getName();
        }
        if (baseEntity instanceof ClinicalUnit) {
            Optional<Building> bName = mBuildingRepository.getBuildingByClinicalUnitId(((ClinicalUnit) baseEntity).getRowGuid());
            if (bName.isPresent())
                buildingName = bName.get().getName();
        }

        if (baseEntity instanceof SurgeryDepartment) {
            Optional<Building> bName = mBuildingRepository.getBuildingBySurgericalUnitId(((SurgeryDepartment) baseEntity).getRowGuid());
            if (bName.isPresent())
                buildingName = bName.get().getName();
        }


        return buildingName;
    }

    private int findQuestionRow(List<Question> questions, Evaluation evaluation) {
        int counter = 0;
        for (Question question : questions) {
            counter++;
            if (question.getId() == evaluation.getQuestionId())
                break;
        }
        return counter;
    }

}
