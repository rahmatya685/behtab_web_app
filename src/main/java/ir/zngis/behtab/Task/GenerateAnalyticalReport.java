package ir.zngis.behtab.Task;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.languages.ArabicLigaturizer;
import com.itextpdf.text.pdf.languages.LanguageProcessor;
import com.vividsolutions.jts.geom.Coordinate;
import ir.zngis.behtab.attachment.AttachmentRepo;
import ir.zngis.behtab.attachment.FileStorageService;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.evaluation.ChartInfo4CategoryRepository;
import ir.zngis.behtab.evaluation.ChartInfoRepository;
import ir.zngis.behtab.evaluation.EvaluationRepository;
import ir.zngis.behtab.evaluation.QuestionCategory4EvaluationRepository;
import ir.zngis.behtab.hospital.HospitalCommentsRepo;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.model.*;
import ir.zngis.behtab.questions.QuestionCategoryRepo;
import ir.zngis.behtab.setting.ExposureIndexRepo;
import ir.zngis.behtab.setting.HazardLevelRepo;
import ir.zngis.behtab.setting.RelativeWeightRepo;
import ir.zngis.behtab.util.ReportFileStatus;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.*;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.*;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;


public class GenerateAnalyticalReport implements Runnable {

    private ReportFileStatus mReportStatus;

    static final String UNKNOWN = "Unknown";
    static final String NA = "N/A";

    private Hospital mHospital;

    private HazardLevelRepo mHazardLevelRepo;

    private ExposureIndexRepo mExposureIndexRepo;

    private RelativeWeightRepo mRelativeWeightRepo;


    private FileStorageService mFileStorageService;

    private QuestionCategoryRepo mQuestionCategoryRepo;

    private HospitalRepo mHospitalRepo;

    private AttachmentRepo mAttachmentRepo;

    private BuildingRepository mBuildingRepository;

    private HospitalCommentsRepo mHospitalCommentsRepo;


    private EvaluationRepository mEvaluationRepository;

    private ChartInfoRepository mChartInfoRepository;

    private ChartInfo4CategoryRepository mChartInfo4CategoryRepository;

    private QuestionCategory4EvaluationRepository mQuestionCategory4EvaluationRepository;

    private Map<String, List<QuestionTitleWithComment>> mModuleComments;

    private String fileName;

    private List<String> mapImgFilesName;

    private java.util.List<String> mGraphImgsFiles = new ArrayList<>();

    private static String[] ROLE_IN_HEALTH_CARE_NETWORK_EN = new String[]{"Referral", "General"};

    private static String[] SERVICE_TYPE_EN = new String[]{"General", "Single specialty", "Treasury"};

    private static String[] ORGANIZATIONAL_AFFILIATION_EN = new String[]{"Governmental", "Private Sector", "Academic", "Charity", "Military", "Social Security Organization"};

    private static String[] BUILDING_TYPOLOGY = new String[]{
//            "W1-Light wood frame single- or multiple family dwellings of one or more stories in height",
//            "W1A-Light wood frame multi-unit, multi-story residential buildings with plan areas on each floor of greater than 3000 square feet",
//            "W2-Wood frame and commercial and industrial building with a floor area larger than 5000 square feet",
            "S1-Steel moment-resisting frame",
            "S2-Braced steel frame",
            "S3-Light metal frame",
            "S4-Steel frame with cast-in-place concrete shear walls",
            "S5-Steel frame with unreinforced masonry infill walls",
            "C1-Concrete moment-resisting frame",
            "C2-Concrete shear wall",
            "C3-Concrete  with unreinforced masonry infill walls",
            "PC1-Tilt-up construction",
            "PC2-Precast concrete frame",
            "RM1-Reinforced masonry with flexible floor and roof diaphragms",
            "RM2-Reinforced masonry with rigid floor and roof diaphragms",
            "URM1-Reinforced masonry bearing-wall building",
            "URM2-UnReinforced masonry bearing-wall building"
//            "MH-Manufactured housing"
    };


    private static String[] STRUCTURE_TYPE_EN = new String[]{"Concrete", "Steel", "Masonry", "Hybrid"};


    private static final String GEOSERVER_LOCATION_MAP_URL = "/behtab/wms?service=WMS&version=1.1.0&request=GetMap&layers=behtab:planet_osm_polygon,behtab:planet_osm_line,behtab:hospital_entrance&styles=,,hospital_entrance_4_wms_service_in_analytical_report&width=%d&height=%d&srs=EPSG:4326&format=image/jpeg&CQL_FILTER=1=1;1=1;%shospital_id=%d&bbox=%s";
    private static final String GEOSERVER_SCETCH_MAP_URL = "/behtab/wms?service=WMS&version=1.1.0&request=GetMap&layers=behtab:planet_osm_polygon,behtab:planet_osm_line,behtab:candidate_location,behtab:building&styles=&width=%d&height=%d&srs=EPSG:4326&format=image/jpeg&CQL_FILTER=1=1;1=1;hospital_id=%d;hospital_id=%d;hospital_id=%d&bbox=%s";

    private File resource;

    private BaseFont bfComic;

    private Font mTitleFont;
    private Font mTextFont;

    private PdfPageEventHelperCustom mPageEvenHelper;

    private LanguageProcessor mArabicLigaturizer;

    private HospitalEntrance mHospitalEntrance;

    private GeoserverProps mGeoserverProps;

    public GenerateAnalyticalReport(Hospital mHospitalId,
                                    FileStorageService fileStorageService,
                                    QuestionCategoryRepo questionCategoryRepo,
                                    HospitalRepo hospitalRepo,
                                    String fileName,
                                    AttachmentRepo attachmentRepo,
                                    HospitalCommentsRepo hospitalCommentsRepo,
                                    EvaluationRepository evaluationRepository,
                                    QuestionCategory4EvaluationRepository questionCategory4EvaluationRepository,
                                    BuildingRepository buildingRepository,
                                    HospitalEntrance hospitalEntrance,
                                    ChartInfoRepository chartInfoRepository,
                                    HazardLevelRepo mHazardLevelRepo,
                                    ExposureIndexRepo mExposureIndexRepo,
                                    RelativeWeightRepo mRelativeWeightRepo,
                                    ChartInfo4CategoryRepository chartInfo4CategoryRepository, GeoserverProps geoserverProps) {

        this.mHospital = mHospitalId;
        this.mFileStorageService = fileStorageService;
        this.mQuestionCategoryRepo = questionCategoryRepo;
        this.mHospitalRepo = hospitalRepo;
        this.fileName = fileName;
        this.mapImgFilesName = new ArrayList();
        //wms Big Image
        this.mapImgFilesName.add(String.format("%s_wms1.jpeg", fileName));
        //wms Small Image
        this.mapImgFilesName.add(String.format("%s_wms2.jpeg", fileName));

        this.mAttachmentRepo = attachmentRepo;
        this.mHospitalCommentsRepo = hospitalCommentsRepo;
        this.mEvaluationRepository = evaluationRepository;
        this.mModuleComments = new HashMap<>();
        this.mQuestionCategory4EvaluationRepository = questionCategory4EvaluationRepository;
        this.mBuildingRepository = buildingRepository;
        this.mHospitalEntrance = hospitalEntrance;
        this.mChartInfoRepository = chartInfoRepository;
        this.mHazardLevelRepo = mHazardLevelRepo;
        this.mExposureIndexRepo = mExposureIndexRepo;
        this.mRelativeWeightRepo = mRelativeWeightRepo;
        this.mChartInfo4CategoryRepository = chartInfo4CategoryRepository;
        mReportStatus = ReportFileStatus.PROCESSING;
        this.mGeoserverProps = geoserverProps;
        mArabicLigaturizer = new ArabicLigaturizer();

        try {


            resource = new ClassPathResource("font/Vazir.ttf").getFile();

            bfComic = BaseFont.createFont(resource.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            mTitleFont = new Font(bfComic, 14, Font.NORMAL);

            mTextFont = new Font(bfComic, 10, Font.NORMAL);

            this.mPageEvenHelper = new PdfPageEventHelperCustom();

        } catch (Exception e) {

        }

    }

    @Override
    public void run() {

        mFileStorageService.deleteUploadFile(fileName);

        getSmallWmsImage();

        getBigWmsImage();

        generateGraphs2();

        try {
            FileOutputStream archivo = new FileOutputStream(mFileStorageService.resolveFileInReportDirec(fileName).toFile().getAbsolutePath());
            Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
            PdfWriter Writer = PdfWriter.getInstance(doc, archivo);
            doc.open();
            Writer.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);

            Writer.setPageEvent(mPageEvenHelper);

            addIntroductionSection(doc);

            addGeneralModule(doc);

            addBuildings(doc);

            doc.newPage();

            addMapImgFile(doc);

            doc.newPage();

            mTextFont.setColor(BaseColor.BLACK);

            addRiskGraphs(doc);

            addRiskInfoTable(doc);

            doc.newPage();

            addObservations(doc);

            addRecommendations(doc);

            doc.close();

            mReportStatus = ReportFileStatus.READY;

            writeToLog(mReportStatus.toString());

        } catch (Exception e) {
            e.printStackTrace();
            writeToLog(e.toString());

        }


    }


    private void addRiskGraphs(Document doc) throws Exception {

        PdfPTable mainTable = new PdfPTable(1);
        mainTable.setWidthPercentage(100);
        mainTable.setSpacingBefore(10);

        PdfPCell riskScoring = new PdfPCell(new Phrase("Risk Scoring", mTitleFont));
        riskScoring.setHorizontalAlignment(Element.ALIGN_CENTER);
        riskScoring.setColspan(2);
        riskScoring.setBorder(Rectangle.NO_BORDER);

        mainTable.addCell(riskScoring);

        for (String graphImg : mGraphImgsFiles) {

            File graphImgFile = new File(graphImg);

            if (graphImgFile.exists()) {

                Image jpeg = Image.getInstance(graphImgFile.toURI().toURL());
                jpeg.setBorder(Rectangle.BOX);
                jpeg.setBorderColor(BaseColor.BLUE);
                jpeg.setBorderWidth(0.5f);

                PdfPCell pdfPCell = new PdfPCell(jpeg, true);
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setPadding(10);
                pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                mainTable.addCell(pdfPCell);
            }
        }


        doc.add(mainTable);
    }

    private void addObservations(Document doc) {

        HazardLevel hazardLevel = null;

        if (mHospital.getHazardLevels() != null)
            hazardLevel = mHazardLevelRepo.findBySetKey(mHospital.getHazardLevels());

        java.util.List<Pair<String, Object>> comments = new ArrayList<>();
        comments.add(new Pair("Executive Summary: \n%s", mHospital.getExecutiveSummary() != null ? mHospital.getExecutiveSummary() : UNKNOWN));
        comments.add(new Pair("Level of Hazard: %s", hazardLevel != null ? hazardLevel.getSetNameEn() : UNKNOWN));
        comments.add(new Pair("Observations for Structural Assessment: \n%s", mHospital.getObservations4StructuralAssessment() != null ? mHospital.getObservations4StructuralAssessment() : UNKNOWN));
        comments.add(new Pair("Observations for Non Structural Assessment:\n%s", mHospital.getObservations4NonStructuralAssessment() != null ? mHospital.getObservations4NonStructuralAssessment() : UNKNOWN));
        comments.add(new Pair("Observations for Organizational Assessment:\n %s", mHospital.getObservations4OrganizationalAssessment() != null ? mHospital.getObservations4OrganizationalAssessment() : UNKNOWN));

        PdfPTable generalModuleOuterTable = new PdfPTable(1);
        generalModuleOuterTable.setTableEvent(new PdfPTableEventCustom());
        generalModuleOuterTable.setWidthPercentage(100);

        PdfPCell generalSectionHeader = new PdfPCell(new Phrase("Overall Engineering Observations", mTitleFont));
        generalSectionHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        generalSectionHeader.setColspan(2);
        generalSectionHeader.setBorder(Rectangle.NO_BORDER);

        generalModuleOuterTable.addCell(generalSectionHeader);

        generalModuleOuterTable.setSpacingBefore(10);
        generalModuleOuterTable.setSpacingAfter(10);


        //Paragraph

        for (Pair<String, Object> pair : comments) {

//            Phrase phrase =new Phrase(String.format(pair.getKey(), mArabicLigaturizer.process(String.valueOf(pair.getValue()))), mTextFont);

            Paragraph paragraph = new Paragraph(String.format(pair.getKey(), pair.getValue()), mTextFont);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.setPhrase(paragraph);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setColspan(1);
            pdfPCell.setPadding(10);
            generalModuleOuterTable.addCell(pdfPCell);
        }
        try {
            doc.add(generalModuleOuterTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addRecommendations(Document doc) {
        java.util.List<Pair<String, Object>> comments = new ArrayList<>();
        comments.add(new Pair("Recommendations for Structural Assessment:\n %s", mHospital.getRecommendations4StructuralAssessment() != null ? mHospital.getRecommendations4StructuralAssessment() : UNKNOWN));
        comments.add(new Pair("Recommendations for Non Structural Assessment:\n %s", mHospital.getRecommendations4NonStructuralAssessment() != null ? mHospital.getRecommendations4NonStructuralAssessment() : UNKNOWN));
        comments.add(new Pair("Recommendations for Organizational Assessment:\n %s", mHospital.getRecommendations4OrganizationalAssessment() != null ? mHospital.getRecommendations4OrganizationalAssessment() : UNKNOWN));
        comments.add(new Pair("Overall Recommendations:\n %s", mHospital.getOverallRecommendations() != null ? mHospital.getOverallRecommendations() : UNKNOWN));


        PdfPTable generalModuleOuterTable = new PdfPTable(1);
        generalModuleOuterTable.setTableEvent(new PdfPTableEventCustom());

        generalModuleOuterTable.setWidthPercentage(100);

        PdfPCell generalSectionHeader = new PdfPCell(new Phrase("Immediate Mitigation Recommendations", mTitleFont));
        generalSectionHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        generalSectionHeader.setColspan(2);
        generalSectionHeader.setBorder(Rectangle.NO_BORDER);

        generalModuleOuterTable.addCell(generalSectionHeader);

        generalModuleOuterTable.setSpacingBefore(10);
        generalModuleOuterTable.setSpacingAfter(10);

        for (Pair<String, Object> pair : comments) {

            Paragraph paragraph = new Paragraph(String.format(pair.getKey(), pair.getValue()), mTextFont);
            paragraph.setAlignment(Element.ALIGN_JUSTIFIED);

            //new Phrase(String.format(pair.getKey(), mArabicLigaturizer.process(String.valueOf(pair.getValue()))), mTextFont)

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.setPhrase(paragraph);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setColspan(1);
            pdfPCell.setPadding(10);
            generalModuleOuterTable.addCell(pdfPCell);
        }

        try {
            doc.add(generalModuleOuterTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addIntroductionSection(Document doc) throws DocumentException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        List<String> buildingsName = new ArrayList<>();
        for (Building building : mBuildingRepository.getUndeletedBuildingsByHospitalId(mHospital.getId())) {
            buildingsName.add(building.getName() == null ? UNKNOWN : building.getName());
        }

        java.util.List<Pair<String, String>> infos = new ArrayList<>();

        infos.add(new Pair<>("Name of the facility: %s ", mHospital.getName()));
        infos.add(new Pair<>("Buildings Name: %s", String.join(",", buildingsName)));
        infos.add(new Pair<>("Date Of Assessment: %s", format.format(mHospital.getModifyDate())));
        infos.add(new Pair<>("Surveyed By: %s", mHospital.getSurveyorUser().getName()));


        PdfPTable introductionSectionTable = new PdfPTable(2);
        introductionSectionTable.setTableEvent(new PdfPTableEventCustom());

        introductionSectionTable.setWidthPercentage(100);

        PdfPCell introductionSectionHeader = new PdfPCell(new Phrase("Rapid Visual Assessment Report", mTitleFont));
        introductionSectionHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        introductionSectionHeader.setColspan(2);
        introductionSectionHeader.setBorder(Rectangle.NO_BORDER);

        introductionSectionTable.addCell(introductionSectionHeader);

        introductionSectionTable.setSpacingBefore(10);
        introductionSectionTable.setSpacingAfter(10);

        for (Pair<String, String> info : infos) {
            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.setPhrase(new Phrase(String.format(info.getKey(), mArabicLigaturizer.process(String.valueOf(info.getValue()))), mTextFont));
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setColspan(2);
            pdfPCell.setPadding(5);
            introductionSectionTable.addCell(pdfPCell);
        }

        doc.add(introductionSectionTable);

        mTextFont.setColor(BaseColor.BLACK);
    }

    private void addBuildings(Document doc) throws DocumentException {

        List<Building> buildings = mBuildingRepository.getUndeletedBuildingsByHospitalId(mHospital.getId());

        int counter = 0;

        for (Building building : buildings) {

            PdfPTable buildingTable = new PdfPTable(2);
            buildingTable.setTableEvent(new PdfPTableEventCustom());
            buildingTable.setWidths(new int[]{6, 4});
            buildingTable.setWidthPercentage(100);
            buildingTable.setPaddingTop(20);
            PdfPCell buildingTitle = new PdfPCell(new Phrase(String.format("%s Properties", mArabicLigaturizer.process(building.getName() == null ? UNKNOWN : building.getName())), mTextFont));
            buildingTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
            buildingTitle.setColspan(2);
            buildingTitle.setBorder(Rectangle.NO_BORDER);

            buildingTable.addCell(buildingTitle);

            List<Pair<String, Object>> props = new ArrayList<>();

            props.add(new Pair<>("Total Built Area (sq.m): ", building.getTotalConstructionArea() == -1 ? UNKNOWN : building.getTotalConstructionArea()));
            props.add(new Pair<>("Commission Year: ", building.getConstructionYear() == null ? UNKNOWN : building.getConstructionYear()));

            props.add(new Pair<>("Floor Area (sq.m): ", (building.getFloorArea() == -1 ? UNKNOWN : building.getFloorArea())));
            props.add(new Pair<>("Number Of Floors: ", building.getNumFloor() == -1 ? UNKNOWN : building.getNumFloor()));

            String typology = " ";

            try {
                typology = (building.getTypology() != null && building.getTypology() < BUILDING_TYPOLOGY.length && building.getTypology() >= 0) ? BUILDING_TYPOLOGY[building.getTypology() - 1] : UNKNOWN;
            } catch (Exception e) {

            }

            props.add(new Pair<>("Typology: ", typology));
            props.add(new Pair<>("Extensions: ", building.isExtension() ? "Yes" : "No"));


            for (Pair<String, Object> prop : props) {

                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setPhrase(new Phrase(mArabicLigaturizer.process(String.valueOf(prop.getKey() + (prop.getValue() == null ? UNKNOWN : prop.getValue()))), mTextFont));
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfPCell.setPaddingTop(5);
                pdfPCell.setPaddingLeft(40);
                pdfPCell.setPaddingBottom(10);
                buildingTable.addCell(pdfPCell);
            }

            buildingTable.setSpacingBefore(10);


            if (counter == 2) {
                doc.newPage();
            }
            counter++;

            doc.add(buildingTable);


        }

    }


    private void addGeneralModule(Document doc) throws Exception {

        ExposureIndex exposureIndex = null;

        if (mHospital.getLevelOfImportance() != null)
            exposureIndex = mExposureIndexRepo.findBySetKey(mHospital.getLevelOfImportance());

        List<Pair<String, List<Pair<String, Object>>>> sections = new ArrayList<>();

        java.util.List<Pair<String, Object>> hospitalInfo = new ArrayList<>();

        hospitalInfo.add(new Pair("Address: %s", mHospital.getAddress() == null ? UNKNOWN : mHospital.getAddress()));

        hospitalInfo.add(new Pair("Telephone: %s  ", mHospital.getPhone() == null ? UNKNOWN : mHospital.getPhone()));

        String coords = UNKNOWN;
        if (mHospitalEntrance != null) {
            Coordinate coordinate = mHospitalEntrance.getAccessPoint().getCoordinate();
            coords = String.format("%f,%f", coordinate.x, coordinate.y);
        }


        hospitalInfo.add(new Pair("GPS Coordinates: %s", coords));

        hospitalInfo.add(new Pair("Website: %s  ", mHospital.getWebSite() == null ? UNKNOWN : mHospital.getWebSite()));

        sections.add(new Pair<>("Contact Information:", hospitalInfo));

        hospitalInfo = new ArrayList<>();


        hospitalInfo.add(new Pair("Bed Number: %s", getAppropirateString(mHospital.getNumTotalApprovedBeds())));


        hospitalInfo.add(new Pair("Occupancy Rate: %s %%", getAppropirateString(mHospital.getBedOccupancyNormalRate())));

//        hospitalInfo.add(new Pair("Total number of active beds : %s  ", mHospital.getNumTotalActiveBeds() != null && mHospital.getNumTotalActiveBeds() != -1 ? mHospital.getNumTotalActiveBeds() : UNKNOWN));

        sections.add(new Pair<>("Capacity:", hospitalInfo));

        hospitalInfo = new ArrayList<>();

//        hospitalInfo.add(new Pair("Total staff count: %s", mHospital.getTotalStaffCount() != null && mHospital.getTotalStaffCount() != -1 ? mHospital.getTotalStaffCount() : UNKNOWN));

        hospitalInfo.add(new Pair("Service Personnel: %s", getAppropirateString(mHospital.getClinicalStaffCount())));

        hospitalInfo.add(new Pair("Administrative Personnel: %s ", getAppropirateString(mHospital.getNonClinicalStaffCount())));

        sections.add(new Pair<>("Staff:", hospitalInfo));

        hospitalInfo = new ArrayList<>();

        hospitalInfo.add(new Pair("Hospital Type: %s", mHospital.getServiceType() != null && mHospital.getServiceType() != -1 ? SERVICE_TYPE_EN[mHospital.getServiceType()] : UNKNOWN));

        //hospitalInfo.add(new Pair("Affiliation: %s", getAppropirateString(mHospital.getOrganizationalAffiliation())));
        hospitalInfo.add(new Pair("Affiliation: %s", mHospital.getOrganizationalAffiliation() != null && mHospital.getOrganizationalAffiliation() >= 0 ? ORGANIZATIONAL_AFFILIATION_EN[mHospital.getOrganizationalAffiliation()] : UNKNOWN));


//        hospitalInfo.add(new Pair("place in the network of health services: %s", mHospital.getRoleInHealthcareNetwork() != null && mHospital.getRoleInHealthcareNetwork() != -1 ? ROLE_IN_HEALTH_CARE_NETWORK_EN[mHospital.getRoleInHealthcareNetwork()] : UNKNOWN));

//        hospitalInfo.add(new Pair("type of structure: %s", mHospital.getStructureType() != null ? mHospital.getStructureType() : UNKNOWN));

//        hospitalInfo.add(new Pair("population served: %s ", mHospital.getServedPopulation() != null && mHospital.getServedPopulation() != -1 ? mHospital.getServedPopulation() : UNKNOWN));

        hospitalInfo.add(new Pair("Level of importance: %s ", exposureIndex != null ? exposureIndex.getSetNameEn() : UNKNOWN));

        sections.add(new Pair<>("Role in Medical Network:", hospitalInfo));

        PdfPTable generalModuleOuterTable = new PdfPTable(2);
        generalModuleOuterTable.setTableEvent(new PdfPTableEventCustom());
        generalModuleOuterTable.setWidths(new int[]{6, 4});
        generalModuleOuterTable.setWidthPercentage(100);

        PdfPCell generalSectionHeader = new PdfPCell(new Phrase("General Information", mTitleFont));
        generalSectionHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        generalSectionHeader.setColspan(2);
        generalSectionHeader.setBorder(Rectangle.NO_BORDER);

        generalModuleOuterTable.addCell(generalSectionHeader);


        PdfPTable generalModuleInnerTable = new PdfPTable(1);
        generalModuleInnerTable.setWidthPercentage(100);

        for (Pair<String, List<Pair<String, Object>>> section : sections) {

            PdfPTable sectionTable = new PdfPTable(1);
            sectionTable.setPaddingTop(20);

            PdfPCell sectionHeader = new PdfPCell(new Paragraph(section.getKey(), mTextFont));
            sectionHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
            sectionHeader.setBorder(Rectangle.NO_BORDER);
            sectionTable.addCell(sectionHeader);

            for (Pair<String, Object> pair : section.getValue()) {

                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setPhrase(new Phrase(String.format(pair.getKey(), mArabicLigaturizer.process(String.valueOf(pair.getValue()))), mTextFont));
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfPCell.setPaddingTop(5);
                pdfPCell.setPaddingLeft(40);
                sectionTable.addCell(pdfPCell);
            }

            PdfPCell pdfPCell = new PdfPCell(sectionTable);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setPaddingTop(5);
            pdfPCell.setPaddingBottom(20);
            generalModuleInnerTable.addCell(pdfPCell);

        }
        PdfPCell pdfPCell = new PdfPCell(generalModuleInnerTable);
        pdfPCell.setColspan(1);
        pdfPCell.setBorder(Rectangle.NO_BORDER);
        pdfPCell.setPaddingTop(5);
        generalModuleOuterTable.addCell(pdfPCell);


        List<Attachment> attachments = mAttachmentRepo.findAll(HospitalEntrance.class.getSimpleName(), mHospitalEntrance.getRowGuid());
        if (attachments.isEmpty())
            attachments = mAttachmentRepo.findAll(Hospital.class.getSimpleName(), String.valueOf(mHospital.getId()));

        File attachFile = new File("");
        int attachedImagePadding = 5;

        File emptyAttachmentPhoto = new ClassPathResource("imgs/icons8-image-file-512.jpg").getFile();

        if (attachments.isEmpty()) {
            attachFile = emptyAttachmentPhoto;

            attachedImagePadding = 60;
        } else {

            Attachment firstAttachment = attachments.get(0);

            for (Attachment attachment : attachments) {
                if (attachment.getModifyDate().after(firstAttachment.getModifyDate()))
                    firstAttachment = attachment;
            }


            attachFile = mFileStorageService.resolveFileInAttachmentDirec(firstAttachment.getFileName()).toFile();
        }
        if (!attachFile.exists()) {
            attachFile = emptyAttachmentPhoto;
            attachedImagePadding = 60;
        }

        if (attachFile.exists()) {
            Image jpeg = Image.getInstance(attachFile.toURI().toURL());
            jpeg.scaleAbsolute(100, 100);
            jpeg.setBorder(Rectangle.BOX);
            jpeg.setBorderColor(BaseColor.BLUE);
            jpeg.setBorderWidth(1);
            jpeg.setSpacingBefore(20);
            jpeg.setSpacingAfter(20);

            pdfPCell = new PdfPCell(jpeg, true);
            pdfPCell.setColspan(2);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setPadding(attachedImagePadding);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            generalModuleOuterTable.addCell(pdfPCell);
        }

        doc.add(generalModuleOuterTable);
    }

    private Object getAppropirateString(Number number) {

        String bedNum = "";
        if (number != null) {
            if ((number instanceof Double && (Double) number == -1) || (number instanceof Integer && (Integer) number == -1)) {
                bedNum = UNKNOWN;
            } else if ((number instanceof Double && (Double) number == -2) || (number instanceof Integer && (Integer) number == -2)) {
                bedNum = NA;
            } else {
                bedNum = number.toString();
            }
        } else {
            bedNum = UNKNOWN;
        }

        return bedNum;
    }

    private void addMapImgFile(Document doc) throws Exception {
        PdfPTable table = new PdfPTable(2);
        table.setSpacingBefore(10);
        table.setTableEvent(new PdfPTableEventCustom());
        table.setWidthPercentage(100);


        File wmsBigImage = mFileStorageService.resolveFileInReportDirec(mapImgFilesName.get(0)).toFile();

        if (!wmsBigImage.exists()) {
            wmsBigImage = new ClassPathResource("imgs/icons8-image-file-512.jpg").getFile();
        }

        if (wmsBigImage.exists()) {

            PdfPTable locationMapTable = new PdfPTable(1);
            locationMapTable.setWidthPercentage(100);

            Image jpeg = null;

            try {
                jpeg = Image.getInstance(wmsBigImage.toURI().toURL());
            } catch (Exception e) {
                e.printStackTrace();
                // writeToLog(e.toString());
            }
            if (jpeg == null) {
                wmsBigImage = new ClassPathResource("imgs/icons8-image-file-512.jpg").getFile();
                jpeg = Image.getInstance(wmsBigImage.toURI().toURL());
            }


            jpeg.setBorder(Rectangle.BOX);
            jpeg.setBorderColor(BaseColor.BLUE);
            jpeg.setBorderWidth(0.5f);

            PdfPCell pdfPCell = new PdfPCell(jpeg, true);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            locationMapTable.addCell(pdfPCell);


            PdfPCell locationMapTitle = new PdfPCell(new Phrase("Location Map", mTextFont));
            locationMapTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
            locationMapTitle.setBorder(Rectangle.NO_BORDER);

            locationMapTable.addCell(locationMapTitle);

            PdfPCell pdfPCellLocationMapTable = new PdfPCell(locationMapTable);
            pdfPCellLocationMapTable.setBorder(Rectangle.NO_BORDER);
            pdfPCellLocationMapTable.setPadding(10);
            pdfPCellLocationMapTable.setColspan(2);

            table.addCell(pdfPCellLocationMapTable);

        }


        List<Attachment> attachments = mAttachmentRepo.findAll("REPORT", String.valueOf(mHospital.getId()));

        File sitePlanMap = new File("");

        if (!attachments.isEmpty()) {
            Attachment attachment = attachments.get(0);
            for (Attachment a : attachments) {
                if (attachment != null && attachment.getModifyDate().before(a.getModifyDate()))
                    attachment = a;
            }
            sitePlanMap = mFileStorageService.resolveFileInAttachmentDirec(attachment.getFileName()).toFile();
        }

        if (!sitePlanMap.exists()) {
            sitePlanMap = new ClassPathResource("imgs/icons8-image-file-512.jpg").getFile();
        }

        if (sitePlanMap.exists()) {
            PdfPTable sitePlanTable = new PdfPTable(1);
            sitePlanTable.setWidthPercentage(100);


            Image jpeg = Image.getInstance(sitePlanMap.toURI().toURL());
            jpeg.setBorder(Rectangle.BOX);
            jpeg.setBorderColor(BaseColor.BLUE);
            jpeg.scaleAbsolute(230, 230);
            jpeg.setBorderWidth(0.5f);
            PdfPCell pdfPCell = new PdfPCell(jpeg);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setPadding(5);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setColspan(1);

            sitePlanTable.addCell(pdfPCell);

            PdfPCell header = new PdfPCell(new Phrase("Plan/Site Plan", mTextFont));
            header.setHorizontalAlignment(Element.ALIGN_CENTER);
            header.setBorder(Rectangle.NO_BORDER);

            sitePlanTable.addCell(header);

            PdfPCell pdfPCellSitePlanTable = new PdfPCell(sitePlanTable);
            pdfPCellSitePlanTable.setBorder(Rectangle.NO_BORDER);
            pdfPCellSitePlanTable.setPadding(5);
            pdfPCellSitePlanTable.setColspan(1);

            table.addCell(pdfPCellSitePlanTable);
        }

        File wmsSmallImage = mFileStorageService.resolveFileInReportDirec(mapImgFilesName.get(1)).toFile();

        if (!wmsSmallImage.exists()) {
            wmsSmallImage = new ClassPathResource("imgs/icons8-image-file-512.jpg").getFile();
        }

        if (wmsSmallImage.exists()) {

            PdfPTable sketchMapTable = new PdfPTable(1);
            sketchMapTable.setWidthPercentage(100);

            Image jpeg = Image.getInstance(wmsSmallImage.toURI().toURL());

            jpeg.setBorder(Rectangle.BOX);
            jpeg.setBorderColor(BaseColor.BLUE);
            jpeg.setBorderWidth(0.5f);
            PdfPCell pdfPCell = new PdfPCell(jpeg, true);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setPadding(5);
            pdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setColspan(1);

            sketchMapTable.addCell(pdfPCell);

            PdfPCell header = new PdfPCell(new Phrase("Building Sketches", mTextFont));
            header.setHorizontalAlignment(Element.ALIGN_CENTER);
            header.setBorder(Rectangle.NO_BORDER);

            sketchMapTable.addCell(header);

            PdfPCell pdfPCellLocationMapTable = new PdfPCell(sketchMapTable);
            pdfPCellLocationMapTable.setBorder(Rectangle.NO_BORDER);
            pdfPCellLocationMapTable.setPadding(5);
            pdfPCellLocationMapTable.setColspan(1);

            table.addCell(pdfPCellLocationMapTable);
        }

        doc.add(table);

    }


    private Element generateTitle(String s) throws IOException, DocumentException {


        Paragraph generalModuleTitle = new Paragraph(s, mTitleFont);
        generalModuleTitle.setAlignment(Element.ALIGN_LEFT);

        return generalModuleTitle;
    }

    private Optional<String> getSmallWmsImage() {

        return mHospitalRepo.getHospitalSmallBBox(mHospital.getId()).map(boundry ->
                {
                    String imageUrl = String.format(mGeoserverProps.getUrl() + GEOSERVER_SCETCH_MAP_URL, 400, 400, mHospital.getId(), mHospital.getId(), mHospital.getId(), boundry);
                    return downloadWms(imageUrl, mapImgFilesName.get(1));
                }
        );
    }

    private Optional<String> getBigWmsImage() {
        return mHospitalRepo.getHospitalBigBBox2(mHospital.getId()).map(boundry ->
        {

            String entranceId = "";
//            if (mHospitalEntrance != null)
//                entranceId = String.format("name='%s'and", mHospitalEntrance.getName());
            String imageUrl = String.format(mGeoserverProps.getUrl() + GEOSERVER_LOCATION_MAP_URL, 900, 450, entranceId, mHospital.getId(), boundry);
            return downloadWms(imageUrl, mapImgFilesName.get(0));
        });
    }

    private String downloadWms(String imageUrl, String fileName) {

        try {
            URL url = new URL(imageUrl);

            String imgFilePath = mFileStorageService.resolveFileInReportDirec(fileName).toFile().getAbsolutePath();

            InputStream inputStream = url.openStream();

            OutputStream outputStream = new FileOutputStream(imgFilePath);

            byte[] data = new byte[2048];
            int length = 0;

            while ((length = inputStream.read(data)) != -1) {
                outputStream.write(data, 0, length);
            }
            inputStream.close();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private void addRiskInfoTable(Document doc) throws DocumentException {
        DecimalFormat decimalFormat = new DecimalFormat("##.#");

        List<Building> buildings = mBuildingRepository.getUndeletedBuildingsByHospitalId(mHospital.getId());


        double exposureIndex = 1;
        double hazardLevel = 1;


        if (mHospital.getHazardLevels() != null) {
            hazardLevel = mHazardLevelRepo.findBySetKey(mHospital.getHazardLevels()).getHazardLevel();
        }


        if (mHospital.getLevelOfImportance() != null) {
            exposureIndex = mExposureIndexRepo.findBySetKey(mHospital.getLevelOfImportance()).getExposureIndex();
        }


        List<RelativeWeight> relativeWeights = mRelativeWeightRepo.findAll();

        List<Pair<String, List<Pair<String, Object>>>> modulesChartInfos = new ArrayList<>();

        java.util.List<Pair<String, Object>> generalChartInfo = new ArrayList<>();

        generalChartInfo.add(new Pair("Exposure Index: %s %%", decimalFormat.format(exposureIndex * 100)));

        generalChartInfo.add(new Pair("Hazard Level: %s %%", decimalFormat.format(hazardLevel * 100)));

        modulesChartInfos.add(new Pair<>("General Indices", generalChartInfo));

        for (RelativeWeight r : relativeWeights) {

            double sumRiskIndex = 0;
            double sumReliabilityIndex = 0;

            java.util.List<Pair<String, Object>> modulesChartInfo = new ArrayList<>();

            for (Building building : buildings) {

                double relativeWeight = 1;

                double prioritization = 0;

                List<ChartInfo> chartInfos = mChartInfoRepository.getChartInfos(building.getRowGuid());


                for (ChartInfo chartInfo : chartInfos) {

                    if (chartInfo.getModuleName().equalsIgnoreCase(r.getSetKey())) {

                        relativeWeight = r.getRelativeWeight();

                        double riskIndex = (chartInfo.getLst() * exposureIndex * hazardLevel);

                        sumRiskIndex += riskIndex;

                        prioritization += relativeWeight * riskIndex;

                        sumReliabilityIndex += chartInfo.getReliability();

                    }
                }
            }

            modulesChartInfo.add(new Pair("Reliability Index: %s %%",  decimalFormat.format((sumReliabilityIndex / buildings.size()) * 100)));

            modulesChartInfo.add(new Pair("Risk Index: %s %%",  decimalFormat.format((sumRiskIndex / buildings.size()) * 100)));

            modulesChartInfo.add(new Pair("Relative Index: %s %%", decimalFormat.format(r.getRelativeWeight())));

            modulesChartInfos.add(new Pair<>(r.getSetKey(), modulesChartInfo));
        }


        PdfPTable generalModuleInnerTable = new PdfPTable(2);
        generalModuleInnerTable.setTableEvent(new PdfPTableEventCustom());
        generalModuleInnerTable.setWidthPercentage(100);

        for (Pair<String, List<Pair<String, Object>>> section : modulesChartInfos) {

            PdfPTable sectionTable = new PdfPTable(1);
            sectionTable.setPaddingTop(20);

            PdfPCell sectionHeader = new PdfPCell(new Paragraph(section.getKey(), mTextFont));
            sectionHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
            sectionHeader.setBorder(Rectangle.NO_BORDER);
            sectionTable.addCell(sectionHeader);

            for (Pair<String, Object> pair : section.getValue()) {

                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setPhrase(new Phrase(String.format(pair.getKey(), mArabicLigaturizer.process(String.valueOf(pair.getValue()))), mTextFont));
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfPCell.setPaddingTop(5);
                pdfPCell.setPaddingLeft(40);
                sectionTable.addCell(pdfPCell);
            }

            PdfPCell pdfPCell = new PdfPCell(sectionTable);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setPaddingTop(5);
            pdfPCell.setPaddingBottom(20);
            generalModuleInnerTable.addCell(pdfPCell);

        }

        doc.add(generalModuleInnerTable);
    }

    private void generateGraphs2() {

        List<Building> buildings = mBuildingRepository.getUndeletedBuildingsByHospitalId(mHospital.getId());


        Map<String, List<Pair<Building, ChartInfo4Category>>> buildingsChartInfo = new LinkedHashMap<>();

        for (Building building : buildings) {
            List<ChartInfo4Category> chartInfo4Categories = mChartInfo4CategoryRepository.getChartInfo4Category(building.getRowGuid());

            for (ChartInfo4Category chartInfo4Category : chartInfo4Categories) {

                if (buildingsChartInfo.containsKey(chartInfo4Category.getModuleName())) {

                    buildingsChartInfo.get(chartInfo4Category.getModuleName()).add(new Pair(building, chartInfo4Category));

                } else {

                    List<Pair<Building, ChartInfo4Category>> pairs = new ArrayList<>();
                    pairs.add(new Pair(building, chartInfo4Category));
                    buildingsChartInfo.put(chartInfo4Category.getModuleName(), pairs);
                }
            }
        }

        for (Map.Entry<String, List<Pair<Building, ChartInfo4Category>>> buildingsChartInf : buildingsChartInfo.entrySet()) {

            List<String> categories = new ArrayList<>();

            DefaultCategoryDataset dataset = new DefaultCategoryDataset();


            for (Pair<Building, ChartInfo4Category> buildingChartInfo4Category : buildingsChartInf.getValue()) {

                String moduleName = buildingChartInfo4Category.getValue().getModuleName();

                if (moduleName.equals("HAZARD"))
                    continue;

                String moduleAcronym = "";

                switch (moduleName) {
                    case "STRUCTURAL":
                        moduleAcronym = "S";
                        break;
                    case "NON_STRUCTURAL":
                        moduleAcronym = "NS";
                        break;
                    case "INSTITUTIONAL":
                        moduleAcronym = "IN";
                        break;
                }

                String categoryLabel = String.format("%s-%d", moduleAcronym, buildingChartInfo4Category.getValue().getRowId());
                String categoryLongLabel = String.format("%s = %s ", categoryLabel, buildingChartInfo4Category.getValue().getQuestionCategory());


                if (!categories.contains(categoryLongLabel))
                    categories.add(categoryLongLabel);

                String buildingName = buildingChartInfo4Category.getKey().getName();
                if (buildingName != null)
                    dataset.addValue(buildingChartInfo4Category.getValue().getWeightedAvgScore(), buildingName, categoryLabel);
            }

            if (!categories.isEmpty())
                generateGraphImage(dataset, buildingsChartInf.getKey(), "", categories);
        }


        //endregion


        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        double exposureIndex = 1;
        double hazardLevel = 1;

        if (mHospital.getHazardLevels() != null)
            hazardLevel = mHazardLevelRepo.findBySetKey(mHospital.getHazardLevels()).getHazardLevel();

        if (mHospital.getLevelOfImportance() != null)
            exposureIndex = mExposureIndexRepo.findBySetKey(mHospital.getLevelOfImportance()).getExposureIndex();


        List<Pair<String, Double>> buidlingsPrioritizations = new ArrayList<>();

        for (Building building : buildings) {

            double relativeWeight = 1;

            double prioritization = 0;

            List<ChartInfo> chartInfos = mChartInfoRepository.getChartInfos(building.getRowGuid());

            for (ChartInfo chartInfo : chartInfos) {

                if (chartInfo.getModuleName().equals("HAZARD"))
                    continue;

                RelativeWeight r = mRelativeWeightRepo.findByModule(chartInfo.getModuleName());

                if (r != null) {

                    relativeWeight = r.getRelativeWeight();

                }
                double riskIndex = (chartInfo.getLst() * exposureIndex * hazardLevel);

                String buildingName = building.getName();

                if (buildingName != null) {

                    dataset.addValue(riskIndex * 100, buildingName, chartInfo.getModuleName());

                    prioritization += relativeWeight * riskIndex;
                }

            }

            buidlingsPrioritizations.add(new Pair<>(building.getName(), prioritization));

        }

        for (Pair<String, Double> buidlingsPrioritization : buidlingsPrioritizations) {
            if (buidlingsPrioritization.getKey() != null)
                dataset.addValue(buidlingsPrioritization.getValue(), buidlingsPrioritization.getKey(), "Total Risk");
        }
        List<RelativeWeight> relativeWeights = mRelativeWeightRepo.findAll();

        StringBuilder stringBuilder = new StringBuilder();

        for (RelativeWeight relativeWeight : relativeWeights) {

            if (stringBuilder.length() > 0)
                stringBuilder.append(",");

            stringBuilder.append(String.format("%d%% * %s", (int) relativeWeight.getRelativeWeight(), relativeWeight.getSetKey()));
        }
        generateGraphImage(dataset, "Overall Score", "Relative Scoring Weights:" + stringBuilder.toString(), new ArrayList<>());


    }

//    private void generateGraphs() {
//
//        java.util.List<QuestionCategory> catesWitData = mQuestionCategoryRepo.getCates4Report(mHospital.getId());
//
//        java.util.List<QuestionCategory> allCates = mQuestionCategoryRepo.getAllCates();
//
//        for (QuestionCategory cat : allCates) {
//            for (QuestionCategory catesWitDatum : catesWitData) {
//                if (catesWitDatum.getId() == cat.getId()) {
//                    cat.setEvaluationAvg(catesWitDatum.getEvaluationAvg());
//                }
//            }
//
//        }
//
//        Map<String, java.util.List<QuestionCategory>> moduleQuestionCats = new HashMap<>();
//
//        for (QuestionCategory questionCategory : allCates) {
//
//            if (moduleQuestionCats.containsKey(questionCategory.getModuleName())) {
//                moduleQuestionCats.get(questionCategory.getModuleName()).add(questionCategory);
//            } else {
//
//                java.util.List questionCats = new ArrayList<>();
//                questionCats.add(questionCategory);
//                moduleQuestionCats.put(questionCategory.getModuleName(), questionCats);
//            }
//        }
//
//
//        for (Map.Entry<String, java.util.List<QuestionCategory>> moduleQuestionCat : moduleQuestionCats.entrySet()) {
//
//            // mModuleComments.put(moduleQuestionCat.getKey(), mEvaluationRepository.getAllCommentsByHospitalIdAndModule(mHospital.getId(), moduleQuestionCat.getKey()));
//
//            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//
//            if (moduleQuestionCat.getKey().equals("HAZARD"))
//                continue;
//
//            List<String> categories = new ArrayList<>();
//
//            for (int i = 0; i < moduleQuestionCat.getValue().size(); i++) {
//
//                QuestionCategory questionCategory = moduleQuestionCat.getValue().get(i);
//
//
//                dataset.addValue(questionCategory.getEvaluationAvg(), questionCategory.getCategoryTitleEn(), "Question Categories");
//
//            }
//
//            generateGraphImage(dataset, moduleQuestionCat.getKey(), "", new ArrayList<>());
//        }
//
//        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//
//        double sumTotal = 0;
//        double countTotal = 0;
//
//        for (Map.Entry<String, java.util.List<QuestionCategory>> moduleQuestionCat : moduleQuestionCats.entrySet()) {
//
//            if (moduleQuestionCat.getKey().equals("HAZARD"))
//                continue;
//
//            double sumPerModule = 0;
//
//            countTotal += moduleQuestionCat.getValue().size();
//
//            for (QuestionCategory questionCategory : moduleQuestionCat.getValue()) {
//                sumPerModule += questionCategory.getEvaluationAvg();
//            }
//
//            sumTotal += sumPerModule;
//
//            double meanVal = 0;
//
//            try {
//                meanVal = sumPerModule / moduleQuestionCat.getValue().size();
//            } catch (Exception e) {
//            }
//
//            dataset.addValue(meanVal, "Modules scores", moduleQuestionCat.getKey());
//
//        }
//        double meanVal = 0;
//        try {
//            meanVal = sumTotal / countTotal;
//        } catch (Exception e) {
//        }
//        dataset.addValue(meanVal, "Total Risk", "Modules scores");
//        generateGraphImage(dataset, "PRIORITIZING", "", new ArrayList<>());
//
//    }

    private void generateGraphImage(DefaultCategoryDataset dataset, String title, String xAxisText, List<String> subtitles) {
        ChartFactory.setChartTheme(StandardChartTheme.createLegacyTheme());
        JFreeChart chart = ChartFactory.createBarChart(
                title + " INDEX", xAxisText /* x-axis label*/,
                "Percent" /* y-axis label */, dataset);
//        chart.addSubtitle(new TextTitle("The " + title + " risk index for this hospital is: X% with a reliability index of Y%."));

        CategoryPlot plot = (CategoryPlot) chart.getPlot();

        plot.setDomainGridlinesVisible(true);
        plot.setRangeCrosshairVisible(true);
        plot.setRangeCrosshairPaint(Color.blue);

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        //rangeAxis.setRange(0, 100);
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setMaximumBarWidth(.05);

        renderer.setDefaultItemLabelGenerator(new CustomCategoryItemLabelGenerator());
        renderer.setDefaultItemLabelsVisible(true);
        ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,
                TextAnchor.TOP_CENTER);

        renderer.setDefaultPositiveItemLabelPosition(position);

        java.awt.Font font3 = new java.awt.Font("Dialog", java.awt.Font.PLAIN, 18);

        renderer.setDefaultItemLabelFont(font3);

        renderer.setDrawBarOutline(true);

        Color[] colors = new Color[]{
                new Color(138, 138, 138),
                new Color(241, 97, 155)
        };


        LinearGradientPaint linearGradientPaint1 = new LinearGradientPaint(
                5, 5, 5, 10,
                new float[]{0.2f, .3f}, colors, MultipleGradientPaint.CycleMethod.REPEAT);

        renderer.setSeriesPaint(0, linearGradientPaint1);


        colors = new Color[]{
                new Color(138, 138, 138),
                new Color(18, 193, 149)

        };

        LinearGradientPaint linearGradientPaint2 = new LinearGradientPaint(
                5, 5, 10, 10,
                new float[]{0.2f, .3f}, colors, MultipleGradientPaint.CycleMethod.REPEAT);


        renderer.setSeriesPaint(1, linearGradientPaint2);

        colors = new Color[]{
                new Color(138, 138, 138),
                new Color(255, 243, 24, 170)
        };

        LinearGradientPaint linearGradientPaint3 = new LinearGradientPaint(
                5, 10, 10, 5,
                new float[]{0.1f, .2f}, colors, MultipleGradientPaint.CycleMethod.REPEAT);

        renderer.setSeriesPaint(2, linearGradientPaint3);


        colors = new Color[]{
                new Color(138, 138, 138),
                new Color(181, 0, 136, 152)
        };

        LinearGradientPaint linearGradientPaint4 = new LinearGradientPaint(
                5, 1, 4, 9,
                new float[]{0.1f, .2f}, colors, MultipleGradientPaint.CycleMethod.REPEAT);

        renderer.setSeriesPaint(3, linearGradientPaint4);


        colors = new Color[]{
                new Color(138, 138, 138),
                new Color(123, 224, 0, 170)
        };

        LinearGradientPaint linearGradientPaint5 = new LinearGradientPaint(
                1, 5, 5, 5,
                new float[]{0.1f, .2f}, colors, MultipleGradientPaint.CycleMethod.REPEAT);

        renderer.setSeriesPaint(4, linearGradientPaint5);

        plot.setBackgroundPaint(Color.WHITE);

        StandardBarPainter painter = new StandardBarPainter();

        BarRenderer.setDefaultBarPainter(painter);

        renderer.setDefaultLegendShape(new java.awt.Rectangle(30, 30));

//        Color primaryColor = Color.red;
//        Color secondaryColor = Color.blue;
//
//        GradientPaint gpVertical = new GradientPaint(5, 5, primaryColor, 10, 5,
//                secondaryColor, true);
//
//        GradientPaint gpHorizontal = new GradientPaint(5, 5, primaryColor, 5,
//                10, secondaryColor, true);
//
//        GradientPaint gpUpSlant = new GradientPaint(5, 5, primaryColor, 10, 10,
//                secondaryColor, true);
//
//        renderer.setSeriesPaint(1, gpVertical);
//        renderer.setSeriesPaint(2, gpHorizontal);
//        renderer.setSeriesPaint(3, gpUpSlant);


//        GradientPaint gp0 = new GradientPaint(0.0f, 0.0f, Color.blue,
//                0.0f, 0.0f, new Color(0, 0, 64));
//        GradientPaint gp1 = new GradientPaint(0.0f, 0.0f, Color.green,
//                0.0f, 0.0f, new Color(0, 64, 0));
//        GradientPaint gp2 = new GradientPaint(0.0f, 0.0f, Color.red,
//                0.0f, 0.0f, new Color(64, 0, 0));
//        renderer.setSeriesPaint(0, gp0);
//        renderer.setSeriesPaint(1, gp1);
//        renderer.setSeriesPaint(2, gp2);


        chart.getLegend().setFrame(BlockBorder.NONE);


        TextTitle legendContent = new TextTitle();
        legendContent.setMaximumLinesToDisplay(subtitles.size());

        StringBuilder stringBuilder = new StringBuilder();

        for (String subtitle : subtitles) {
            stringBuilder.append(subtitle);
            stringBuilder.append("    ");
        }
        legendContent.setText(stringBuilder.toString());
        legendContent.setPadding(20, 20, 20, 20);
        chart.addSubtitle(legendContent);

        BufferedImage objBufferedImage = chart.createBufferedImage(900, 500);
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        try {
            ImageIO.write(objBufferedImage, "png", bas);
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] byteArray = bas.toByteArray();

        String imgFilePath = mFileStorageService.resolveFileInReportDirec(String.format("%s_%s.png", fileName, title)).toFile().getAbsolutePath();

        InputStream in = new ByteArrayInputStream(byteArray);
        BufferedImage image = null;
        try {

            image = ImageIO.read(in);

            File outputfile = new File(imgFilePath);

            ImageIO.write(image, "png", outputfile);

        } catch (IOException e) {
            e.printStackTrace();
        }

        mGraphImgsFiles.add(imgFilePath);
    }


    private void writeToLog(String string) {
        Path filePath = mFileStorageService.generateFile("STATUS_" + fileName.replace("pdf", "txt"));

        File file = filePath.toFile();

        try {
            if (file.exists())
                file.delete();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(string);
            fileWriter.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
