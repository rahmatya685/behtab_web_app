package ir.zngis.behtab.Task;

import org.jfree.chart.labels.AbstractCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CustomCategoryItemLabelGenerator extends StandardCategoryItemLabelGenerator {
    protected CustomCategoryItemLabelGenerator( ) {
        super(DEFAULT_LABEL_FORMAT_STRING,new DecimalFormat("0.0"));
    }

}
