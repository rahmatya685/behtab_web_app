package ir.zngis.behtab.Task;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "geoserver")
public class GeoserverProps {

    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
