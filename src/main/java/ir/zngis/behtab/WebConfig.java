package ir.zngis.behtab;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/*
enables CORS requests from any origin to any endpoint in the application.
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {


    @Bean
    public JtsModule jtsModule() {
        return new JtsModule();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**") ;
//        registry.addMapping("/protected/**");
////       registry.addMapping("/***") ;

        registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE").allowedOrigins("*")
                .allowedHeaders("*");

    }

}
