package ir.zngis.behtab.Entrance;


import ir.zngis.behtab.model.Hospital;
import ir.zngis.behtab.model.HospitalEntrance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntranceRepository extends JpaRepository<HospitalEntrance,String> {


    HospitalEntrance findAllByHospitalByHospitalId(Hospital hospital);



    @Query(value = "select * from hospital_entrance where hospital_entrance.hospital_id = :hospital_id order by hospital_entrance.modify_date limit 1",nativeQuery = true)
    HospitalEntrance getFirstInsertedEntrance(@Param("hospital_id")int hospital_id);

    List<HospitalEntrance> getAllByHospitalByHospitalId(  int hospitalId);


    @Query(value = "SELECT * FROM hospital_entrance   WHERE  hospital_id IN (select hospital.id from hospital   where  hospital.evaluator_user_id = :userId or hospital.surveyor_user_id = :userId)",nativeQuery = true)
    List<HospitalEntrance> getEntranceByUserId(@Param("userId") int userId);
}
