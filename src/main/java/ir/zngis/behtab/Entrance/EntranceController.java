package ir.zngis.behtab.Entrance;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.model.HospitalEntrance;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/protected/")
public class EntranceController {


    @Autowired
    EntranceRepository entranceRepository;

    @Autowired
    HospitalRepo hospitalRepo;

    @Autowired
    UserRepo userRepo;



    @PostMapping("entrances/getAllEntrances4User")
    public List<HospitalEntrance> getAllBuildingsOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return entranceRepository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities()) || ControllerHelper.UserIsSurveyor(user.getAuthorities())) {
                        return entranceRepository.getEntranceByUserId(user.getId());
                    } else
                        return new ArrayList<HospitalEntrance>();
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("hospitals/{hospitalid}/entrances/addAll")
    public Optional<List<HospitalEntrance>> insert(@RequestBody List<HospitalEntrance> entrances, @PathVariable("hospitalid") int hospitalid, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->

                hospitalRepo.findById(hospitalid).map(hospital -> {
                    if (!entrances.isEmpty()) {
                        entrances.forEach(building -> {
                            building.setHospitalByHospitalId(hospital);
                            building.setEditorUser(user);
                        });
                        entranceRepository.saveAll(entrances);
                    }
                    return getAllBuildingsOfUser(principal);
                })

        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping(value = "hospitals/{hospitalid}/entrances/add")
    public HospitalEntrance insert(@RequestBody HospitalEntrance hospitalEntrance, @PathVariable("hospitalid") int hospitalid, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    return hospitalRepo.findById(hospitalid).map(hospital -> {
                        hospitalEntrance.setHospitalByHospitalId(hospital);
                        hospitalEntrance.setEditorUser(user);
                        hospitalEntrance.setModifyDate(new Date());
                        return entranceRepository.save(hospitalEntrance);
                    }).orElse(hospitalEntrance);

                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @PostMapping(value = "hospitals/{hospitalid}/entrances", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<HospitalEntrance> get(@RequestBody int hospitalId, @PathVariable("hospitalid") int hospitalid) {
        return entranceRepository.getAllByHospitalByHospitalId(hospitalId);
    }


    @PostMapping("hospitals/{hospitalid}/entrances/delete")
    public boolean delete(@RequestBody List<String> buildingIds, @PathVariable("hospitalid") int hospitalid, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {
                    List<HospitalEntrance> buildings = entranceRepository.findAllById(buildingIds);
                    buildings.forEach(building -> {
                        building.setModifyDate(new Date());
                        building.setEditorUser(user);
                        building.setStatus(EntityStatus.DELETE);
                    });
                    entranceRepository.saveAll(buildings);
                    return true;
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }
}
