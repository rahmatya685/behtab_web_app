package ir.zngis.behtab.evaluation;


import ir.zngis.behtab.model.QuestionCategory4Evaluation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionCategory4EvaluationRepository extends JpaRepository<QuestionCategory4Evaluation,Integer> {


    @Query( value = "select * from  getbuildingsscores(:p_hospital_id)",nativeQuery = true)
    List<QuestionCategory4Evaluation> getEvaluationsSocre4Building(@Param("p_hospital_id") int hospitalId);

}
