package ir.zngis.behtab.evaluation;

import ir.zngis.behtab.model.ChartInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChartInfoRepository extends JpaRepository<ChartInfo, String> {


    @Query(value = "select * from calc_risk_primary_info_by_module_4_building(:buildingId)", nativeQuery = true)
    List<ChartInfo> getChartInfos(@Param("buildingId") String buildingId);
}
