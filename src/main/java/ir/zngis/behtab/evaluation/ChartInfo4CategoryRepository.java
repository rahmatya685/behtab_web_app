package ir.zngis.behtab.evaluation;

import ir.zngis.behtab.model.ChartInfo4Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChartInfo4CategoryRepository extends JpaRepository<ChartInfo4Category, String> {


    @Query(value = "select * from get_chart_info_4_module_level3(:buildingId)", nativeQuery = true)
    public List<ChartInfo4Category> getChartInfo4Category(@Param("buildingId") String buildingId);

}
