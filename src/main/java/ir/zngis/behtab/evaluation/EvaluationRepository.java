package ir.zngis.behtab.evaluation;


import ir.zngis.behtab.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvaluationRepository extends JpaRepository<Evaluation,String> {


    @Query(value = "delete  * from evaluation where building_id =   :buildingId  or clinical_unit_id in " +
            "(select  clinical_unit.id from clinical_unit where clinical_unit.building_id = :buildingId ) or " +
            "(select  non_clinical_unit.id from non_clinical_unit where non_clinical_unit.building_id = :buildingId ) or " +
            "(select  surgery_department.id from surgery_department where surgery_department.building_id = :buildingId )",nativeQuery = true)
    boolean deleteAllEvaluationsByBuilding(@Param("buildingId")String buildingId);


    boolean deleteAllBySurgeryDepartmentBySurgeryDepartmentId(String surgicalUnitId);


    boolean deleteAllByNonClinicalUnitByNonClinicalUnitId(String nonClinicalUnitId);

    boolean deleteAllByClinicalUnitByClinicalUnitId(String clinicalUnitId);


    @Query(value = " select evaluation.* from evaluation  \n" +
            "             where  evaluation.status != 1 and \n" +
            "             evaluation.building_id in (select id from building where building.hospital_id in(select id from hospital where surveyor_user_id = :userId OR evaluator_user_id = :userId) and (Building.status != 1 or Building.status is null )) OR\n" +
            "             evaluation.clinical_unit_id in (select id from clinical_unit where clinical_unit.building_id in (select id from building where building.hospital_id in(select id from hospital where surveyor_user_id = :userId OR evaluator_user_id = :userId) and (Building.status != 1 or  Building.status is null )) and (clinical_unit.status != 1 or clinical_unit.status is null)) OR\n" +
            "             evaluation.non_clinical_unit_id in (select id from non_clinical_unit where non_clinical_unit.building_id in (select id from building where building.hospital_id in(select id from hospital where surveyor_user_id = :userId OR evaluator_user_id = :userId) and (Building.status != 1 or Building.status is null )) and (non_clinical_unit.status != 1 or non_clinical_unit.status is null)) OR\n" +
            "             evaluation.surgery_department_id in (select id from surgery_department where surgery_department.building_id in (select id from building where  building.hospital_id in(select id from hospital where surveyor_user_id = :userId OR evaluator_user_id = :userId) and (Building.status != 1 or Building.status is null )) and (surgery_department.status != 1 or surgery_department.status is null))",nativeQuery = true)
    List<Evaluation> findAllByEditorUser(@Param("userId") int userId);



    @Query(value = "select evaluation.* from evaluation join question on evaluation.question_id = question.id      \n" +
            "                          where  question.status!=1 and  evaluation.status!=1   and  \n" +
            "             (building_id in (select building.id from building where hospital_id = :hospitalId and status!=1) \n" +
            "              or     clinical_unit_id in (select  clinical_unit.id from clinical_unit where clinical_unit.building_id in (select building.id from building where hospital_id = :hospitalId and status!=1) and status!=1 )  \n" +
            "                          or   \n" +
            "                          non_clinical_unit_id in (select  non_clinical_unit.id from non_clinical_unit where non_clinical_unit.building_id in (select building.id from building where hospital_id = :hospitalId and status!=1) and status!=1 ) \n" +
            "                           or  \n" +
            "                          surgery_department_id in (select  surgery_department.id from surgery_department where surgery_department.building_id in (select building.id from building where hospital_id = :hospitalId    and status!=1) and status!=1 )) order by evaluation.question_id,evaluation.modify_date asc ;",nativeQuery = true)
    List<Evaluation> getAllByHospitalId(@Param("hospitalId") int hospitalId);


    @Query(value = "select evaluation.comment  as comment ,evaluation.id as id, question.id as question_id,question.category_id as category_id,question.question_title_en  as question_title_en from evaluation join question on evaluation.question_id =question.id from evaluation where evaluation.comment is not null and length(trim(evaluation.comment))>0 and  (building_id in (select building.id from building where building.hospital_id = :hospitalId) \n" +
            "OR clinical_unit_id in(select clinical_unit.id from clinical_unit where building_id in (select building.id from building where building.hospital_id = :hospitalId))\n" +
            "OR non_clinical_unit_id in(select non_clinical_unit.id from non_clinical_unit where building_id in (select building.id from building where building.hospital_id = :hospitalId))\n" +
            "OR surgery_department_id in(select surgery_department.id from surgery_department where building_id in (select building.id from building where building.hospital_id = :hospitalId))\n" +
            "and question_id in (select id from question where category_id in (select id from question_category where module_name like :module)))",nativeQuery = true)
    List<QuestionTitleWithComment> getAllCommentsByHospitalIdAndModule(@Param("hospitalId") int hospitalId, @Param("module")String module);






}
