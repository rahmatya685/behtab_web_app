package ir.zngis.behtab.evaluation;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.model.Evaluation;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.model.UserRole;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/protected/")
public class EvaluationController {

    @Autowired
    EvaluationRepository repository;


    @Autowired
    UserRepo userRepo;

    @Autowired
    HospitalRepo hospitalRepo;

    @PostMapping("evaluation/addAll")
    public List<Evaluation> insert(@RequestBody List<Evaluation> evaluations, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {

                    evaluations.forEach(evaluation ->
                            evaluation.setEditorUser(user)
                    );
                    return repository.saveAll(evaluations);
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping(value = "evaluations")
    public List<Evaluation> getEvaluations(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return repository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities())) {
                        return repository.findAllByEditorUser(user.getId());
                    } else
                        return new ArrayList<Evaluation>();
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }


    @PostMapping(value = "evaluation/delete/{id}")
    public boolean delete(@PathVariable("id") String id, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> repository.findById(id).map(evaluation ->
                {
                    evaluation.setUserId(user.getId());
                    evaluation.setStatus(EntityStatus.DELETE);
                    repository.save(evaluation);
                    return true;
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.ENTITY_NOT_FOUND))).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @PostMapping("evaluation/delete")
    public boolean delete(@RequestBody List<String> ids, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->
                {
                    List<Evaluation> evaluations = repository.findAllById(ids);
                    evaluations.forEach(evaluation -> {
                                evaluation.setUserId(user.getId());
                                evaluation.setStatus(EntityStatus.DELETE);
                            }
                    );

                    repository.saveAll(evaluations);
                    return true;
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @PostMapping("evaluation/update")
    public List<Evaluation> update(@RequestBody List<Evaluation> evaluations) {
        return repository.saveAll(evaluations);
    }




}
