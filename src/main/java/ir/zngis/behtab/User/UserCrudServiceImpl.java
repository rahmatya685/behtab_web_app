package ir.zngis.behtab.User;


import ir.zngis.behtab.model.User;
import ir.zngis.behtab.model.UserRole;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserCrudServiceImpl implements UserCrudService {


    @Autowired
    UserRepo userRepo;


    @Autowired
    UserRolesRepo userAttributeRepo;

    @Override
    public User save(User user) {
        if (user.getId() == 0) {
            Optional<User> foundUser = findByUsername(user.getUsername());
            if (foundUser.isPresent()) {
                user.setId(foundUser.get().getId());

                userAttributeRepo.deleteAllByUser(user);
            }
        }

        for (UserRole authority : user.getAuthorities()) {
            authority.setUser(user);
        }

        User updateUser = userRepo.save(user);

        //userAttributeRepo.saveAll(user.getAuthorities());

        return updateUser;
    }


    @Override
    public Optional<User> find(Integer id) {
        return userRepo.findById(id);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public Optional<User> findByToken(String token) {
        return userRepo.findByToken(token);
    }


    @Override
    public Page<User> getUsers(Pageable pageable) {
        return userRepo.getAllUsers(pageable);
    }


    @Override
    public boolean deleteUser(User user) {
        try {
            user.setStatus(EntityStatus.DELETE);
            userRepo.save(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
