package ir.zngis.behtab.User;

import ir.zngis.behtab.model.User;
import ir.zngis.behtab.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public interface UserRolesRepo extends JpaRepository<UserRole, Integer> {

//    @Modifying
//    @Transactional
//    @Query(value = "delete  from user_roles where user_id = :userId ", nativeQuery = true)
////    void deleteByUserId(@Param("userId") int userId);


//    @Transactional
//    void deleteAllByUser(User user);

    @Modifying
    @Transactional
    @Query("delete  from UserRole  u where u.user = :user")
    void deleteAllByUser(@Param("user") User user);

    @Modifying
    @Transactional
    @Query("delete  from UserRole  u where u.id in (:roleIds)")
    void deleteAll(@Param("roleIds") List<Integer> roleIds);
}
