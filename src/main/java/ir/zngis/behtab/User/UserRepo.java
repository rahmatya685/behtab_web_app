package ir.zngis.behtab.User;

import ir.zngis.behtab.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepo extends JpaRepository<User, Integer> {

    @Query(value = "select * from public.user where  status != 1  AND  public.user.user_name like :username ", nativeQuery = true)
    Optional<User> findByUsername(@Param("username") String username);

    Optional<User> findByToken(String token);


    @Query(value = "select * from public.user where  status != 1 order  by id ", nativeQuery = true)
    Page<User> getAllUsers(Pageable pageable);


    @Query(value = "select * from public.user  where public.user.id in (select user_roles.user_id from user_roles where user_roles.authority like :role) ", nativeQuery = true)
    List<User> getAllByRole(@Param("role") String role);


}
