package ir.zngis.behtab.User;

import ir.zngis.behtab.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
* User Security operations like login, logout and CURD operations on {@link ir.zngis.behtab.model.User}
*
*
 */
public interface UserCrudService {

    User save(User user);

    boolean deleteUser(User user);


    Optional<User> find(Integer id);

    Optional<User> findByUsername(String username);


    Optional<User> findByToken(String token);


    Page<User> getUsers(Pageable pageable);

}
