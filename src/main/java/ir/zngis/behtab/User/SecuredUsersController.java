package ir.zngis.behtab.User;


import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.model.User;
import ir.zngis.behtab.model.UserRole;
import ir.zngis.behtab.model.UserRoleType;
import ir.zngis.behtab.security.UserAuthenticationService;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/protected/users")
public class SecuredUsersController {

    @Autowired
    UserAuthenticationService authentication;

    @Autowired
    UserCrudService userCrudService;


    @Autowired
    UserRepo userRepo;

    @Autowired
    UserRolesRepo userRolesRepo;

    @PostMapping("/current")
    User getCurrent(@AuthenticationPrincipal final User user) {
        return user;
    }

    @GetMapping("/logoutdevice")
    String logoutDevice(@AuthenticationPrincipal final User user, HttpServletRequest httpServletRequest,@RequestParam("imei") String imei) {

        authentication.logout(user,imei);
        return "login.html";
    }

    @GetMapping("/logout")
    String logout(@AuthenticationPrincipal final User user, HttpServletRequest httpServletRequest ) {
        InetAddress inetAddress = ControllerHelper.getClientIpAddr(httpServletRequest);

        authentication.logout(user,inetAddress.getHostAddress());
        return "login.html";
    }

    @PostMapping("/evaluators")
    List<User> getEvaluatorUsers() {
        return userRepo.getAllByRole(String.valueOf(Constants.UserRoles.Evaluator));
    }

    @PostMapping("/surveyors")
    List<User> getSurveyorsUsers() {
        return userRepo.getAllByRole(String.valueOf(Constants.UserRoles.Surveyor));
    }


    @PostMapping("")
    Object getUsers(Pageable pageable, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return userCrudService.getUsers(pageable);
                    } else {
                        return new ResourceNotFoundException(Constants.ACTION_NOT_ALLOWED);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/update")
    User updateUser(@RequestBody User editedUser) {
        Optional<User> oldUser = userCrudService.find(editedUser.getId());
        if (oldUser.isPresent()) {

            editedUser.setToken(oldUser.get().getToken());

            //  userRolesRepo.deleteAllByUser(editedUser);


            Iterator<UserRole> userRoleIterator = oldUser.get().getAuthorities().iterator();

            List<Integer> roleIds = new ArrayList<>();

            while (userRoleIterator.hasNext())
                roleIds.add(userRoleIterator.next().getId());

            //userRolesRepo.deleteAll(roleIds);


            for (UserRole authority : editedUser.getAuthorities()) {
                authority.setUser(editedUser);
            }
            oldUser.get().getAuthorities().clear();
            userRepo.save(oldUser.get());

            return userRepo.save(editedUser);


        } else {
            return userCrudService.save(editedUser);
        }

    }


    @PostMapping("/delete")
    Object deleteUser(@RequestBody User user, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(requestingUser -> {
                    if (ControllerHelper.UserIsAdmin(requestingUser.getAuthorities())) {
                        return userCrudService.deleteUser(user);
                    } else {
                        return new ResourceNotFoundException(Constants.DELETING_USER_IS_ALLOWED_4_ADMIN);
                    }
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("/roles")
    List<UserRoleType> getRoles(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {

                    List<UserRoleType> userRoles = new ArrayList<>();

                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {

                        userRoles.add(new UserRoleType(Constants.UserRoles.Surveillance, "ناظر", "Surveillance"));
                        userRoles.add(new UserRoleType(Constants.UserRoles.Admin, "مدیر", "Admin"));
                        userRoles.add(new UserRoleType(Constants.UserRoles.Surveyor, "سرپرست تیم - برداشتگر", "Surveyor"));
                        userRoles.add(new UserRoleType(Constants.UserRoles.Evaluator, "ارزیاب", "Evaluator"));

                    } else if (ControllerHelper.UserIsSurvillance(user.getAuthorities())) {
//                        userRoles.add(new Pair<>(1, "برداشت گر"));
//                        userRoles.add(new Pair<>(2, "ارزیاب"));
                    }

                    return userRoles;
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }
}
