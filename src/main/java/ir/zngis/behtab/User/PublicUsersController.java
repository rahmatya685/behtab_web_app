package ir.zngis.behtab.User;


import ir.zngis.behtab.model.User;
import ir.zngis.behtab.security.UserAuthenticationService;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.swing.text.html.Option;
import java.net.InetAddress;
import java.util.Optional;

@RestController
@RequestMapping("/users")
final class PublicUsersController {

    @Autowired
    UserAuthenticationService userAuthenticationService;

    @Autowired
    UserCrudService userCrudService;

    @GetMapping("/deviceLogin")
    String deviceLogin(@RequestParam("userName") String userName, @RequestParam("password") String password,
                 HttpServletRequest httpServletRequest, @RequestParam("setupId") String setupId,@RequestParam("imei") String imei) {

//        InetAddress inetAddress = ControllerHelper.getClientIpAddr(httpServletRequest);

        return userAuthenticationService.deviseLogin(userName, password, imei,setupId).orElseThrow(() -> new RuntimeException(Constants.INVALID_USERNAME_AND_PASSWORD));
    }

    @GetMapping("/login")
    String login(@RequestParam("userName") String userName, @RequestParam("password") String password,
                 HttpServletRequest httpServletRequest) {


        InetAddress inetAddress = ControllerHelper.getClientIpAddr(httpServletRequest);

        return userAuthenticationService.login(userName, password, inetAddress.getHostAddress()).orElseThrow(() -> new RuntimeException(Constants.INVALID_USERNAME_AND_PASSWORD));
    }


    @GetMapping("/register")
    String register(User user, HttpServletRequest httpServletRequest) {

        InetAddress inetAddress = ControllerHelper.getClientIpAddr(httpServletRequest);

        return userAuthenticationService.register(user, inetAddress.getHostAddress()).orElseThrow(() -> new RuntimeException(Constants.INVALID_USERNAME_AND_PASSWORD));
    }


}
