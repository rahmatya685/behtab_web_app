package ir.zngis.behtab.User;

import ir.zngis.behtab.model.LoginDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LoginDeviceRepo extends JpaRepository<LoginDevice,Integer> {
}
