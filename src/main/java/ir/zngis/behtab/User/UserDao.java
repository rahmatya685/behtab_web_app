package ir.zngis.behtab.User;

import ir.zngis.behtab.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "UsersController",path = "/listAllUsers")
public interface UserDao  extends JpaRepository<User,Integer> {

}
