package ir.zngis.behtab.CandidateLocation;


import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.model.CandidateLocation;
import ir.zngis.behtab.model.ResourceNotFoundException;
import ir.zngis.behtab.util.Constants;
import ir.zngis.behtab.util.ControllerHelper;
import ir.zngis.behtab.util.EntityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/protected/")
public class CandidateLocationController {

    @Autowired
    HospitalRepo hospitalRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    CandidateLocationRepository candidateLocationRepository;



    @PostMapping("candidatelocations/getAllCandidatelocations4User")
    public List<CandidateLocation> getAllBuildingsOfUser(Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    if (ControllerHelper.UserIsAdmin(user.getAuthorities())) {
                        return candidateLocationRepository.findAll();
                    } else if (ControllerHelper.UserIsEvaluator(user.getAuthorities()) || ControllerHelper.UserIsSurveyor(user.getAuthorities())) {
                        return candidateLocationRepository.getCandidateLocationByUserId(user.getId());
                    } else
                        return new ArrayList<CandidateLocation>();
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping("hospitals/{hospitalid}/candidatelocations/addAll")
    public Optional<List<CandidateLocation>> insert(@RequestBody List<CandidateLocation> entrances, @PathVariable("hospitalid") int hospitalid, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user ->

                hospitalRepo.findById(hospitalid).map(hospital -> {
                    if (!entrances.isEmpty()) {
                        entrances.forEach(building -> {
                            building.setHospitalByHospitalId(hospital);
                            building.setEditorUser(user);
                        });
                        candidateLocationRepository.saveAll(entrances);
                    }
                    return getAllBuildingsOfUser(principal);
                })

        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));
    }

    @PostMapping(value = "hospitals/{hospitalid}/candidatelocations/add")
    public CandidateLocation insert(@RequestBody CandidateLocation hospitalEntrance, @PathVariable("hospitalid") int hospitalid, Principal principal) {
        return userRepo.findByUsername(principal.getName()).map(user -> {
                    return hospitalRepo.findById(hospitalid).map(hospital -> {
                        hospitalEntrance.setHospitalByHospitalId(hospital);
                        hospitalEntrance.setEditorUser(user);
                        hospitalEntrance.setModifyDate(new Date());
                        return candidateLocationRepository.save(hospitalEntrance);
                    }).orElse(hospitalEntrance);

                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }

    @PostMapping(value = "hospitals/{hospitalid}/candidatelocations", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<CandidateLocation> get(@RequestBody int hospitalId, @PathVariable("hospitalid") int hospitalid) {
        return candidateLocationRepository.findAllByHospitalByHospitalId(hospitalId);
    }


    @PostMapping("hospitals/{hospitalid}/candidatelocations/delete")
    public boolean delete(@RequestBody List<String> buildingIds, @PathVariable("hospitalid") int hospitalid, Principal principal) {

        return userRepo.findByUsername(principal.getName()).map(user -> {
                    List<CandidateLocation> buildings = candidateLocationRepository.findAllById(buildingIds);
                    buildings.forEach(building -> {
                        building.setModifyDate(new Date());
                        building.setEditorUser(user);
                        building.setStatus(EntityStatus.DELETE);
                    });
                    candidateLocationRepository.saveAll(buildings);
                    return true;
                }
        ).orElseThrow(() -> new ResourceNotFoundException(Constants.USER_NOT_FOUND));

    }
}
