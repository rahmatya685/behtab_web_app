package ir.zngis.behtab.CandidateLocation;

import ir.zngis.behtab.model.CandidateLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CandidateLocationRepository extends JpaRepository<CandidateLocation,String> {



    List<CandidateLocation> findAllByHospitalByHospitalId(int hospitalId);


    @Query(value = "SELECT * FROM candidate_location   WHERE  hospital_id IN (select hospital.id from hospital   where  hospital.evaluator_user_id = :userId or hospital.surveyor_user_id = :userId)",nativeQuery = true)
    List<CandidateLocation> getCandidateLocationByUserId(@Param("userId") int userId);
}
