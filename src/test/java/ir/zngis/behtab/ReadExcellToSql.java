package ir.zngis.behtab;


import ir.zngis.behtab.model.Question;
import ir.zngis.behtab.model.QuestionCategory;
import ir.zngis.behtab.util.EntityStatus;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.swing.text.html.parser.Entity;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;


public class ReadExcellToSql {
    public static final String SAMPLE_XLSX_FILE_PATH = "G:\\Work Offers\\UnHabitate\\questions\\Questions Format  V7 -98-03-15.xlsx";
    private static final String SQL_FILE_Insert = "G:\\Work Offers\\UnHabitate\\questions\\import_insert.sql";
    private static final String SQL_FILE_Update = "G:\\Work Offers\\UnHabitate\\questions\\import_update.sql";


    List<QuestionCategory> questionCategories = new ArrayList<>();
    List<Question> questions = new ArrayList<>();

    public void collectInfo() {

        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }


        Sheet sheet = workbook.getSheetAt(0);


        int lastRowQuestions = 167; //sheet.getLastRowNum();
        int lastRowCategories = 14;


        for (int i = 1; i < lastRowCategories; i++) {
            Row row = sheet.getRow(i);

            int id = (int) row.getCell(12).getNumericCellValue();
            String modoule = row.getCell(13).getStringCellValue();
            String questionCatPerName = row.getCell(14).getStringCellValue();
            String questionCatEngName = row.getCell(15).getStringCellValue();
            int status = (int) row.getCell(16).getNumericCellValue();


            QuestionCategory questionCategory = new QuestionCategory();
            questionCategory.setId(id);
            questionCategory.setCategoryTitleEn(questionCatEngName);
            questionCategory.setCategoryTitleFa(questionCatPerName);
            questionCategory.setModuleName(modoule);
            questionCategory.setStatus(EntityStatus.toEntityStatus(status));

            questionCategories.add(questionCategory);
        }

        for (int i = 1; i <= lastRowQuestions; i++) {
            Row row = sheet.getRow(i);

            System.out.println("Row=>" + row.getRowNum());

            int catId = (int) row.getCell(0).getNumericCellValue();
            int id = (int) row.getCell(1).getNumericCellValue();

            String qTitileFa = row.getCell(2).getStringCellValue();
            String qTitileEng = row.getCell(3).getStringCellValue();

            String qDesctiptionFa = row.getCell(4).getStringCellValue();
            String qDesctiptionEng = row.getCell(5).getStringCellValue();

            double weight = row.getCell(6).getNumericCellValue();

            int status = (int) row.getCell(7).getNumericCellValue();

            int isForHospital = (int) row.getCell(8).getNumericCellValue();

            int isForHealthHouse = (int) row.getCell(9).getNumericCellValue();


            Question question = new Question();
            question.setId(id);
            question.setCategoryId(catId);
            question.setQuestionTitleEn(qTitileEng);
            question.setQuestionTitleFa(qTitileFa);
            question.setBriefDescriptionEn(qDesctiptionEng);
            question.setBriefDescriptionFa(qDesctiptionFa);
            question.setHelpLink("https://unhabitat.org/iran/");
            question.setCode(String.format("F-%d", id));
            question.setWeight(weight);
            question.setStatus(EntityStatus.toEntityStatus(status));
            question.setUsed4HealthHouse(isForHealthHouse == 1);
            question.setUsed4Hospital(isForHospital ==1);

            questions.add(question);
        }
    }

    @Test
    public void genUpdateStatement() {


        collectInfo();


        String sqlUpdate = "update  public.question_category set category_title_en = '%s' ,category_title_fa= '%s' ,module_name ='%s' where public.question_category.id = %d ;";

        StringBuilder sqlContent = new StringBuilder();

        for (QuestionCategory questionCategory : questionCategories) {

            switch (questionCategory.getStatus()) {

                case DELETE:
                    break;
                case INSERT:
                    break;
                case SYNCED:
                    sqlContent.append("\n" + String.format(sqlUpdate,
                            questionCategory.getCategoryTitleEn(), questionCategory.getCategoryTitleFa(), questionCategory.getModuleName(), questionCategory.getId()
                    ));
                    break;
            }


        }


        sqlUpdate = "update public.question set brief_description_en ='%s' , brief_description_fa='%s' ,code='%s' ,help_link='%s' , question_title_en='%s' ,question_title_fa='%s' ,weight = %f,category_id='%s',hospital=%d ,health_house=%d where id=%d;";

        for (Question question : questions) {

            switch (question.getStatus()) {

                case DELETE:
                    break;
                case INSERT:
                    break;
                case SYNCED:
                    sqlContent.append("\n" + String.format(sqlUpdate,
                            question.getBriefDescriptionEn(), question.getBriefDescriptionFa(),
                            question.getCode(), question.getHelpLink(), question.getQuestionTitleEn(),
                            question.getQuestionTitleFa(), question.getWeight(), question.getCategoryId(),
                            question.isUsed4Hospital()? 1 : 0,question.isUsed4HealthHouse()?1:0,question.getId()
                    ));
                    break;
            }

        }

        writeToSqlFile(sqlContent.toString(), SQL_FILE_Update);


    }

    @Test
    public void genInsertStatement() {
        System.out.print("test");

        collectInfo();

//        StringBuilder sqlContent = new StringBuilder("insert  into public.question_category id,category_title_en,category_title_fa,module_name values ");
//        for (Map.Entry<String, QuestionCategory> stringQuestionCategoryEntry : questionCategories.entrySet()) {
//
//            QuestionCategory questionCategory = stringQuestionCategoryEntry.getValue();
//
//            sqlContent.append("\n" + String.format(" (%d,'%s','%s','%s'),",
//                    questionCategory.getId(), questionCategory.getCategoryTitleEn(), questionCategory.getCategoryTitleFa(), questionCategory.getModuleName()
//            ));
//
//
//        }
//        int index = sqlContent.lastIndexOf(",");
//        sqlContent.replace(index, index, ";");
//
//        sqlContent.append("\n insert  into public.question (id,brief_description_en,brief_description_fa,code,help_link, question_title_en,question_title_fa,weight,category_id) values");
//
//        for (Map.Entry<String, Question> stringQuestionEntry : questions.entrySet()) {
//
//            Question question = stringQuestionEntry.getValue();
//
//            sqlContent.append("\n" + String.format("  (%d,'%s','%s','%s','%s','%s','%s', %f  , %d ),",
//                    question.getId(), question.getBriefDescriptionEn(), question.getBriefDescriptionFa(),
//                    question.getCode(), question.getHelpLink(), question.getQuestionTitleEn(),
//                    question.getQuestionTitleFa(), question.getWeight(), question.getCategoryId()
//            ));
//        }
//
//        index = sqlContent.lastIndexOf(",");
//        sqlContent.replace(index, index, ";");
//
//        writeToSqlFile(sqlContent.toString(), SQL_FILE_Insert);


    }

    public void writeToSqlFile(String content, String fileName) {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {


            File file = new File(fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            bw.write(content);

            System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
    }


}
