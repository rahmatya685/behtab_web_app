package ir.zngis.behtab.evaluation;

import ir.zngis.behtab.model.QuestionCategory4Evaluation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;



@RunWith(SpringRunner.class)
@SpringBootTest
public class EvaluationRepositoryTest {


    @Autowired
    QuestionCategory4EvaluationRepository repository;
    @Test
    public void getEvaluationsSocre4Building() {

        List<QuestionCategory4Evaluation> data =repository.getEvaluationsSocre4Building(886);


        data.stream().map(questionCategory4Evaluation -> questionCategory4Evaluation.getModule_name()).distinct().forEach(s -> {

            System.out.print(s);

        });

        Assert.assertFalse( data.isEmpty());

    }
}