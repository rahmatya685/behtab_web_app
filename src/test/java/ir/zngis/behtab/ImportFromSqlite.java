package ir.zngis.behtab;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import ir.zngis.behtab.Entrance.EntranceController;
import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.attachment.AttachmentController;
import ir.zngis.behtab.attachment.AttachmentRepo;
import ir.zngis.behtab.building.BuildingController;
import ir.zngis.behtab.building.BuildingRepository;
import ir.zngis.behtab.building.dto.BuildingMobile;
import ir.zngis.behtab.evaluation.EvaluationController;
import ir.zngis.behtab.hospital.HospitalController;
import ir.zngis.behtab.hospital.HospitalRepo;
import ir.zngis.behtab.hospitalUnit.*;
import ir.zngis.behtab.model.*;
import ir.zngis.behtab.questions.QuestionRepo;
import ir.zngis.behtab.unit_category.UnitCategoryRepo;
import ir.zngis.behtab.unit_sub_category.UnitSubCategoryRepo;
import ir.zngis.behtab.util.EntityStatus;
import ir.zngis.behtab.util.HealthLocationType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Principal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportFromSqlite {


    // private static final String mUserName = "sm.pv";
    // // String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\sm.pv\\sm.pv\\Db2\\Db_";
    //    String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\sm.pv\\sm.pv\\Db1\\Db_";
    // String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\sm.pv\\sm.pv\\Db_22Tir\\Db_";
    //String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\sm.pv\\sm.pv\\db3\\Db_";
   // String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\sm.pv\\sm.pv\\db4\\Db_";





    //private static final String mUserName = "ay.pv";
    // String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ay.pv\\ay.pv\\Db98.4.16-17\\Db_";
    //String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ay.pv\\ay.pv\\Db98.4.18\\Db_";
    //String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ay.pv\\ay.pv\\Db98.4.22\\Db_";
    //String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ay.pv\\ay.pv\\Db98.4.24\\Db_";
    //String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ay.pv\\ay.pv\\Db98.4.28\\Db_";


//    private static final String mUserName = "fk.pv";
//    String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\fk.pv\\Db\\Db\\Db_";
//


    //private static final String mUserName = "ba.pv";
    //    String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ba.pv\\Db1\\Db_";
   // String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ba.pv\\Db2\\Db_";


   private static final String mUserName = "gh.pv";
//    String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ghanizaded\\98-05-28\\Db_";
    // String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ghanizaded\\db4\\Db_";
    String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Desktop\\ghanizaded\\db5\\Db_";



//    private static final String mUserName = "mr.pv";
//
//    String url = "jdbc:sqlite:C:\\Users\\Alireaza-PC\\Downloads\\mr.pv(2019-06-27) (Ms Rafizade)\\mr.pv(2019-06-27) (Ms Rafizade)\\Db\\Db_";
//


    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    UserRepo userRepo;

    @Autowired
    HospitalController hospitalController;


    @Autowired
    HospitalRepo hospitalRepo;

    @Autowired
    BuildingRepository buildingRepository;

    @Autowired
    SurgeryDepartmentRepository surgeryDepartmentRepository;


    @Autowired
    ClinicalUnitRepository clinicalUnitRepository;

    @Autowired
    NonClinicalUnitRepository nonClinicalUnitRepository;

    @Autowired
    QuestionRepo questionRepo;

    @Autowired
    BuildingController buildingController;

    @Autowired
    EntranceController entranceController;


    @Autowired
    SurgeryDepartmentController surgeryDepartmentController;

    @Autowired
    NonClinicalUnitController nonClinicalUnitController;

    @Autowired
    ClinicalUnitController clinicalUnitController;

    @Autowired
    EvaluationController evaluationController;

    @Autowired
    AttachmentController attachmentController;

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    UnitCategoryRepo unitCategoryRepo;
    @Autowired
    UnitSubCategoryRepo unitSubCategoryRepo;


    private User user;

    Connection connection;


    @Before
    public void setup() {
        mapper.registerModule(new JtsModule());

        user = userRepo.findByUsername(mUserName).get();

        connection = connect();
    }

    @Test
    public void collectData() {
        Principal principal = () -> mUserName;

        // List<Hospital> hospitals = getHospitals();

        //hospitalController.update(hospitals, principal);

        List<Building> buildings = getBuildings();


        for (Building building : buildings) {
            switch (building.getStatus()) {

                case DELETE:
                    List<String> buildingIds = new ArrayList<>();
                    buildingIds.add(building.getRowGuid());
                    buildingController.delete(buildingIds, building.getHospitalId(), principal);
                    break;
                case INSERT:
                case SYNCED:

                    BuildingMobile buildingMobile = new BuildingMobile();

                    buildingMobile.setHospitalByHospitalId(building.getHospitalByHospitalId());

                    buildingMobile.setGeom(building.getGeom());

                    buildingMobile.setHospitalId(building.getHospitalId());

                    buildingRepository.findById(building.getRowGuid()).ifPresent(oldBuilding -> {
                        buildingMobile.setName(oldBuilding.getName());
                        buildingMobile.setComment(oldBuilding.getComment());
                    });

                    buildingMobile.setRowGuid(building.getRowGuid());

                    List<BuildingMobile> buildingMobiles = new ArrayList<>();

                    buildingMobiles.add(buildingMobile);

                    buildingController.insert(buildingMobiles, building.getHospitalId(), principal);
                    break;
            }
        }


        List<HospitalEntrance> entrances = getEntrances();

        for (HospitalEntrance building : entrances) {
            switch (building.getStatus()) {

                case DELETE: {
                    List<String> buildingIds = new ArrayList<>();
                    buildingIds.add(building.getRowGuid());
                    buildingController.delete(buildingIds, building.getHospitalId(), principal);
                    break;
                }
                case INSERT:
                case SYNCED:
                    entranceController.insert(building, building.getHospitalId(), principal);
                    break;
            }
        }

        List<SurgeryDepartment> surgeryDepartments = getSurgeryDepartments();

        for (SurgeryDepartment surgeryDepartment : surgeryDepartments) {

            buildingRepository.findById(surgeryDepartment.getBuildingId()).ifPresent(b -> {
                switch (surgeryDepartment.getStatus()) {
                    case DELETE:
                        List<String> deleteIds = new ArrayList<>();
                        deleteIds.add(surgeryDepartment.getRowGuid());
                        surgeryDepartmentController.delete(deleteIds, b.getHospitalId(), b.getRowGuid(), principal);
                        break;
                    case INSERT:
                    case SYNCED:
                        List<SurgeryDepartment> surgeryDepartments4Insert = new ArrayList<>();
                        surgeryDepartments4Insert.add(surgeryDepartment);
                        surgeryDepartmentController.insert(surgeryDepartments4Insert, b.getHospitalId(), b.getRowGuid(), principal);
                        break;
                }
            });
        }

        List<ClinicalUnit> ClinicalUnits = getClinicalUnits();

        for (ClinicalUnit clinicalUnit : ClinicalUnits) {

            buildingRepository.findById(clinicalUnit.getBuildingId()).ifPresent(b -> {

                switch (clinicalUnit.getStatus()) {
                    case DELETE:
                        List<String> deleteIds = new ArrayList<>();
                        deleteIds.add(clinicalUnit.getRowGuid());
                        clinicalUnitController.delete(deleteIds, b.getHospitalId(), b.getRowGuid(), principal);
                        break;
                    case INSERT:
                    case SYNCED:
                        List<ClinicalUnit> ClinicalUnits4Insert = new ArrayList<>();
                        ClinicalUnits4Insert.add(clinicalUnit);
                        clinicalUnitController.insertAll(ClinicalUnits4Insert, b.getHospitalId(), b.getRowGuid(), principal);
                        break;
                }

            });

        }


        List<NonClinicalUnit> nonClinicalUnits = getNonClinicalUnits();

        for (NonClinicalUnit nonClinicalUnit : nonClinicalUnits) {

            buildingRepository.findById(nonClinicalUnit.getBuildingId()).ifPresent(b -> {

                switch (nonClinicalUnit.getStatus()) {
                    case DELETE:
                        List<String> deleteIds = new ArrayList<>();
                        deleteIds.add(nonClinicalUnit.getRowGuid());
                        nonClinicalUnitController.delete(deleteIds, b.getHospitalId(), b.getRowGuid(), principal);
                        break;
                    case SYNCED:
                    case INSERT:
                        List<NonClinicalUnit> ClinicalUnits4Insert = new ArrayList<>();
                        ClinicalUnits4Insert.add(nonClinicalUnit);
                        System.out.print(nonClinicalUnit.getRowGuid());
                        nonClinicalUnitController.insert(ClinicalUnits4Insert, b.getHospitalId(), b.getRowGuid(), principal);
                        break;
                }
            });
        }

        List<Evaluation> evaluations = getEvaluations();


        for (Evaluation evaluation : evaluations) {
            switch (evaluation.getStatus()) {
                case DELETE:
                    try {
                        evaluationController.delete(evaluation.getRowGuid(), principal);
                    } catch (Exception e) {

                    }
                    break;
                case SYNCED:
                case INSERT:
                    List<Evaluation> evaluation4Insert = new ArrayList<>();
                    evaluation4Insert.add(evaluation);
                    evaluationController.insert(evaluation4Insert, principal);
                    break;

            }
        }


        List<Attachment> attachments = getAttachments();

        for (Attachment attachment : attachments) {
            switch (attachment.getStatus()) {
                case DELETE:
                    List<String> deleteIds = new ArrayList<>();
                    deleteIds.add(attachment.getRowGuid());
                    attachmentController.deleteAttachments(deleteIds, principal);
                    break;
                case INSERT:
                case SYNCED:
                    attachmentRepo.save(attachment);
                    break;
            }
        }

        Assert.assertEquals(1, 1);
    }

    /**
     * Connect to the test.db database
     *
     * @return the Connection object
     */
    private Connection connect() {
        // SQLite connection string
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }


    private List<Hospital> getHospitals() {
        List<Hospital> entites = new ArrayList<>();


        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from hospital");

            while (resultSet.next()) {

                Hospital entity = new Hospital();

                entity.setHealthLocationType(HealthLocationType.HOSPITAL);

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setId(resultSet.getInt("ID"));

                entity.setName(resultSet.getString("NAME"));

                entity.setAddress(resultSet.getString("ADDRESS"));


                String geomBoundaryString = resultSet.getString("BOUNDARY");

                if (geomBoundaryString != null) {
                    Polygon boundary = mapper.readValue(geomBoundaryString, Polygon.class);

                    entity.setBoundary(boundary);
                }

                entity.setPhone(resultSet.getString("PHONE"));

                entity.setFax(resultSet.getString("FAX"));

                entity.setWebSite(resultSet.getString("WEB_SITE"));

                entity.setNumTotalApprovedBeds(resultSet.getInt("NUM_TOTAL_APPROVED_BEDS"));

                entity.setNumTotalActiveBeds(resultSet.getInt("NUM_TOTAL_ACTIVE_BEDS"));

                entity.setBedOccupancyNormalRate(Double.valueOf(resultSet.getLong("BED_OCCUPANCY_RATE_NORM")));

                entity.setTotalStaffCount(resultSet.getInt("TOTAL_STAFF_COUNT"));

                entity.setClinicalStaffCount(resultSet.getInt("CLINICAL_STAFF_COUNT"));

                entity.setNonClinicalStaffCount(resultSet.getInt("NON_CLINICAL_STAFF_COUNT"));

                entity.setSeniorManagersName(resultSet.getString("SENIOR_MANAGERS_NAME"));

                entity.setCrisisManagersNamesAndPhones(resultSet.getString("CRISIS_MANAGERS_NAMES_AND_PHONES"));

                entity.setOrganizationalAffiliation(resultSet.getInt("ORGANIZATIONAL_AFFILIATION"));

                entity.setServiceType(resultSet.getInt("SERVICE_TYPE"));

                entity.setRoleInCrisisAndDisasters(resultSet.getInt("ROLE_IN_HEALTHCARE_NETWORK"));

                entity.setRoleInCrisisAndDisasters(resultSet.getInt("ROLE_IN_CRISIS_AND_DISASTERS"));

                entity.setStructureType(resultSet.getString("STRUCTURE_TYPE"));

                entity.setServedPopulation(resultSet.getLong("SERVED_POPULATION"));


                String geomINFLUENCE_AREAString = resultSet.getString("INFLUENCE_AREA");
                if (geomINFLUENCE_AREAString != null) {
                    Polygon geomINFLUENCE_AREA = mapper.readValue(geomINFLUENCE_AREAString, Polygon.class);

                    entity.setInfluenceArea(geomINFLUENCE_AREA);
                }


                int evalUser = resultSet.getInt("evaluator_user_id");

                int survUser = resultSet.getInt("surveyor_user_id");


                entity.setEvaluatorUserId(evalUser);
                entity.setEvaluatorUser(userRepo.getOne(evalUser));

                entity.setSurveyorUserId(survUser);
                entity.setSurveyorUser(userRepo.getOne(survUser));


                if (hasColumn(resultSet, "TYPE")) {
                    int type = resultSet.getInt("TYPE");
                    entity.setHealthLocationType(HealthLocationType.toHealthLocationType(type));
                }


                String geomCENTER_POINTString = resultSet.getString("CENTER_POINT");

                Point geomgeomCENTER_POINTString = mapper.readValue(geomCENTER_POINTString, Point.class);

                entity.setCenterPoint(geomgeomCENTER_POINTString);


                entites.add(entity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }

    private List<ClinicalUnit> getClinicalUnits() {
        List<ClinicalUnit> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from clinical_unit ");

            while (resultSet.next()) {

                ClinicalUnit entity = new ClinicalUnit();

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setRowGuid(resultSet.getString("ROW_GUID"));

                entity.setCategory(resultSet.getInt("CATEGORY"));

                entity.setRealStaffCount(resultSet.getInt("REAL_STAFF_COUNT"));

                entity.setPredictedStaffCount(resultSet.getInt("PREDICTED_STAFF_COUNT"));

                String buildingId = resultSet.getString("BUILDING_ID");
                Building building = buildingRepository.findById(buildingId).get();

                entity.setBuildingId(buildingId);
                entity.setBuildingByBuildingId(building);


                int name = resultSet.getInt("NAME");
                UnitSubCategory unitSubCategory = unitSubCategoryRepo.findById(name).get();
                entity.setName(name);
                entity.setUnitSubCategoryByName(unitSubCategory);

                int category = resultSet.getInt("CATEGORY");
                UnitCategory unitCategory = unitCategoryRepo.findById(category).get();
                entity.setCategory(category);
                entity.setUnitCategoryByCategory(unitCategory);


                entity.setNormalCapacity(resultSet.getInt("NORMAL_CAPACITY"));
                entity.setMaxCapacity(resultSet.getInt("MAX_CAPACITY"));
                entity.setIsolatedBedCount(resultSet.getInt("ISOLATED_BED_COUNT"));

                entites.add(entity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }


    private List<NonClinicalUnit> getNonClinicalUnits() {
        List<NonClinicalUnit> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from non_clinical_unit ");

            while (resultSet.next()) {

                NonClinicalUnit entity = new NonClinicalUnit();

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setRowGuid(resultSet.getString("ROW_GUID"));


                entity.setRealStaffCount(resultSet.getInt("REAL_STAFF_COUNT"));

                entity.setPredictedStaffCount(resultSet.getInt("PREDICTED_STAFF_COUNT"));

                entity.setObservation(resultSet.getString("OBSERVATION"));

                String buildingId = resultSet.getString("BUILDING_ID");
                Building building = buildingRepository.findById(buildingId).get();

                entity.setBuildingId(buildingId);
                entity.setBuildingByBuildingId(building);

                int name = resultSet.getInt("NAME");
                UnitSubCategory unitSubCategory = unitSubCategoryRepo.findById(name).get();
                entity.setName(name);
                entity.setUnitSubCategoryByName(unitSubCategory);

                int category = resultSet.getInt("CATEGORY");
                UnitCategory unitCategory = unitCategoryRepo.findById(category).get();
                entity.setCategory(category);
                entity.setUnitCategoryByCategory(unitCategory);

                entites.add(entity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }


    private List<SurgeryDepartment> getSurgeryDepartments() {
        List<SurgeryDepartment> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from surgery_department ");

            while (resultSet.next()) {

                SurgeryDepartment entity = new SurgeryDepartment();

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setRowGuid(resultSet.getString("ROW_GUID"));

                entity.setName(resultSet.getString("NAME"));


                String buildingId = resultSet.getString("BUILDING_ID");
                Building building = buildingRepository.findById(buildingId).get();

                entity.setBuildingId(buildingId);
                entity.setBuildingByBuildingId(building);

                entity.setNumBedsNormCondition(resultSet.getInt("NUM_BEDS_NORM_CONDITION"));

                entity.setNumBedsCrisisCondition(resultSet.getInt("NUM_BEDS_CRISIS_CONDITION"));


                entites.add(entity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }


    private List<Evaluation> getEvaluations() {
        List<Evaluation> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from evaluation");

            while (resultSet.next()) {

                Evaluation entity = new Evaluation();

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setRowGuid(resultSet.getString("ROW_GUID"));


                String buildingId = resultSet.getString("BUILDING_ID");
                if (buildingId != null) {
                    buildingRepository.findById(buildingId).ifPresent(b -> {
                        entity.setBuildingId(buildingId);
                        entity.setBuildingByBuildingId(b);
                    });
                }


                entity.setHasObserved(resultSet.getBoolean("HAS_OBSERVED"));
                entity.setRiskLevel(resultSet.getInt("RISK_LEVEL"));
                entity.setComment(resultSet.getString("COMMENT"));


                String SURGERY_DEPARTMENT_ID = resultSet.getString("SURGERY_DEPARTMENT_ID");
                if (SURGERY_DEPARTMENT_ID != null) {
                    surgeryDepartmentRepository.findById(SURGERY_DEPARTMENT_ID).ifPresent(s -> {
                        entity.setSurgeryDepartmentId(SURGERY_DEPARTMENT_ID);
                        entity.setSurgeryDepartmentBySurgeryDepartmentId(s);
                    });
                }


                String NON_CLINICAL_UNIT_ID = resultSet.getString("NON_CLINICAL_UNIT_ID");
                if (NON_CLINICAL_UNIT_ID != null) {
                    nonClinicalUnitRepository.findById(NON_CLINICAL_UNIT_ID).ifPresent(n -> {
                        entity.setNonClinicalUnitId(NON_CLINICAL_UNIT_ID);
                        entity.setNonClinicalUnitByNonClinicalUnitId(n);
                    });
                }


                String CLINICAL_UNIT_ID = resultSet.getString("CLINICAL_UNIT_ID");

                if (CLINICAL_UNIT_ID != null) {
                    clinicalUnitRepository.findById("CLINICAL_UNIT_ID").ifPresent(c -> {
                        entity.setClinicalUnitId(CLINICAL_UNIT_ID);
                        entity.setClinicalUnitByClinicalUnitId(c);
                    });
                }

                int questionId = resultSet.getInt("QUESTION_ID");

                Question question = questionRepo.findById(questionId).get();

                entity.setQuestionId(questionId);
                entity.setQuestionByQuestionId(question);


                entites.add(entity);


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }


    private List<HospitalEntrance> getEntrances() {
        List<HospitalEntrance> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from hospital_entrance ");

            while (resultSet.next()) {

                HospitalEntrance entity = new HospitalEntrance();

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setRowGuid(resultSet.getString("ROW_GUID"));

                entity.setName(resultSet.getString("NAME"));


                int hospitalId = resultSet.getInt("HOSPITAL_ID");

                entity.setHospitalId(hospitalId);

                entity.setHospitalByHospitalId(hospitalRepo.getOne(hospitalId));


                String geomString = resultSet.getString("ACCESS_POINT");

                Point geometry = mapper.readValue(geomString, Point.class);
                entity.setAccessPoint(geometry);

                entites.add(entity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }


    private List<Attachment> getAttachments() {
        List<Attachment> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from attachment");

            while (resultSet.next()) {

                Attachment entity = new Attachment();

                entity.setEditorUser(user);

                entity.setModifyDate(new Date());

                entity.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));

                entity.setRowGuid(resultSet.getString("ROW_GUID"));

                entity.setLinkId(resultSet.getString("LINK_ID"));

                entity.setExtension(resultSet.getString("EXTENSION"));

                entity.setPath(resultSet.getString("PATH"));

                entity.setCategory(resultSet.getString("CATEGORY"));

                String fileName = resultSet.getString("NAME");

                entity.setFileName(fileName);

//                String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                        .path("/downloadFile/")
//                        .path(fileName)
//                        .toUriString();
                entity.setFileDownloadUri("http://178.63.104.54:8080/behtab/downloadFile/" + fileName);

                entity.setComment(resultSet.getString("COMMENT"));

                entites.add(entity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;

    }


    private List<Building> getBuildings() {

        List<Building> entites = new ArrayList<>();

        try {

            ResultSet resultSet = connection.createStatement().executeQuery("select * from building");

            while (resultSet.next()) {

                Building building = new Building();
                building.setName(resultSet.getString("NAME"));
                building.setRowGuid(resultSet.getString("ROW_GUID"));
                building.setComment(resultSet.getString("COMMENT"));
                building.setStatus(EntityStatus.toEntityStatus(resultSet.getInt("STATUS")));
                building.setModifyDate(new Date());
                building.setEditorUser(user);

                int hospitalId = resultSet.getInt("HOSPITAL_ID");

                building.setHospitalId(hospitalId);

                building.setHospitalByHospitalId(hospitalRepo.getOne(hospitalId));


                String geomString = resultSet.getString("GEOM");

                Polygon geometry = mapper.readValue(geomString, Polygon.class);
                building.setGeom(geometry);


                entites.add(building);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return entites;
    }

    public static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columns = rsmd.getColumnCount();
        for (int x = 1; x <= columns; x++) {
            if (columnName.equals(rsmd.getColumnName(x))) {
                return true;
            }
        }
        return false;
    }
}
