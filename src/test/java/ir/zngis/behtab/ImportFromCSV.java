package ir.zngis.behtab;


import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import ir.zngis.behtab.User.UserRepo;
import ir.zngis.behtab.model.Building;
import ir.zngis.behtab.util.EntityStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportFromCSV {


    String[] files = new String[]{"BUILDING.csv"};

    String fileDirec = "C:\\Users\\Alireaza-PC\\Desktop\\";


    @Autowired
    UserRepo userRepo;


    @Test
    public void readFiles() {


        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JtsModule());




        GeometryFactory geometryFactory =  new GeometryFactory();

        List<Building> buildings=new ArrayList<>();

        try {

             try (CSVReader csvReader = new CSVReader(new FileReader(fileDirec+files[0]))) {
                String[] values = null;
                while ((values = csvReader.readNext()) != null) {
                    Building building=new Building();
                    building.setName(values[0]);
                    building.setComment(values[1]);


                    building.setHospitalId(Integer.valueOf(values[1]));

                    building.setStatus(EntityStatus.INSERT);

//                    building.setRowGuid();

                    Polygon polygon = mapper.readValue("", Polygon.class);
                    building.setGeom(polygon);

                    building.setModifyDate(new Date());

//                    userRepo.findByUsername()
//                    building.setEditorUser();

                 }
            }

        } catch (Exception e) {

        }

    }
}
