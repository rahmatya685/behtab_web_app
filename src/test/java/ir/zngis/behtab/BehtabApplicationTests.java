package ir.zngis.behtab;

import ir.zngis.behtab.building.BuildingController;
import ir.zngis.behtab.evaluation.EvaluationController;
import ir.zngis.behtab.hospitalUnit.ClinicalUnitController;
import ir.zngis.behtab.hospitalUnit.NonClinicalUnitController;
import ir.zngis.behtab.hospitalUnit.SurgeryDepartmentController;
import org.junit.Test;
import org.junit.runner.RunWith;
 import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BehtabApplicationTests {

    @Autowired
    private BuildingController buildingController;


    @Autowired
    private SurgeryDepartmentController surgeryDepartmentController;

    @Autowired
    private NonClinicalUnitController nonClinicalUnitController;

    @Autowired
    private ClinicalUnitController clinicalUnitController;

    @Autowired
    private EvaluationController evaluationController;

    @Test
    public void contextLoads() {


//        String geojsonPrimary = "{\"type\":\"Polygon\",\"coordinates\":[[[51.46105594933033,35.69655046810327],[51.456145159900196,35.69606471197106],[51.45573243498802,35.69268693686852],[51.46087087690831,35.69276808138963],[51.46240811794997,35.6959372821819],[51.46250769495964,35.69744110408772],[51.46105594933033,35.69655046810327]]]}";
//
//        GeojsonConvertor geojsonConvertor = new GeojsonConvertor();
//
//        Geometry geometry = geojsonConvertor.convertToEntityAttribute(geojsonPrimary);
//
//        String convertedGeojson = geojsonConvertor.convertToDatabaseColumn(geometry);
//
//        Assert.assertEquals(geojsonPrimary, convertedGeojson);

    }


    @Test
    public void syncData() {

    }

}
