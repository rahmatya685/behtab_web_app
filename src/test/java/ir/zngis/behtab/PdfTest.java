package ir.zngis.behtab;


import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.languages.ArabicLigaturizer;
import com.itextpdf.text.pdf.languages.LanguageProcessor;
import ir.zngis.behtab.attachment.FileStorageService;
import ir.zngis.behtab.model.QuestionCategory;
import ir.zngis.behtab.questions.QuestionCategoryRepo;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PdfTest {

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    QuestionCategoryRepo questionCategoryRepo;


    @Test
    public void genPdf() {

        String fileName = "iTextHelloWorld.pdf";

        fileStorageService.deleteUploadFile(fileName);


        String imageUrl = "http://localhost:8060/geoserver/behtab/wms?service=WMS&version=1.1.0&request=GetMap&layers=behtab:hospital_entrance,behtab:candidate_location,behtab:building&styles=&width=500&height=500&srs=EPSG:4326&format=image/jpeg&CQL_FILTER=hospital_id=5;hospital_id=5;hospital_id=5&bbox=51.4559234858151,35.6841104230497,51.4620725110925,35.6957042503234";

        try {

//            URL url = new URL(imageUrl);
//
//            String imgFilePath = fileStorageService.resolveFileInReportDirec("test.jpeg").toFile().getAbsolutePath();
//
//            InputStream inputStream = url.openStream();
//
//            OutputStream outputStream = new FileOutputStream(imgFilePath);
//
//            byte[] data = new byte[2048];
//            int length = 0;
//
//            while ((length = inputStream.read(data)) != -1) {
//                outputStream.write(data, 0, length);
//            }
//
//            inputStream.close();
//            outputStream.close();

            File resource = new ClassPathResource(
                    "font/Vazir.ttf").getFile();

            FileOutputStream archivo = new FileOutputStream(fileStorageService.resolveFileInReportDirec(fileName).toFile().getAbsolutePath());
            Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
            PdfWriter Writer = PdfWriter.getInstance(doc, archivo);
            doc.open();
            LanguageProcessor al = new ArabicLigaturizer();
            Writer.setRunDirection(PdfWriter.RUN_DIRECTION_LTR);
            BaseFont bfComic = BaseFont.createFont(resource.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bfComic, 12, Font.NORMAL);

            Paragraph p = new Paragraph(al.process("Hospitaldfs Name: تهران نو"), font);
            p.setAlignment(Element.ALIGN_CENTER);


            List list = new List(false);
            list.add(new Chunk(al.process("Hospital Name: تهران نو"), font));
            list.add(p);

            //            File downloadedImgFile = new File(imgFilePath);
//
//            if (downloadedImgFile.exists()){
//                Jpeg jpeg=new Jpeg(downloadedImgFile.toURI().toURL());
//
//                doc.add(jpeg);
//
//            }




            doc.add(p);
            doc.add(list);
            doc.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void generateGraph() {

        java.util.List<QuestionCategory> questionCategories = questionCategoryRepo.getCates4Report(693);
        DefaultStatisticalCategoryDataset dataset = new DefaultStatisticalCategoryDataset();


        for (QuestionCategory questionCategory : questionCategories) {

            dataset.add(questionCategory.getEvaluationAvg(),5, questionCategory.getCategoryTitleEn(), "Warm-up");
        }


        JFreeChart chart = ChartFactory.createBarChart(
                "Performance: JFreeSVG vs Batik", null /* x-axis label*/,
                "Milliseconds" /* y-axis label */, dataset);
        chart.addSubtitle(new TextTitle("Time to generate 1000 charts in SVG "
                + "format (lower bars = better performance)"));
        chart.setBackgroundPaint(Color.WHITE);
        CategoryPlot plot = (CategoryPlot) chart.getPlot();

        // ******************************************************************
        //  More than 150 demo applications are included with the JFreeChart
        //  Developer Guide...for more information, see:
        //
        //  >   http://www.object-refinery.com/jfreechart/guide.html
        //
        // ******************************************************************

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        chart.getLegend().setFrame(BlockBorder.NONE);

        BufferedImage objBufferedImage = chart.createBufferedImage(600, 800);
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        try {
            ImageIO.write(objBufferedImage, "png", bas);
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] byteArray = bas.toByteArray();

        String imgFilePath = fileStorageService.resolveFileInReportDirec("testdd.png").toFile().getAbsolutePath();

        InputStream in = new ByteArrayInputStream(byteArray);
        BufferedImage image = null;
        try {
            image = ImageIO.read(in);

            File outputfile = new File(imgFilePath);
            ImageIO.write(image, "png", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
